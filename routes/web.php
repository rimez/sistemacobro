<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/inicio', function () {
    return view('inicio');
});

//login
Route::get('/', 'Login@index')->name('login');
Route::post('/ingreso', 'Login@ingreso')->name('ingreso');
Route::get('/cerrar_sesion','Login@cerrar_sesion')->name('cerrar_sesion');
//sucursal
Route::get('sucursal/sucursal', 'SucursalControlador@index')->name('sucursales');
Route::get('sucursal/abm-sucursal', 'SucursalControlador@mostrar')->name('abm_sucursal');
Route::post('sucursal/guardar', 'SucursalControlador@guardar')->name('guardar_sucursal');
Route::post('sucursal/actualizar', 'SucursalControlador@actualizar')->name('actualizar_sucursal');
Route::get('sucursal/editar{id}', 'SucursalControlador@editar')->name('editar_sucursal');
Route::get('sucursal/eliminar{id}', 'SucursalControlador@eliminar')->name('eliminar_sucursal');
//feriados
Route::get('feriado/feriado', 'FeriadoControlador@index')->name('feriado');
Route::get('feriado/abm-feriado', 'FeriadoControlador@mostrar')->name('abm_feriado');
Route::post('feriado/guardar', 'FeriadoControlador@guardar')->name('guardar_feriado');
Route::post('feriado/actualizar', 'FeriadoControlador@actualizar')->name('actualizar_feriado');
Route::get('feriado/editar{id}', 'FeriadoControlador@editar')->name('editar_feriado');
Route::get('feriado/eliminar{id}', 'FeriadoControlador@eliminar')->name('eliminar_feriado');
//zonas
Route::get('zona/zona', 'ZonasControlador@index')->name('zonas');
Route::get('zona/abm-zona', 'ZonasControlador@mostrar')->name('abm_zonas');
Route::post('zona/guardar', 'ZonasControlador@guardar')->name('guardar_zona');
Route::post('zona/actualizar', 'ZonasControlador@actualizar')->name('actualizar_zona');
Route::get('zona/editar{id}', 'ZonasControlador@editar')->name('editar_zona');
Route::get('zona/eliminar{id}', 'ZonasControlador@eliminar')->name('eliminar_zona');
//usuarios
Route::get('usuario/usuario', 'UsuariosControlador@index')->name('usuarios');
Route::get('usuarios/abm-usuarios', 'UsuariosControlador@mostrar')->name('abm_usuarios');
Route::post('usuario/guardar', 'UsuariosControlador@guardar')->name('guardar_usuario');
Route::post('usuario/actualizar', 'UsuariosControlador@actualizar')->name('actualizar_usuario');
Route::get('usuario/editar{id}', 'UsuariosControlador@editar')->name('editar_usuario');
Route::get('usuario/eliminar{id}', 'UsuariosControlador@eliminarUsuario')->name('eliminar_usuario');
//clientes
Route::get('clientes/cliente', 'ClientesControlador@index')->name('clientes');
Route::get('clientes/abm-cliente', 'ClientesControlador@mostrar')->name('abm_clientes');
Route::post('clientes/guardar', 'ClientesControlador@guardar')->name('guardar_cliente');
Route::post('clientes/actualizar', 'ClientesControlador@actualizar')->name('actualizar_cliente');
Route::get('clientes/editar{id}', 'ClientesControlador@editar')->name('editar_cliente');
Route::get('clientes/eliminar{id}', 'ClientesControlador@eliminarCliente')->name('eliminar_cliente');
Route::get('clientes/obtnerGarante{idDepartamento}','ClientesControlador@obtnerGarante');
//independiente
Route::get('independiente/independiente{tabla}', 'independienteControlador@index')->name('independiente');
Route::post('independiente/guardar', 'independienteControlador@guardar')->name('guardar_independiente');
Route::post('independiente/actualizar', 'independienteControlador@actualizar')->name('actualizar_independiente');
Route::post('independiente/eliminar', 'independienteControlador@eliminar')->name('eliminar_independiente');

//perfiles
Route::get('usuario/perfil', 'PerfilesControlador@index')->name('perfil');;
Route::post('usuario/perfil_guardar', 'PerfilesControlador@guardar')->name('guardar_perfil');
Route::post('usuario/perfil_actualizar', 'PerfilesControlador@actualizar')->name('actualizar_perfil');
Route::get('usuario/perfil_eliminar{id}', 'PerfilesControlador@eliminar')->name('eliminar_perfil');
//departamentos
Route::get('departamento', 'DepartamentosControlador@index')->name('departamento');;
Route::post('departamento_guardar', 'DepartamentosControlador@guardar')->name('guardar_departamento');
Route::post('departamento_actualizar', 'DepartamentosControlador@actualizar')->name('actualizar_departamento');
Route::get('departamento_eliminar{id}', 'DepartamentosControlador@eliminar')->name('eliminar_departamento');
//nacionalidades
Route::get('nacionalidad', 'ControladorNacionalidades@index')->name('nacionalidad');;
Route::post('nacionalidad_guardar', 'ControladorNacionalidades@guardar')->name('guardar_nacionalidad');
Route::post('nacionalidad_actualizar', 'ControladorNacionalidades@actualizar')->name('actualizar_nacionalidad');
Route::get('nacionalidad_eliminar{id}', 'ControladorNacionalidades@eliminar')->name('eliminar_nacionalidad');
//ciudades
Route::get('ciudad/mostrar{idDepartamento}','ControladorCiudades@mostrar');
//planes
Route::get('planes/planes', 'PlanesControladorControlador@index')->name('planes');
Route::get('planes/abm-planes', 'PlanesControladorControlador@mostrar')->name('abm_planes');
Route::post('planes/guardar', 'PlanesControladorControlador@guardar')->name('guardar_plan');
Route::post('planes/actualizar', 'PlanesControladorControlador@actualizar')->name('actualizar_plan');
Route::get('planes/editar{id}', 'PlanesControladorControlador@editar')->name('editar_plan');
Route::get('planes/eliminar{id}', 'PlanesControladorControlador@eliminar')->name('eliminar_plan');
Route::get('planes/obtnerGastos{idPlan}','PlanesControladorControlador@obtnerGastos');
//creditos
Route::get('creditos/creditos', 'CreditosControlador@index')->name('creditos');
Route::get('creditos/abm-creditos', 'CreditosControlador@mostrar')->name('abm_creditos');
Route::post('creditos/guardar', 'CreditosControlador@guardar')->name('guardar_credito');
Route::post('creditos/actualizar', 'CreditosControlador@actualizar')->name('actualizar_credito');
Route::post('creditos/rechazar', 'CreditosControlador@rechazar')->name('rechazar_credito');
Route::get('creditos/editar{id}', 'CreditosControlador@editar')->name('editar_credito');
Route::get('creditos/eliminar{id}', 'CreditosControlador@eliminarCliente')->name('eliminar_credito');
