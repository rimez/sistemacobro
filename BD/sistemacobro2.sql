﻿# Host: localhost  (Version 5.7.24)
# Date: 2020-09-04 22:41:57
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "clientes"
#

DROP TABLE IF EXISTS `clientes`;
CREATE TABLE `clientes` (
  `idcliente` int(11) NOT NULL AUTO_INCREMENT,
  `nombrecliente` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `apellidocliente` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `cedulacliente` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `fechanaccliente` date DEFAULT NULL,
  `telefonocliente` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `celularcliente` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `sexocliente` tinyint(4) DEFAULT NULL,
  `estadocivil` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `domiciliocliente` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `coordenadasdomicilio` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `cedulavenccliente` date DEFAULT NULL,
  `fechaingresocliente` datetime DEFAULT NULL,
  `profesiones_idprofesion` int(11) NOT NULL,
  `estados_idestados` int(11) NOT NULL,
  `nacionalidades_idnacionalidad` int(11) NOT NULL,
  `ocupaciones_idocupaciones` int(11) NOT NULL,
  `ingresopromcliente` int(11) DEFAULT NULL,
  `ciudades_idciudades` int(11) NOT NULL,
  `ciudades_iddepartamentos` int(11) NOT NULL,
  PRIMARY KEY (`idcliente`,`profesiones_idprofesion`,`estados_idestados`,`nacionalidades_idnacionalidad`,`ocupaciones_idocupaciones`,`ciudades_idciudades`,`ciudades_iddepartamentos`),
  KEY `fk_cliente_profesiones1_idx` (`profesiones_idprofesion`),
  KEY `fk_cliente_estados1_idx` (`estados_idestados`),
  KEY `fk_cliente_nacionalidades1_idx` (`nacionalidades_idnacionalidad`),
  KEY `fk_clientes_ocupaciones1_idx` (`ocupaciones_idocupaciones`),
  KEY `fk_clientes_ciudades1_idx` (`ciudades_idciudades`,`ciudades_iddepartamentos`),
  CONSTRAINT `fk_cliente_estados1` FOREIGN KEY (`estados_idestados`) REFERENCES `estados` (`idestados`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cliente_nacionalidades1` FOREIGN KEY (`nacionalidades_idnacionalidad`) REFERENCES `nacionalidades` (`idnacionalidad`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cliente_profesiones1` FOREIGN KEY (`profesiones_idprofesion`) REFERENCES `profesiones` (`idprofesion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_clientes_ciudades1` FOREIGN KEY (`ciudades_idciudades`, `ciudades_iddepartamentos`) REFERENCES `ciudades` (`idciudades`, `iddepartamentos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_clientes_ocupaciones1` FOREIGN KEY (`ocupaciones_idocupaciones`) REFERENCES `ocupaciones` (`idocupaciones`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

#
# Structure for table "departamentos"
#

DROP TABLE IF EXISTS `departamentos`;
CREATE TABLE `departamentos` (
  `iddepartamentos` int(11) NOT NULL AUTO_INCREMENT,
  `departamento` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`iddepartamentos`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

#
# Structure for table "ciudades"
#

DROP TABLE IF EXISTS `ciudades`;
CREATE TABLE `ciudades` (
  `idciudades` int(11) NOT NULL AUTO_INCREMENT,
  `ciudad` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `iddepartamentos` int(11) NOT NULL,
  PRIMARY KEY (`idciudades`,`iddepartamentos`),
  KEY `fk_ciudades_departamentos_idx` (`iddepartamentos`),
  CONSTRAINT `fk_ciudades_departamentos` FOREIGN KEY (`iddepartamentos`) REFERENCES `departamentos` (`iddepartamentos`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

#
# Structure for table "estados"
#

DROP TABLE IF EXISTS `estados`;
CREATE TABLE `estados` (
  `idestados` int(11) NOT NULL AUTO_INCREMENT,
  `estado` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idestados`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

#
# Structure for table "nacionalidades"
#

DROP TABLE IF EXISTS `nacionalidades`;
CREATE TABLE `nacionalidades` (
  `idnacionalidad` int(11) NOT NULL AUTO_INCREMENT,
  `nacionalidad` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`idnacionalidad`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

#
# Structure for table "ocupaciones"
#

DROP TABLE IF EXISTS `ocupaciones`;
CREATE TABLE `ocupaciones` (
  `idocupaciones` int(11) NOT NULL AUTO_INCREMENT,
  `ocupacion` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idocupaciones`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

#
# Structure for table "perfiles"
#

DROP TABLE IF EXISTS `perfiles`;
CREATE TABLE `perfiles` (
  `idperfil` int(11) NOT NULL AUTO_INCREMENT,
  `perfil` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`idperfil`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

#
# Structure for table "planescredito"
#

DROP TABLE IF EXISTS `planescredito`;
CREATE TABLE `planescredito` (
  `idplanescredito` int(11) NOT NULL AUTO_INCREMENT,
  `montoplan` int(11) DEFAULT NULL,
  `plazoplan` int(11) DEFAULT NULL,
  `interesplan` int(11) DEFAULT NULL,
  `tipopagoplan` tinyint(4) DEFAULT NULL,
  `estadoplan` tinyint(4) DEFAULT NULL,
  `fechacreacionplan` datetime DEFAULT NULL,
  PRIMARY KEY (`idplanescredito`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

#
# Structure for table "profesiones"
#

DROP TABLE IF EXISTS `profesiones`;
CREATE TABLE `profesiones` (
  `idprofesion` int(11) NOT NULL AUTO_INCREMENT,
  `profesion` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idprofesion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

#
# Structure for table "sucursales"
#

DROP TABLE IF EXISTS `sucursales`;
CREATE TABLE `sucursales` (
  `idsucursales` int(11) NOT NULL AUTO_INCREMENT,
  `nombresucursal` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `telefonosucursal` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `celularsucursal` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `fechacreacionsucursal` datetime DEFAULT NULL,
  PRIMARY KEY (`idsucursales`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

#
# Structure for table "tipoprestamo"
#

DROP TABLE IF EXISTS `tipoprestamo`;
CREATE TABLE `tipoprestamo` (
  `idtipoprestamo` int(11) NOT NULL AUTO_INCREMENT,
  `nombretipoprestamo` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idtipoprestamo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

#
# Structure for table "usuarios"
#

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `idusuarios` int(11) NOT NULL AUTO_INCREMENT,
  `nombreusuario` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `apellidousuario` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cedulausuario` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `direccionusuario` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `celularusuario` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fechanacusuario` date DEFAULT NULL,
  `usuario` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `passusuario` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,
  `fotousuario` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `estado` tinyint(4) DEFAULT NULL,
  `ultimologinusuario` datetime DEFAULT NULL,
  `fechaingresousuario` date DEFAULT NULL,
  `idnacionalidad` int(11) NOT NULL,
  `idperfil` int(11) NOT NULL,
  `idciudades` int(11) NOT NULL,
  `iddepartamentos` int(11) NOT NULL,
  `sucursalidsucursal` int(11) NOT NULL,
  PRIMARY KEY (`idusuarios`,`idnacionalidad`,`idperfil`,`idciudades`,`iddepartamentos`,`sucursalidsucursal`),
  KEY `fk_usuarios_nacionalidades1_idx` (`idnacionalidad`),
  KEY `fk_usuarios_perfiles1_idx` (`idperfil`),
  KEY `fk_usuarios_ciudades1_idx` (`idciudades`,`iddepartamentos`),
  KEY `fk_usuarios_sucursales1_idx` (`sucursalidsucursal`),
  CONSTRAINT `fk_usuarios_ciudades1` FOREIGN KEY (`idciudades`, `iddepartamentos`) REFERENCES `ciudades` (`idciudades`, `iddepartamentos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuarios_nacionalidades1` FOREIGN KEY (`idnacionalidad`) REFERENCES `nacionalidades` (`idnacionalidad`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuarios_perfiles1` FOREIGN KEY (`idperfil`) REFERENCES `perfiles` (`idperfil`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuarios_sucursales1` FOREIGN KEY (`sucursalidsucursal`) REFERENCES `sucursales` (`idsucursales`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

#
# Structure for table "creditos"
#

DROP TABLE IF EXISTS `creditos`;
CREATE TABLE `creditos` (
  `idcreditos` int(11) NOT NULL AUTO_INCREMENT,
  `clientes_idcliente` int(11) NOT NULL,
  `creditogastosadm` int(11) DEFAULT NULL,
  `creditofechasolicitud` date DEFAULT NULL,
  `creditofechadesembolso` date DEFAULT NULL,
  `creditonomgarante` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `creditoapegarante` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `creditocigarante` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `creditoidgarante` int(11) DEFAULT NULL,
  `creditoestadosolicitud` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `creditonropagare` int(11) DEFAULT NULL,
  `tipoprestamo_idtipoprestamo` int(11) NOT NULL,
  `usuarios_idusuarios` int(11) NOT NULL,
  `planescredito_idplanescredito` int(11) NOT NULL,
  PRIMARY KEY (`idcreditos`,`clientes_idcliente`,`tipoprestamo_idtipoprestamo`,`usuarios_idusuarios`,`planescredito_idplanescredito`),
  KEY `fk_creditos_clientes1_idx` (`clientes_idcliente`),
  KEY `fk_creditos_tipoprestamo1_idx` (`tipoprestamo_idtipoprestamo`),
  KEY `fk_creditos_usuarios1_idx` (`usuarios_idusuarios`),
  KEY `fk_creditos_planescredito1_idx` (`planescredito_idplanescredito`),
  CONSTRAINT `fk_creditos_clientes1` FOREIGN KEY (`clientes_idcliente`) REFERENCES `clientes` (`idcliente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_creditos_planescredito1` FOREIGN KEY (`planescredito_idplanescredito`) REFERENCES `planescredito` (`idplanescredito`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_creditos_tipoprestamo1` FOREIGN KEY (`tipoprestamo_idtipoprestamo`) REFERENCES `tipoprestamo` (`idtipoprestamo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_creditos_usuarios1` FOREIGN KEY (`usuarios_idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

#
# Structure for table "zonacobranza"
#

DROP TABLE IF EXISTS `zonacobranza`;
CREATE TABLE `zonacobranza` (
  `idzonacobranza` int(11) NOT NULL AUTO_INCREMENT,
  `nombrezona` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `usuarios_idusuarios` int(11) NOT NULL,
  PRIMARY KEY (`idzonacobranza`,`usuarios_idusuarios`),
  UNIQUE KEY `usuarios_idusuarios_UNIQUE` (`usuarios_idusuarios`),
  KEY `fk_zonacobranza_usuarios1_idx` (`usuarios_idusuarios`),
  CONSTRAINT `fk_zonacobranza_usuarios1` FOREIGN KEY (`usuarios_idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
