﻿# Host: localhost  (Version 5.7.24)
# Date: 2020-09-24 17:08:45
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "clientes"
#

DROP TABLE IF EXISTS `clientes`;
CREATE TABLE `clientes` (
  `idcliente` int(11) NOT NULL AUTO_INCREMENT,
  `nombrecliente` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `apellidocliente` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `cedulacliente` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `fechanaccliente` date DEFAULT NULL,
  `telefonocliente` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `celularcliente` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `sexocliente` tinyint(4) DEFAULT NULL,
  `estadocivil` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `domiciliocliente` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `coordenadasdomicilio` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `cedulavenccliente` date DEFAULT NULL,
  `fechaingresocliente` datetime DEFAULT NULL,
  `profesiones_idprofesion` int(11) NOT NULL,
  `estados_idestados` int(11) NOT NULL,
  `nacionalidades_idnacionalidad` int(11) NOT NULL,
  `ocupaciones_idocupaciones` int(11) NOT NULL,
  `ingresopromcliente` int(11) DEFAULT NULL,
  `ciudades_idciudades` int(11) NOT NULL,
  `ciudades_iddepartamentos` int(11) NOT NULL,
  PRIMARY KEY (`idcliente`,`profesiones_idprofesion`,`estados_idestados`,`nacionalidades_idnacionalidad`,`ocupaciones_idocupaciones`,`ciudades_idciudades`,`ciudades_iddepartamentos`),
  KEY `fk_cliente_profesiones1_idx` (`profesiones_idprofesion`),
  KEY `fk_cliente_estados1_idx` (`estados_idestados`),
  KEY `fk_cliente_nacionalidades1_idx` (`nacionalidades_idnacionalidad`),
  KEY `fk_clientes_ocupaciones1_idx` (`ocupaciones_idocupaciones`),
  KEY `fk_clientes_ciudades1_idx` (`ciudades_idciudades`,`ciudades_iddepartamentos`),
  CONSTRAINT `fk_cliente_estados1` FOREIGN KEY (`estados_idestados`) REFERENCES `estados` (`idestados`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cliente_nacionalidades1` FOREIGN KEY (`nacionalidades_idnacionalidad`) REFERENCES `nacionalidades` (`idnacionalidad`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cliente_profesiones1` FOREIGN KEY (`profesiones_idprofesion`) REFERENCES `profesiones` (`idprofesion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_clientes_ciudades1` FOREIGN KEY (`ciudades_idciudades`, `ciudades_iddepartamentos`) REFERENCES `ciudades` (`idciudades`, `iddepartamentos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_clientes_ocupaciones1` FOREIGN KEY (`ocupaciones_idocupaciones`) REFERENCES `ocupaciones` (`idocupaciones`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

#
# Data for table "clientes"
#

INSERT INTO `clientes` VALUES (1,'Richar Daniel','Silva','47575748','1990-09-10','(3884) 884-848','(4484) 848-848',1,'1','Pte Franco Casi Costanera','-27.3018058,-55.9583927','2091-02-10','2020-09-24 17:07:13',1,1,7,1,2000000,5,7);

#
# Structure for table "departamentos"
#

DROP TABLE IF EXISTS `departamentos`;
CREATE TABLE `departamentos` (
  `iddepartamentos` int(11) NOT NULL AUTO_INCREMENT,
  `departamento` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`iddepartamentos`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

#
# Data for table "departamentos"
#

INSERT INTO `departamentos` VALUES (7,'Itapua');

#
# Structure for table "ciudades"
#

DROP TABLE IF EXISTS `ciudades`;
CREATE TABLE `ciudades` (
  `idciudades` int(11) NOT NULL AUTO_INCREMENT,
  `ciudad` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `iddepartamentos` int(11) NOT NULL,
  PRIMARY KEY (`idciudades`,`iddepartamentos`),
  KEY `fk_ciudades_departamentos_idx` (`iddepartamentos`),
  CONSTRAINT `fk_ciudades_departamentos` FOREIGN KEY (`iddepartamentos`) REFERENCES `departamentos` (`iddepartamentos`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

#
# Data for table "ciudades"
#

INSERT INTO `ciudades` VALUES (5,'Encarnacion',7);

#
# Structure for table "estados"
#

DROP TABLE IF EXISTS `estados`;
CREATE TABLE `estados` (
  `idestados` int(11) NOT NULL AUTO_INCREMENT,
  `estado` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idestados`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

#
# Data for table "estados"
#

INSERT INTO `estados` VALUES (1,'ACTIVO'),(2,'INACTIVO');

#
# Structure for table "nacionalidades"
#

DROP TABLE IF EXISTS `nacionalidades`;
CREATE TABLE `nacionalidades` (
  `idnacionalidad` int(11) NOT NULL AUTO_INCREMENT,
  `nacionalidad` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`idnacionalidad`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

#
# Data for table "nacionalidades"
#

INSERT INTO `nacionalidades` VALUES (7,'Paraguaya');

#
# Structure for table "ocupaciones"
#

DROP TABLE IF EXISTS `ocupaciones`;
CREATE TABLE `ocupaciones` (
  `idocupaciones` int(11) NOT NULL AUTO_INCREMENT,
  `ocupacion` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idocupaciones`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

#
# Data for table "ocupaciones"
#

INSERT INTO `ocupaciones` VALUES (1,'Empleado');

#
# Structure for table "perfiles"
#

DROP TABLE IF EXISTS `perfiles`;
CREATE TABLE `perfiles` (
  `idperfil` int(11) NOT NULL AUTO_INCREMENT,
  `perfil` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`idperfil`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

#
# Data for table "perfiles"
#

INSERT INTO `perfiles` VALUES (21,'Desarrollador'),(22,'cobrador');

#
# Structure for table "planescredito"
#

DROP TABLE IF EXISTS `planescredito`;
CREATE TABLE `planescredito` (
  `idplanescredito` int(11) NOT NULL AUTO_INCREMENT,
  `montoplan` int(11) DEFAULT NULL,
  `plazoplan` int(11) DEFAULT NULL,
  `interesplan` int(11) DEFAULT NULL,
  `tipopagoplan` tinyint(4) DEFAULT NULL,
  `estadoplan` tinyint(4) DEFAULT NULL,
  `fechacreacionplan` datetime DEFAULT NULL,
  `gatosadmplan` int(11) DEFAULT NULL,
  PRIMARY KEY (`idplanescredito`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

#
# Data for table "planescredito"
#

INSERT INTO `planescredito` VALUES (1,500000,44,40,1,1,'2020-09-24 17:05:08',26000);

#
# Structure for table "profesiones"
#

DROP TABLE IF EXISTS `profesiones`;
CREATE TABLE `profesiones` (
  `idprofesion` int(11) NOT NULL AUTO_INCREMENT,
  `profesion` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idprofesion`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

#
# Data for table "profesiones"
#

INSERT INTO `profesiones` VALUES (1,'ing informatico');

#
# Structure for table "sucursales"
#

DROP TABLE IF EXISTS `sucursales`;
CREATE TABLE `sucursales` (
  `idsucursales` int(11) NOT NULL AUTO_INCREMENT,
  `nombresucursal` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `telefonosucursal` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `celularsucursal` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `fechacreacionsucursal` datetime DEFAULT NULL,
  PRIMARY KEY (`idsucursales`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

#
# Data for table "sucursales"
#

INSERT INTO `sucursales` VALUES (1,'COBRO AGIL S.A','(9388) 492-929','(9293) 838-383','2020-09-24 16:57:14');

#
# Structure for table "feridados"
#

DROP TABLE IF EXISTS `feridados`;
CREATE TABLE `feridados` (
  `idferidados` int(11) NOT NULL AUTO_INCREMENT,
  `fechaferiados` date DEFAULT NULL,
  `descricionferidados` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `sucursales_idsucursales` int(11) NOT NULL,
  PRIMARY KEY (`idferidados`,`sucursales_idsucursales`),
  KEY `fk_feridados_sucursales1_idx` (`sucursales_idsucursales`),
  CONSTRAINT `fk_feridados_sucursales1` FOREIGN KEY (`sucursales_idsucursales`) REFERENCES `sucursales` (`idsucursales`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

#
# Data for table "feridados"
#


#
# Structure for table "tipoprestamo"
#

DROP TABLE IF EXISTS `tipoprestamo`;
CREATE TABLE `tipoprestamo` (
  `idtipoprestamo` int(11) NOT NULL AUTO_INCREMENT,
  `nombretipoprestamo` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idtipoprestamo`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

#
# Data for table "tipoprestamo"
#

INSERT INTO `tipoprestamo` VALUES (1,'Comerciales');

#
# Structure for table "usuarios"
#

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `idusuarios` int(11) NOT NULL AUTO_INCREMENT,
  `nombreusuario` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `apellidousuario` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cedulausuario` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `direccionusuario` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `celularusuario` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fechanacusuario` date DEFAULT NULL,
  `usuario` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `passusuario` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,
  `fotousuario` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `estado` tinyint(4) DEFAULT NULL,
  `ultimologinusuario` datetime DEFAULT NULL,
  `fechaingresousuario` date DEFAULT NULL,
  `idnacionalidad` int(11) NOT NULL,
  `idperfil` int(11) NOT NULL,
  `idciudades` int(11) NOT NULL,
  `iddepartamentos` int(11) NOT NULL,
  `sucursalidsucursal` int(11) NOT NULL,
  PRIMARY KEY (`idusuarios`,`idnacionalidad`,`idperfil`,`idciudades`,`iddepartamentos`,`sucursalidsucursal`),
  KEY `fk_usuarios_nacionalidades1_idx` (`idnacionalidad`),
  KEY `fk_usuarios_perfiles1_idx` (`idperfil`),
  KEY `fk_usuarios_ciudades1_idx` (`idciudades`,`iddepartamentos`),
  KEY `fk_usuarios_sucursales1_idx` (`sucursalidsucursal`),
  CONSTRAINT `fk_usuarios_ciudades1` FOREIGN KEY (`idciudades`, `iddepartamentos`) REFERENCES `ciudades` (`idciudades`, `iddepartamentos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuarios_nacionalidades1` FOREIGN KEY (`idnacionalidad`) REFERENCES `nacionalidades` (`idnacionalidad`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuarios_perfiles1` FOREIGN KEY (`idperfil`) REFERENCES `perfiles` (`idperfil`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuarios_sucursales1` FOREIGN KEY (`sucursalidsucursal`) REFERENCES `sucursales` (`idsucursales`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

#
# Data for table "usuarios"
#

INSERT INTO `usuarios` VALUES (9,'Richar Daniel','Meza Silva','2223344','Pte Franco Casi Costanera','(9230) 929-329','1990-07-10','RMEZA','$2y$10$RP2Qk2vgIy57XaNU/w11ZOdyMc0CyyLlZTUv2D0Jz4nND9tPLsa5C','/assets/lte/img/usuarios/RMEZA/351.png',1,'2020-09-24 17:01:00','2020-09-24',7,21,5,7,1),(10,'Dario','Marin','238845','Pte Franco Casi Costanera','(2394) 848-489','1990-03-10','dmarin','$2y$10$m0jCrqL84.M8YWhkpMeyPO98LWN7s/IfbnXGqBUYBk6QpBZxJtyFi','/assets/lte/img/usuarios/dmarin/default.png',1,NULL,'2020-09-24',7,22,5,7,1);

#
# Structure for table "creditos"
#

DROP TABLE IF EXISTS `creditos`;
CREATE TABLE `creditos` (
  `idcreditos` int(11) NOT NULL AUTO_INCREMENT,
  `clientes_idcliente` int(11) NOT NULL,
  `creditogastosadm` int(11) DEFAULT NULL,
  `creditofechasolicitud` date DEFAULT NULL,
  `creditogarantia` tinyint(4) DEFAULT NULL,
  `creditofechadesembolso` date DEFAULT NULL,
  `creditonomgarante` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `creditoapegarante` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `creditocigarante` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `creditoidgarante` int(11) DEFAULT NULL,
  `creditoestadosolicitud` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `creditonropagare` int(11) DEFAULT NULL,
  `tipoprestamo_idtipoprestamo` int(11) NOT NULL,
  `usuarios_idusuarios` int(11) NOT NULL,
  `planescredito_idplanescredito` int(11) NOT NULL,
  PRIMARY KEY (`idcreditos`,`clientes_idcliente`,`tipoprestamo_idtipoprestamo`,`usuarios_idusuarios`,`planescredito_idplanescredito`),
  KEY `fk_creditos_clientes1_idx` (`clientes_idcliente`),
  KEY `fk_creditos_tipoprestamo1_idx` (`tipoprestamo_idtipoprestamo`),
  KEY `fk_creditos_usuarios1_idx` (`usuarios_idusuarios`),
  KEY `fk_creditos_planescredito1_idx` (`planescredito_idplanescredito`),
  CONSTRAINT `fk_creditos_clientes1` FOREIGN KEY (`clientes_idcliente`) REFERENCES `clientes` (`idcliente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_creditos_planescredito1` FOREIGN KEY (`planescredito_idplanescredito`) REFERENCES `planescredito` (`idplanescredito`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_creditos_tipoprestamo1` FOREIGN KEY (`tipoprestamo_idtipoprestamo`) REFERENCES `tipoprestamo` (`idtipoprestamo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_creditos_usuarios1` FOREIGN KEY (`usuarios_idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

#
# Data for table "creditos"
#

INSERT INTO `creditos` VALUES (1,1,26000,'2020-09-24',1,NULL,NULL,NULL,NULL,NULL,'SOLICITADO',NULL,1,10,1);

#
# Structure for table "zonacobranza"
#

DROP TABLE IF EXISTS `zonacobranza`;
CREATE TABLE `zonacobranza` (
  `idzonacobranza` int(11) NOT NULL AUTO_INCREMENT,
  `nombrezona` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `usuarios_idusuarios` int(11) NOT NULL,
  PRIMARY KEY (`idzonacobranza`,`usuarios_idusuarios`),
  UNIQUE KEY `usuarios_idusuarios_UNIQUE` (`usuarios_idusuarios`),
  KEY `fk_zonacobranza_usuarios1_idx` (`usuarios_idusuarios`),
  CONSTRAINT `fk_zonacobranza_usuarios1` FOREIGN KEY (`usuarios_idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

#
# Data for table "zonacobranza"
#

INSERT INTO `zonacobranza` VALUES (1,'15001',10);
