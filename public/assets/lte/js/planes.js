/*======================================
EDITAR PLAN DE CREDITO
======================================*/
$(document).on("click",".btnEditarPlan", function(){
    var idPlan = $(this).attr("idPlan");
    var montoPlan = $(this).attr("montoPlan");
    var plazoPlan = $(this).attr("plazoPlan");
    var interesPlan = $(this).attr("interesPlan");
    var tipoPagoPlan = $(this).attr("tipoPagoPlan");
    var estadoPlan = $(this).attr("estadoPlan");
    var gastosadmPlan = $(this).attr("gastosadmPlan");
    $("#editarEstado > option[value="+estadoPlan+"]").attr("selected",true);
    $("#idActual").val(idPlan);
    $("#EditarMonto").val(montoPlan);
    $("#nuevoGastAdm").val(gastosadmPlan);
    $("#editarPlazo").val(plazoPlan);
    $("#editarTasaInteres").val(interesPlan);
    $("#editarTipoPago > option[value="+tipoPagoPlan+"]").attr("selected",true);
});
/*=============================================
ELIMINAR PLAN DE CREDITOS
=============================================*/
$(document).on("click",".btnEliminarPlan", function(){

    swal({
        title: '¿Esta seguro de borrar este plan de creditos?',
        text: "Si no lo esta puede cancelar la acción",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, borrar plan de creditos'
    }).then((result)=>{
        if (result.value) {
            //window.location = "index.php?ruta=categorias&idCategoria="+idCategoria;
            window.location.href = $(this).data('href');
        }
    })
});
