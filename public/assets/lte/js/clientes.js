/*=============================================
SELECCIONAR COMBOS PARA ACTUALIZAR
=============================================*/
$(document).ready(function(){
  //  var idPerfil = $('.select2').attr('idbd');
    var sexo = $('.select2').attr('sexo');
    var estadoCivil = $('.select2').attr('estadoCivil');
    var idProfesion = $('.select2').attr('idProfesion');
    var idOcupacion = $('.select2').attr('idOcupacion');
    var idNacionalidad = $('.select2').attr('idNaci');
    var idDepartamento = $('.select2').attr('idDepar');
    var idCiudad = $('.select2').attr('idCiu');
    $("#sexo > option[value="+sexo+"]").attr("selected",true);
    $("#estadoCivil > option[value="+estadoCivil+"]").attr("selected",true);
    $('#idprofesion').val(idProfesion).trigger('change.select2');
    $('#idOcupacion').val(idOcupacion).trigger('change.select2');
    $('#idnacionalidad').val(idNacionalidad).trigger('change.select2');
    $('#idDepartamento').val(idDepartamento).trigger('change.select2');
    $('#idCiudad').val(idCiudad).trigger('change.select2');
});
/*=============================================
ELIMINAR usuario
=============================================*/
$(document).on("click",".btnEliminarCliente", function(){

    swal({
        title: '¿Esta seguro de borrar el cliente?',
        text: "Si no lo esta puede cancelar la acción",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, borrar cliente'
    }).then((result)=>{

        if (result.value) {

            //window.location = "index.php?ruta=categorias&idCategoria="+idCategoria;
            window.location.href = $(this).data('href');

        }

    })

});
