/*=============================================
ELIMINAR ASIGNACION DE PERMISO A USUARIO
=============================================*/
$(document).on("click",".btnEliminarPermisoDis", function(){

    swal({
        title: '¿Esta seguro de borrar el estadoss?',
        text: "Si no lo esta puede cancelar la acción",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, borrar estado'
    }).then((result)=>{

        if (result.value) {

            //window.location = "index.php?ruta=categorias&idCategoria="+idCategoria;
            window.location.href = $(this).data('href');


        }

    })

})
