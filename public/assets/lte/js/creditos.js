/*=============================================
MANIPULAR DATOS DE PLAN.
=============================================*/
var $plan = $('.plan') ;
$plan.select2().on('change', function() {
    var id = $("#idPlan").select2('val');
    $.ajax({
        url:"../planes/obtnerGastos" + id,
        type:'GET',
        dataType: "json",
        success:function(data) {
            var gastos = data.gatosadmplan;
            $("#gastosadm").val(gastos);
            console.log(gastos);
        }
    });


});
/*=============================================
MANIPULAR GARANTIAS.
=============================================*/
var $garantia = $('.garantia') ;
var $garante = $('.garante') ;
var nombre = $('.garantia').attr("nombre");
var usuario = $('.garantia').attr("cedula");
$garantia.select2().on('change', function() {

    //console.log($garantia.val());

    if($garantia.val()!="0"){
        $garante.prop('disabled',true);
        $("#nuevoApeyNomGar").val(null);
        $("#nuevaciGar").val(null);
        $('#garante').val("").trigger('change.select2');
    }else{
        //console.log($('#garante').val());

        $garante.prop('disabled',false);

    }
});
/*=============================================
BLOQUEAR GARANTE.
=============================================*/
var $clientes = $('.clientes') ;
$clientes.select2().on('change', function() {
    // var id = $("#cliente").select2('val');
    // var idCliente = "#G"+id;
    // console.log(idCliente);
    // $(idCliente).prop("disabled", !$(idCliente).prop('disabled'));
});
/*=============================================
ASIGNAR GARANTE
=============================================*/
var $garante = $('.garante') ;
$garante.select2().on('change', function() {
     var id = $("#garante").select2('val');
    // var idCliente = "#C"+id;
    // $(idCliente).prop('disabled',!$(idCliente).prop('disabled'));
   if (id!=""){
       $.ajax({
           url:"../clientes/obtnerGarante" + id,
           type:'GET',
           dataType: "json",
           success:function(data) {
               var nombreyApeGarante = data.apellidocliente+", "+data.nombrecliente;
               var ciGarante = data.cedulacliente;
               $("#nuevoApeyNomGar").val(nombreyApeGarante);
               $("#nuevaciGar").val(ciGarante);
           }
       });

       $("#idGarante").val(id);
   }else{
       $("#nuevoApeyNomGar").val("");
       $("#nuevaciGar").val("");
   }

});
/*=============================================
Aprobar Credito
=============================================*/
$(document).on("click",".btnAprobarCredito", function(){
    var idCredito = $(this).attr("idCredito");
    var nombreCliente = $(this).attr("nombreCliente");
    var montoPlan = $(this).attr("montoPlan");
    var estadoCredito = $(this).attr("estadoCredito");
    var token = $('meta[name="csrf-token"]').attr('content');

    if(estadoCredito=="DESEMBOLSADO"){
        swal({
            title: 'El Crédito ya se encuenta desembolsado',
            type: 'error',
            text: 'No se puede aprobar un crédito en estado desembolsado desde este modulo.',
            confirmButtonText: 'Aceptar',
            timer: 4000
        })
    }else{
        swal({
            title: '¿Está seguro/a de aprobar este crédito?',
            html: '<p align="left"><b>Crédito número: </b>'+idCredito+'</p>' +
                '<p align="left"><b>Cliente: </b>'+nombreCliente+'</p>'+
                '<p align="left"><b>Monto: </b>'+montoPlan+'</p>',
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Si, aprobar credito'
        }).then((result)=>{
            if (result.value) {
                $.ajax({
                    type: 'post',
                    url : "../creditos/actualizar",
                    data: {'id' : idCredito,
                        _token : token},
                    dataType:'json',
                    success: function (respuesta) {
                        swal({
                            type: "success",
                            title: "Crédito Aprobado",
                            timer: 1500
                        });
                        window.location = "../creditos/creditos";
                    }
                });
            }

        })
    }

});
/*=============================================
rechazar Credito
=============================================*/
$(document).on("click",".btnRechazarCredito", function(){
    var idCredito = $(this).attr("idCredito");
    var nombreCliente = $(this).attr("nombreCliente");
    var montoPlan = $(this).attr("montoPlan");
    var estadoCredito = $(this).attr("estadoCredito");
    var token = $('meta[name="csrf-token"]').attr('content');

    if(estadoCredito=="DESEMBOLSADO"){
        swal({
            title: 'El Crédito ya se encuenta desembolsado',
            type: 'error',
            text: 'No se puede rechazar un crédito en estado desembolsado desde este modulo.',
            confirmButtonText: 'Aceptar',
            timer: 4000
        })


    }else{
        swal({
            title: '¿Está seguro/a de rechazar este crédito?',
            html: '<p align="left"><b>Crédito número: </b>'+idCredito+'</p>' +
                '<p align="left"><b>Cliente: </b>'+nombreCliente+'</p>'+
                '<p align="left"><b>Monto: </b>'+montoPlan+'</p>',
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Si, rechazar crédito'
        }).then((result)=>{

            if (result.value) {
                $.ajax({
                    type: 'post',
                    url : "../creditos/rechazar",
                    data: {'id' : idCredito,
                        _token : token},
                    dataType:'json',
                    success: function (respuesta) {
                        swal({
                            type: "success",
                            title: "Credito Rechazado",
                            timer: 1500

                        });
                        window.location = "../creditos/creditos";
                    }
                });
            }
        })
    }
});
