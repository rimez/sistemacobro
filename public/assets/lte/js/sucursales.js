/*======================================
EDITAR USUARIO
======================================*/
$(document).on("click",".btnEditarSucursal", function(){
    var idSucursal = $(this).attr("idSucursal");
    var nombre = $(this).attr("nombreSucursal");
    var telefono = $(this).attr("telefono");
    var celular = $(this).attr("celular");

    $("#editarNombre").val(nombre);
    $("#editarTelefono").val(telefono);
    $("#editarCelular").val(celular);
    $("#idActual").val(idSucursal);
});
/*=============================================
ELIMINAR sucursal
=============================================*/
$(document).on("click",".btnEliminarSucursal", function(){

    swal({
        title: '¿Esta seguro de borrar esta sucursal?',
        text: "Si no lo esta puede cancelar la acción",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, borrar sucursal'
    }).then((result)=>{

        if (result.value) {

            //window.location = "index.php?ruta=categorias&idCategoria="+idCategoria;
            window.location.href = $(this).data('href');

        }

    })

})
