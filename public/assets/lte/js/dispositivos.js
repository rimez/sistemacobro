/*======================================
EDITAR DISPOSITIVO
======================================*/
$(document).on("click",".btnEditarDispositivo", function(){
    var idDispositivo = $(this).attr("idDispositivo");
    var dispositivo = $(this).attr("dispositivo");
    var mac = $(this).attr("mac");
    var ip = $(this).attr("ip");
    var IdtipoDispositivo = $(this).attr("IdtipoDispositivo");
    var idestadoDispositivo = $(this).attr("idestadoDispositivo");

    // var select=document.getElementById("estadosElementos");
    // var buscar=$(this).attr("nombreEstado");
    // for(var i=1;i<select.length;i++)
    // {
    //     if(select.options[i].text==buscar)
    //     {
    //         // seleccionamos el valor que coincide
    //         select.selectedIndex=i;
    //     }
    // }
    $("#idActual").val(idDispositivo);
    $("#editarDispositivo").val(dispositivo);
    $("#editarMac").val(mac);
    $("#editarIp").val(ip);
    $("#editarTipoDispositivo").val(IdtipoDispositivo);
    $("#editarEstadoDispositivo").val(idestadoDispositivo);
});

/*=============================================
ELIMINAR usuario
=============================================*/
$(document).on("click",".btnEliminarUsuario", function(){

    swal({
        title: '¿Esta seguro de borrar el usuario?',
        text: "Si no lo esta puede cancelar la acción",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, borrar usuario'
    }).then((result)=>{

        if (result.value) {

            //window.location = "index.php?ruta=categorias&idCategoria="+idCategoria;
            window.location.href = $(this).data('href');

        }

    })

})

