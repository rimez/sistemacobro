/*======================================
EDITAR IDEPENDIENTE
======================================*/
$(document).on("click",".btnEditarIndependiente", function(){
    var idIndependiente = $(this).attr("idIndependiente");
    var nombre = $(this).attr("nombre");

    $("#nuevoNombre").val(nombre);
    $("#idActual").val(idIndependiente);

});
/*=============================================
ELIMINAR INDEPENDIENTE
=============================================*/
$(document).on("click",".btnEliminarIndependiente", function(){

    swal({
        title: '¿Esta seguro de borrar este elemento?',
        text: "Si no lo esta puede cancelar la acción",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, borrar'
    }).then((result)=>{

        if (result.value) {
            var tabla = $(this).attr("tablaeli");
            var id = $(this).attr("idEli");
            var token = $(this).attr("token");
            var data = {tabla:tabla, id:id,_token:token};
            $.ajax({
                type: "post",
                url: "eliminar",
                data : data,
                success: function (data) {
                    swal({
                        type: "success",
                        title: "Eliminado/a correctamente",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false

                    }).then((result)=>{
                        if(result.value){
                            //window.location.href = $(this).data('href');
                            window.location = '/independiente/independiente'+tabla;
                        }

                    });

                },
                error: function () {
                    swal({
                        type: "error",
                        title: "No se puede eliminar este elemento. Ya esta siendo utilizado",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false

                    }).then((result)=>{
                        if(result.value){
                            //window.location.href = $(this).data('href');
                            window.location = '/independiente/independiente'+tabla;
                        }

                    });
                }




            })



        }

    })

})
