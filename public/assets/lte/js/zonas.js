/*======================================
EDITAR USUARIO
======================================*/
$(document).on("click",".btnEditarZona", function(){
    var idZona = $(this).attr("idZona");
    var idUsuario = $(this).attr("idUsuario");
   var nombre = $(this).attr("nombreZona");
    $("#comboUsuario > option[value="+idUsuario+"]").attr("selected",true);
    $("#idActual").val(idZona);
   $("#editarNombre").val(nombre);
});
/*=============================================
ELIMINAR zona
=============================================*/
$(document).on("click",".btnEliminarZona", function(){

    swal({
        title: '¿Esta seguro de borrar esta zona?',
        text: "Si no lo esta puede cancelar la acción",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, borrar usuario'
    }).then((result)=>{

        if (result.value) {

            //window.location = "index.php?ruta=categorias&idCategoria="+idCategoria;
            window.location.href = $(this).data('href');

        }

    })

});
