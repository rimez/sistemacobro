/*======================================
EDITAR Estado
======================================*/
$(document).on("click",".btnEditarEstado", function(){
    var idEstado = $(this).attr("idEstado");
    var nombre = $(this).attr("nombre");

    $("#nuevoNombre").val(nombre);
    $("#idActual").val(idEstado);

});

/*=============================================
ELIMINAR estado DISPOSITIVO
=============================================*/
$(document).on("click",".btnEliminarAsignacion", function(){

    swal({
        title: '¿Esta seguro/a que desea borrar esta asignacion?',
        text: "Si no lo esta puede cancelar la acción",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, borrar asignacion'
    }).then((result)=>{

        if (result.value) {

            //window.location = "index.php?ruta=categorias&idCategoria="+idCategoria;
            window.location.href = $(this).data('href');

        }

    })

})
/*=============================================
ELIMINAR ASIGNACION DE  DISPOSITIVO A USUARIO
=============================================*/
$(document).on("click",".btnEliminarEstadoDis", function(){

    swal({
        title: '¿Esta seguro de borrar el estado?',
        text: "Si no lo esta puede cancelar la acción",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, borrar estado'
    }).then((result)=>{

        if (result.value) {

            //window.location = "index.php?ruta=categorias&idCategoria="+idCategoria;
            window.location.href = $(this).data('href');

        }

    })

})

/*=============================================
ELIMINAR ASIGNACION DE DISPOSITIVO
=============================================*/
$(document).on("click",".btnEliminarDispositivo", function(){

    swal({
        title: '¿Esta seguro de borrar el estado?',
        text: "Si no lo esta puede cancelar la acción",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, borrar estado'
    }).then((result)=>{

        if (result.value) {

            //window.location = "index.php?ruta=categorias&idCategoria="+idCategoria;
            window.location.href = $(this).data('href');

        }

    })

})

