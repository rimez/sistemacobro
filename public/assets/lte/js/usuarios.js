/*======================================
EDITAR USUARIO
======================================*/
$(document).on("click",".btnEditarUsuario", function(){
    var idUsuario = $(this).attr("idUsuario");
    var nombre = $(this).attr("nombre");
    var usuario = $(this).attr("usuario");
    var pass = $(this).attr("pass");
    var idEstado = $(this).attr("idEstado");
    var select=document.getElementById("estadosElementos");
    var buscar=$(this).attr("nombreEstado");
    for(var i=1;i<select.length;i++)
    {
        if(select.options[i].text==buscar)
        {
            // seleccionamos el valor que coincide
            select.selectedIndex=i;
        }
    }

    $("#editarNombre").val(nombre);
    $("#ediitarUsuario").val(usuario);
    $("#passwordActual").val(pass);
    $("#idActual").val(idUsuario);
    $("#idEstado").val(idEstado);
});

/*=============================================
ELIMINAR usuario
=============================================*/
$(document).on("click",".btnEliminarUsuario", function(){

    swal({
        title: '¿Esta seguro de borrar el usuario?',
        text: "Si no lo esta puede cancelar la acción",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, borrar usuario'
    }).then((result)=>{

        if (result.value) {

            //window.location = "index.php?ruta=categorias&idCategoria="+idCategoria;
            window.location.href = $(this).data('href');

        }

    })

})
/*=============================================
SUBIENDO LA FOTO DEL USUARIO
=============================================*/
$(".nuevaFoto").change(function(){

    var imagen = this.files[0];

    /*=============================================
      VALIDAMOS EL FORMATO DE LA IMAGEN SEA JPG O PNG
      =============================================*/

    if(imagen["type"] != "image/jpeg" && imagen["type"] != "image/png"){

        $(".nuevaFoto").val("");

        swal({
            title: "Error al subir la imagen",
            text: "¡La imagen debe estar en formato JPG o PNG!",
            type: "error",
            confirmButtonText: "¡Cerrar!"
        });

    }else if(imagen["size"] > 4000000){

        $(".nuevaFoto").val("");

        swal({
            title: "Error al subir la imagen",
            text: "¡La imagen no debe pesar más de 4MB!",
            type: "error",
            confirmButtonText: "¡Cerrar!"
        });

    }else{

        var datosImagen = new FileReader;
        datosImagen.readAsDataURL(imagen);

        $(datosImagen).on("load", function(event){

            var rutaImagen = event.target.result;

            $(".previsualizar").attr("src", rutaImagen);

        })

    }
})
/*=============================================
OBTENER LISTA CIUDADES.
=============================================*/
var $departamento = $('.departamento');
var $selecCiudad = $(".ciudad");
var btnAgregar = $('.btnCiudad');

$departamento.select2().on('change', function() {
    if ($departamento.val() !== ""){
        $.ajax({
            url:"../ciudad/mostrar" + $departamento.val(), // if you say $(this) here it will refer to the ajax call not $('.company2')
            type:'GET',
            dataType: "json",
            success:function(data) {
                $selecCiudad.empty();
                $('.ciudad').prop('disabled', false);
                $.each(data, function(i, item) {
                    $selecCiudad.append($("<option></option>").attr("value", item["idciudades"]).text(item["ciudad"]));
                });
                $selecCiudad.prop('disabled',false);

            }
        });

    }else{
        $selecCiudad.empty();
        $selecCiudad.append($("<option></option>").attr("value", "").text("Seleccionar Ciudad"));
        $('.ciudad').prop('disabled',true);
    }
}).trigger('change');

/*=============================================
OBTENER LISTA CIUDADES ACTUALIZAR.
=============================================*/
var $departamento2 = $('.departamento2');
var $selecCiudad2 = $(".ciudad2");
var btnAgregar2 = $('.btnCiudad2');

$departamento2.select2().on('change', function() {
    if ($departamento2.val() !== ""){
        $.ajax({
            url:"../ciudad/mostrar" + $departamento2.val(), // if you say $(this) here it will refer to the ajax call not $('.company2')
            type:'GET',
            dataType: "json",
            success:function(data) {
                $selecCiudad2.empty();
                $('.ciudad2').prop('disabled', false);
                $.each(data, function(i, item) {
                    $selecCiudad2.append($("<option></option>").attr("value", item["idciudades"]).text(item["ciudad"]));
                });
                $selecCiudad2.prop('disabled',false);

            }
        });

    }
    // else{
    //     $selecCiudad2.empty();
    //     $selecCiudad2.append($("<option></option>").attr("value", "").text("Seleccionar Ciudad"));
    //     $('.ciudad2').prop('disabled',true);
    // }
}).trigger('change');

/*=============================================
SELECCIONAR PERFIL AL ACTUALIZAR.
=============================================*/
$(document).ready(function(){
  var idPerfil = $('.select2').attr('idbd');
   var idNacionalidad = $('.select2').attr('idNac');
    var idDepartamento = $('.select2').attr('idDepar');
    var idCiudad = $('.select2').attr('idCiu');
   $('#idPErfil').val(idPerfil).trigger('change.select2');
   $('#idnacionalidad').val(idNacionalidad).trigger('change.select2');
    $('#idDepartamento').val(idDepartamento).trigger('change.select2');
    $('#idCiudad').val(idCiudad).trigger('change.select2');
});







