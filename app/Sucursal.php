<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sucursal extends Model
{
    protected $table = 'sucursales';
    protected $primaryKey = 'idsucursales';
    protected $dates = ['fechacreacionsucursal'];
    public $timestamps = false;
}
