<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = 'clientes';
    protected $primaryKey = 'idcliente';
    protected $dates = ['fechanaccliente','fechaingresocliente'];
    public $timestamps = false;

}
