<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\Sucursal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class SucursalControlador extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sucursales = Sucursal::all();
        return view('abmSucursales', compact('sucursales'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function guardar(Request $request)
    {
      // dd($request->all());
        $validadorsucursal = Validator::make($request->all(), [
            'nuevoNombre' => "required|min:5|max:45|regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚÜü.' ]+$/|unique:sucursales,nombresucursal",
            'nuevoTelefono' => 'required|max:45|regex:/^[()\-0-9 ]+$/',
            'nuevoCelular' => 'required|max:45|regex:/^[()\-0-9 ]+$/',
        ],

            [
                'nuevoNombre.required' => 'El campo nombre Sucursal es requerido',
                'nuevoNombre.min' => 'El campo nombre Sucursal debe contener como minimo 5 caracteres',
                'nuevoNombre.max' => 'El campo nombre Sucursal debe contener como maximo 100 caracteres',
                'nuevoNombre.regex' => 'El campo nombre Sucursal no puede estar vacío o contener caracteres especiales',
                'nuevoNombre.unique' => 'El nombre de Sucursal ya esta asignada a otra Sucursal',
                //telefono
                'nuevoTelefono.required' => 'El campo Teléfono es requerido',
                'nuevoTelefono.max'  => 'El campo Teléfono no puede sobrepasar 45 caracteres',
                'nuevoTelefono.regex' => 'El campo Teléfono no puede estar vacío o contener caracteres especiales',
                //celular
                'nuevoCelular.required' => 'El campo celular es requerido',
                'nuevoCelular.max'  => 'El campo celular no puede sobrepasar 45 caracteres',
                'nuevoCelular.regex' => 'El campo celular no puede estar vacío o contener caracteres especiales'
            ]);

        if ($validadorsucursal->fails()) {
            return redirect()->back()->withErrors($validadorsucursal)->withInput();
        }else{
            $sucursal = new Sucursal();
            $sucursal->nombresucursal = $request->nuevoNombre;
            $sucursal->telefonosucursal = $request->nuevoTelefono;
            $sucursal->celularsucursal = $request->nuevoCelular;
            $sucursal->fechacreacionsucursal =date("Y-m-d H:i:s");
           $guardado = $sucursal->save();
           if($guardado){
               return redirect()->back()->with('mensaje','Sucursal registrada correctamente');

           }else{
               return redirect()->back()->withErrors('mensaje','Error al guardar sucursal');
           }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function mostrar($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function actualizar(Request $request)
    {
      //  dd($request->all());
        $validadorsucursal = Validator::make($request->all(), [
            'editarNombre' => "required|min:5|max:45|regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚÜü.' ]+$/|unique:sucursales,nombresucursal,".$request->input('idActual').",idsucursales",
            'editarTelefono' => 'required|max:45|regex:/^[()\-0-9 ]+$/',
            'editarCelular' => 'required|max:45|regex:/^[()\-0-9 ]+$/',
        ],

            [
                'editarNombre.required' => 'El campo nombre Sucursal es requerido',
                'editarNombre.min' => 'El campo nombre Sucursal debe contener como minimo 5 caracteres',
                'editarNombre.max' => 'El campo nombre Sucursal debe contener como maximo 100 caracteres',
                'editarNombre.regex' => 'El campo nombre Sucursal no puede estar vacío o contener caracteres especiales',
                'editarNombre.unique' => 'El nombre de Sucursal ya esta asignada a otra Sucursal',
                //telefono
                'editarTelefono.required' => 'El campo Teléfono es requerido',
                'editarTelefono.max'  => 'El campo Teléfono no puede sobrepasar 45 caracteres',
                'editarTelefono.regex' => 'El campo Teléfono no puede estar vacío o contener caracteres especiales',
                //celular
                'editarCelular.required' => 'El campo celular es requerido',
                'editarCelular.max'  => 'El campo celular no puede sobrepasar 45 caracteres',
                'editarCelular.regex' => 'El campo celular no puede estar vacío o contener caracteres especiales'
            ]);

        if ($validadorsucursal->fails()) {
            return redirect()->back()->withErrors($validadorsucursal)->withInput();
        }else{
            $id = $request->idActual;
            $sucursal = Sucursal::find($id);


            $sucursal->nombresucursal = $request->editarNombre;
            $sucursal->telefonosucursal = $request->editarTelefono;
            $sucursal->celularsucursal = $request->editarCelular;
           $guardado =  $sucursal->save();
            if($guardado){
                return redirect()->back()->with('mensaje','Sucursal actualizada correctamente');

            }else{
                return redirect()->back()->withErrors('mensaje','Error al actualizar datos sucursal');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function eliminar($id)
    {
        try {

            // Conseguimos el objeto
            $sucursal=Sucursal::where('idsucursales', '=', $id)->first();

            // Lo eliminamos de la base de datos
            $eliminar = $sucursal->delete();
            if($eliminar){
                return Redirect::back()->with('mensaje','Sucursal eliminada con exito');
            }else{
                return Redirect::back()->withErrors('mensaje','Error al eliminar esta sucursal');
            }


        }catch (\Illuminate\Database\QueryException $e){
            return Redirect::back()->withErrors(['No se puede eliminar esta sucursal. Ya esta siendo utilizado/a']);
        }
    }
}
