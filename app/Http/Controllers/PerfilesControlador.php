<?php

namespace App\Http\Controllers;
use App\Perfiles;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class PerfilesControlador extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function guardar(Request $request)
    {
        $validadorPerfil = Validator::make($request->all(),[
            'nuevoNombre' => 'required|min:3|max:45|regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚÜü ]+$/|unique:perfiles,perfil'],
        [
            'nuevoNombre.required' => 'El campo nombre Perfil es requerido',
            'nuevoNombre.min' => 'El campo nombre perfil debe contener como minimo 3 caracteres',
            'nuevoNombre.max' => 'El campo nombre perfil debe contener como maximo 100 caracteres',
            'nuevoNombre.regex' => 'El campo nombre perfil no puede estar vacío o contener caracteres especiales',
            'nuevoNombre.unique' => 'El nombre de perfil ya esta asignado a otro perfil'
        ]);

        if ($validadorPerfil->fails()) {
//            return redirect('usuario/usuario')
//                ->withErrors($validadorPerfil)
//                ->withInput();
            return redirect()->back()->withErrors($validadorPerfil)->withInput();
        }else{
            $perfil = new Perfiles;
            $perfil->perfil = $request->nuevoNombre;
            $perfil->save();
            return redirect('usuario/usuario')->with('mensaje','Perfil registrado correctamente');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function mostrar($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function eliminar($id)
    {
        //
    }
}
