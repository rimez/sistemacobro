<?php

namespace App\Http\Controllers;

use App\Http\Requests\ValidacionUsuario;
use App\Http\Requests\ValidacionUsuarioActualizar;
use App\Sucursal;
use App\Usuario;
use App\Utilidades\UtilFechas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Funciones;
use phpDocumentor;
use Fylesystem;


class UsuariosControlador extends Controller
{
    use Funciones;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $nacionalidades = db::select("SELECT
                                              `nacionalidades`.`idnacionalidad`,
                                              `nacionalidades`.`nacionalidad`
                                            FROM
                                              `nacionalidades`");
        $perfiles = db::select("SELECT
                                          `perfiles`.`idperfil`,
                                          `perfiles`.`perfil`
                                        FROM
                                          `perfiles`");
        $departamentos = db::select("SELECT
                                              `departamentos`.`iddepartamentos`,
                                              `departamentos`.`departamento`
                                            FROM
                                              `departamentos`");
        $sucursales = Sucursal::all();


        return view('usuarios', compact('perfiles','nacionalidades','departamentos','sucursales'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function guardar(ValidacionUsuario $request)
    {
//    public function guardar(Request $request)
//    {

        $errores = array();
        $fechaNacimiento = $request->input('nuevaFechaNacimiento');
        $edad = $this->obtenerEdad($fechaNacimiento);
        if ($edad < 18){
           array_push($errores,"El usuario no puede ser menor de edad");
            return redirect('usuario/usuario')->withErrors($errores);
        }

        if ($request->input("nuevaFoto")!= null) {

            list($ancho, $alto) = getimagesize($_FILES["nuevaFoto"]["tmp_name"]);

            $nuevoAncho = 500;
            $nuevoAlto = 500;
            /*========================================================================
			DIRECTORIO DONDE SE GUARDARA LA FOTO DEL USUARIO            =
			========================================================================*/
            $usuario = $request->input('usuario');
            $directorio = public_path().'/assets/lte/img/usuarios/'.$usuario;
            File::makeDirectory($directorio, $mode = 0755, true, true);
            /*=================================
			SI ES JPG            =
			=================================*/
            if($_FILES["nuevaFoto"]["type"] == "image/jpeg") {
                /*=======================================================
                GUARDAR IMAGEN EN EL DIRECTORIO            =
                =======================================================*/
                $aleatorio = mt_rand(100,999);
                $ruta = $directorio."/".$aleatorio.".jpg";
                $origen = imagecreatefromjpeg($_FILES["nuevaFoto"]["tmp_name"]);
                $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                imagejpeg($destino, $ruta);
                $rutabd= '/assets/lte/img/usuarios/'.$usuario."/".$aleatorio.".jpg";
            }
            /*=================================
			SI ES PNG           =
			=================================*/
            if($_FILES["nuevaFoto"]["type"] == "image/png") {
                /*=======================================================
                GUARDAR IMAGEN EN EL DIRECTORIO            =
                =======================================================*/
                $aleatorio = mt_rand(100,999);
                $ruta = $directorio."/".$aleatorio.".png";
                $origen = imagecreatefrompng($_FILES["nuevaFoto"]["tmp_name"]);
                $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                imagepng($destino, $ruta);
                $rutabd= '/assets/lte/img/usuarios/'.$usuario."/".$aleatorio.".png";
            }
        }else{
            /*========================================================================
			DIRECTORIO DONDE SE GUARDARA LA FOTO DEL USUARIO            =
			========================================================================*/
            $usuario = $request->input('usuario');
            $directorio = str_replace("\\","/",public_path()).'/assets/lte/img/usuarios/'.$usuario;
            File::makeDirectory($directorio, $mode = 0755, true, true);
            $imagenDefault = str_replace("\\","/",public_path()).'/assets/lte/img/default/default.png';
            $directorio = str_replace("\\","/",public_path()).'/assets/lte/img/usuarios/'.$usuario.'/default.png';
            copy($imagenDefault,$directorio);
            $rutabd='/assets/lte/img/usuarios/'.$usuario."/default.png";
        }
        $clave = Hash::make($request->contrasena);

        $usuario = new Usuario();
        $usuario->nombreusuario = $request->nombre;
        $usuario->apellidousuario = $request->apellido;
        $usuario->cedulausuario = $request->cedula;
        $usuario->direccionusuario = $request->direccion;
        $usuario->celularusuario = $request->nuevoTelefono;
        $usuario->fechanacusuario = $this->fechaParaBD($request->nuevaFechaNacimiento);
        $usuario->usuario = $request->usuario;
        $usuario->passusuario = $clave;
        $usuario->fotousuario = $rutabd;
        $usuario->estado = 1;
        $usuario->fechaingresousuario = $this->obtenerHoyBD();
        $usuario->idnacionalidad = $request->idnacionalidad;
        $usuario->idperfil = $request->nuevoPerfil;
        $usuario->idciudades = $request->idciudad;
        $usuario->iddepartamentos = $request->idDepartamento;
        $usuario->sucursalidsucursal = $request->nuevaSucursal;
        $usuario->save();
        return redirect('usuarios/abm-usuarios')->with('mensaje','Usuario registrado correctamente');



    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function mostrar()
    {
       $usuarios= db::select('SELECT
                                      `usuarios`.`idusuarios`,
                                      `usuarios`.`nombreusuario`,
                                      `usuarios`.`apellidousuario`,
                                      `usuarios`.`usuario`,
                                      `usuarios`.`fotousuario`,
                                      `perfiles`.`perfil`,
                                      `usuarios`.`estado`,
                                      Date_Format(`usuarios`.`fechaingresousuario`, "%d-%m-%Y") AS `fecha`,
                                      Date_Format(`usuarios`.`ultimologinusuario`, "%d-%m-%Y %H:%i:%s") AS
                                    `fechaLogin`,
                                      `sucursales`.`nombresucursal`
                                    FROM
                                      `usuarios`
                                      INNER JOIN `perfiles` ON `usuarios`.`idperfil` = `perfiles`.`idperfil`
                                      INNER JOIN `sucursales` ON `sucursales`.`idsucursales` =
                                    `usuarios`.`sucursalidsucursal`');



        return view('abmUsuarios', compact('usuarios'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $usuario = db::select('SELECT
                                      `usuarios`.`idusuarios`,
                                      `usuarios`.`nombreusuario`,
                                      `usuarios`.`apellidousuario`,
                                      `usuarios`.`cedulausuario`,
                                      `usuarios`.`direccionusuario`,
                                      `usuarios`.`celularusuario`,
                                      DATE_FORMAT(`usuarios`.`fechanacusuario`,"%d%m%Y") AS fecha,
                                      `usuarios`.`usuario`,
                                      `usuarios`.`fotousuario`,
                                      `usuarios`.`idnacionalidad`,
                                      `usuarios`.`idperfil`,
                                      `usuarios`.`iddepartamentos`,
                                      `usuarios`.`idciudades`
                                    FROM
                                      `usuarios`
                                    WHERE
                                      `usuarios`.`idusuarios` ='.$id);
        $nacionalidades = db::select("SELECT
                                              `nacionalidades`.`idnacionalidad`,
                                              `nacionalidades`.`nacionalidad`
                                            FROM
                                              `nacionalidades`");
        $perfiles = db::select("SELECT
                                          `perfiles`.`idperfil`,
                                          `perfiles`.`perfil`
                                        FROM
                                          `perfiles`");
        $departamentos = db::select("SELECT
                                              `departamentos`.`iddepartamentos`,
                                              `departamentos`.`departamento`
                                            FROM
                                              `departamentos`");
        $ciudades = db::select("SELECT
                                          `ciudades`.`idciudades`,
                                          `ciudades`.`ciudad`,
                                          `ciudades`.`iddepartamentos`
                                        FROM
                                          `ciudades`
                                        WHERE
                                          `ciudades`.`iddepartamentos` =".$usuario[0]->iddepartamentos);

        return view('actualizarUsuario',compact('usuario','nacionalidades','perfiles',
            'departamentos','ciudades'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function actualizar(ValidacionUsuarioActualizar $request)
    {
        $errores = array();
        $fechaNacimiento = $request->input('nuevaFechaNacimiento');
        $edad = $this->obtenerEdad($fechaNacimiento);
        if ($edad < 18){
            array_push($errores,"El usuario no puede ser menor de edad");
            return redirect()->back()->withErrors($errores);
        }

//        if ($request->input("nuevaFoto")!= null) {
        if($request->hasFile('nuevaFoto')){

            list($ancho, $alto) = getimagesize($_FILES["nuevaFoto"]["tmp_name"]);

            $nuevoAncho = 500;
            $nuevoAlto = 500;
            /*========================================================================
			DIRECTORIO DONDE SE GUARDARA LA FOTO DEL USUARIO            =
			========================================================================*/
            $usuario = $request->input('usuario');
            $directorio = public_path().$request->input('rutaFoto');
            unlink($directorio);
            /*=================================
			SI ES JPG            =
			=================================*/
            if($_FILES["nuevaFoto"]["type"] == "image/jpeg") {
                /*=======================================================
                GUARDAR IMAGEN EN EL DIRECTORIO            =
                =======================================================*/
                $aleatorio = mt_rand(100,999);
                $ruta = public_path().'/assets/lte/img/usuarios/'.$usuario."/".$aleatorio.".jpg";
                $origen = imagecreatefromjpeg($_FILES["nuevaFoto"]["tmp_name"]);
                $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                imagejpeg($destino, $ruta);
                $rutabd= '/assets/lte/img/usuarios/'.$usuario."/".$aleatorio.".jpg";

            }
            /*=================================
			SI ES PNG           =
			=================================*/
            if($_FILES["nuevaFoto"]["type"] == "image/png") {
                /*=======================================================
                GUARDAR IMAGEN EN EL DIRECTORIO            =
                =======================================================*/
                $aleatorio = mt_rand(100,999);
                $ruta = public_path().'/assets/lte/img/usuarios/'.$usuario."/".$aleatorio.".png";
                $origen = imagecreatefrompng($_FILES["nuevaFoto"]["tmp_name"]);
                $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                imagepng($destino, $ruta);
                $rutabd= '/assets/lte/img/usuarios/'.$usuario."/".$aleatorio.".png";

            }
        }else{
            $rutabd=$request->input('rutaFoto');
        }

        $idUsu = $request->input('idUsuario');
        $nombre = $request->input('nombre');
        $apellidoUsu = $request->input('apellido');
        $idPErfilUsu = $request->input('idPErfil');
        $cedulaUsu = $request->input('cedula');
        $telefonoUsu = $request->input('nuevoTelefono');
        $fechaNacUsu = $this->fechaParaBD($request->input('nuevaFechaNacimiento'));
        $idNacUsu = $request->input('idnacionalidad');
        $idDepartamentoUsu = $request->input('idDepartamento');
        $idCiudadUsu = $request->input('idciudad');
        $direccionUsu = $request->input('direccion');

        Usuario::where('idusuarios',$idUsu)->update(array('nombreusuario'=>$nombre,'apellidousuario'=>$apellidoUsu,
        'idperfil'=>$idPErfilUsu, 'cedulausuario'=>$cedulaUsu, 'celularusuario'=>$telefonoUsu,
            'fechanacusuario'=>$fechaNacUsu, 'idnacionalidad'=>$idNacUsu,  'iddepartamentos'=>$idDepartamentoUsu,
            'idciudades'=>$idCiudadUsu, 'direccionusuario'=>$direccionUsu, 'fotousuario'=>$rutabd));
        return redirect('usuarios/abm-usuarios')->with('mensaje','Usuario actualizado con exito');




    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function eliminarUsuario($id)
    {
        try {

            // Conseguimos el objeto
            $usuario=Usuario::where('idusuarios', '=', $id)->first();

            // Lo eliminamos de la base de datos
            $eliminar = $usuario->delete();
            if($eliminar){
                return Redirect::back()->with('mensaje','Usuario eliminado con exito');
            }else{
                return Redirect::back()->withErrors('mensaje','Error al eliminar este usuario');
            }
        }catch (\Illuminate\Database\QueryException $e){
            return Redirect::back()->withErrors(['No se puede eliminar este usuario. Ya esta siendo utilizado/a']);
        }

    }
}
