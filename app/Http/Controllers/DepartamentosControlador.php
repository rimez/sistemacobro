<?php

namespace App\Http\Controllers;

use App\Departamentos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DepartamentosControlador extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function guardar(Request $request)
    {
        $validadorDepatamento = Validator::make($request->all(), [
            'nuevoNombre' => 'required|min:3|max:45|regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚÜü ]+$/|unique:departamentos,departamento'],
            [
                'nuevoNombre.required' => 'El campo nombre departamento es requerido',
                'nuevoNombre.min' => 'El campo nombre departamento debe contener como minimo 3 caracteres',
                'nuevoNombre.max' => 'El campo nombre departamento debe contener como maximo 100 caracteres',
                'nuevoNombre.regex' => 'El campo nombre departamento no puede estar vacío o contener caracteres especiales',
                'nuevoNombre.unique' => 'El nombre de departamento ya esta asignado a otro departamento'
            ]);

        if ($validadorDepatamento->fails()) {
            return redirect('usuario/usuario')
                ->withErrors($validadorDepatamento)
                ->withInput();
        }else{
            $departamento = new Departamentos();
            $departamento->departamento = $request->nuevoNombre;
            $departamento->save();
            return redirect('usuario/usuario')->with('mensaje','Departamento registrado correctamente');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function mostrar($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function eliminar($id)
    {
        //
    }
}
