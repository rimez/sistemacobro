<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\PlanesCredito;
use App\Sucursal;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Psy\Util\Json;

class PlanesControladorControlador extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $planes = PlanesCredito::all();
        return view('abmPlanesCredito', compact('planes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function guardar(Request $request)
    {
      //  dd($request->all());
        $validadorPlan = Validator::make($request->all(), [

            'nuevoMonto' => 'required|max:9|min:6|regex:/^[0-9]+$/',
            'nuevoGastAdm' => 'required|max:6|min:5|regex:/^[0-9]+$/',
            'nuevoPlazo' => 'required|max:2|min:1|regex:/^[0-9]+$/',
            'nuevaTasaInteres' => 'required|max:2|min:1|regex:/^[0-9]+$/',
            'nuevoTipoPago' => 'required',


        ],

            [
                //monto
                'nuevoMonto.required' => 'El campo Monto es requerido',
                'nuevoMonto.min' => 'El campo Monto debe contener como minimo 6 caracteres',
                'nuevoMonto.max' => 'El campo Monto debe contener como maximo 9 caracteres',
                'nuevoMonto.regex' => 'El campo Monto no puede estar vacío o contener caracteres especiales',
                //gastos adm
                'nuevoGastAdm.required' => 'El campo Gastos Administrativos es requerido',
                'nuevoGastAdm.min' => 'El campo Gastos Administrativos debe contener como minimo 5 caracteres',
                'nuevoGastAdm.max' => 'El campo Gastos Administrativos debe contener como maximo 6 caracteres',
                'nuevoGastAdm.regex' => 'El campo Gastos Administrativos no puede estar vacío o contener caracteres especiales',
                //plazo
                'nuevoPlazo.required' => 'El campo Plazo es requerido',
                'nuevoPlazo.min' => 'El campo Plazo debe contener como minimo 1 caracteres',
                'nuevoPlazo.max' => 'El campo Plazo debe contener como maximo 2 caracteres',
                'nuevoPlazo.regex' => 'El campo Plazo no puede estar vacío o contener caracteres especiales',
                //tasa
                'nuevaTasaInteres.required' => 'El campo tasa de interes es requerido',
                'nuevaTasaInteres.min' => 'El campo tasa de interes debe contener como minimo 6 caracteres',
                'nuevaTasaInteres.max' => 'El campo tasa de interes debe contener como maximo 9 caracteres',
                'nuevaTasaInteres.regex' => 'El campo tasa de interes no puede estar vacío o contener caracteres especiales',
                //monto
                'nuevoTipoPago.required' => 'El campo Tipo de pago es requerido',

            ]);

        if ($validadorPlan->fails()) {
            return redirect()->back()->withErrors($validadorPlan)->withInput();
        }else{
            $plan = new PlanesCredito();
            $plan->montoplan = $request->nuevoMonto;
            $plan->plazoplan = $request->nuevoPlazo;
            $plan->interesplan = $request->nuevaTasaInteres;
            $plan->tipopagoplan = $request->nuevoTipoPago;
            $plan->estadoplan = 1;
            $plan->fechacreacionplan =date("Y-m-d H:i:s");
            $plan->gatosadmplan = $request->nuevoGastAdm;
            $guardado = $plan->save();
            if($guardado){
                return redirect()->back()->with('mensaje','Plan de creditos registrado correctamente');

            }else{
                return redirect()->back()->withErrors('mensaje','Error al guardar plan de creditos');
            }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function mostrar($id)
    {
        //
    }
    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View|string
     */
    public function obtnerGastos($idPlan)
    {
        $gasto = PlanesCredito::find($idPlan);

        return Json::encode($gasto);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function actualizar(Request $request)
    {
        //dd($request->all());
        $id = $request->idActual;
        $plan = PlanesCredito::find($id);
        $plan->estadoplan = $request->editarEstado;
        $guardado = $plan->save();
        if($guardado){
            return redirect()->back()->with('mensaje','Plan de creditos actualizado correctamente');

        }else{
            return redirect()->back()->withErrors('mensaje','Error al editar plan de creditos');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function eliminar($id)
    {
        try {

            // Conseguimos el objeto
            $plan=PlanesCredito::where('idplanescredito', '=', $id)->first();

            // Lo eliminamos de la base de datos
            $eliminar = $plan->delete();
            if($eliminar){
                return Redirect::back()->with('mensaje','Plan de credito eliminado con exito');
            }else{
                return Redirect::back()->withErrors('mensaje','Error al eliminar este plan de creditos');
            }

        }catch (\Illuminate\Database\QueryException $e){
            return Redirect::back()->withErrors(['No se puede eliminar esta plan de creditos. Ya esta siendo utilizado/a']);
        }
    }
}
