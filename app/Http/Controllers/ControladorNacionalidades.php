<?php

namespace App\Http\Controllers;

use App\Nacionalidades;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ControladorNacionalidades extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function guardar(Request $request)
    {
        $validadorNacionalidad = Validator::make($request->all(), [
            'nuevoNombre' => 'required|min:3|max:45|regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚÜü ]+$/|unique:departamentos,departamento'],
            [
                'nuevoNombre.required' => 'El campo nombre nacionalidad es requerido',
                'nuevoNombre.min' => 'El campo nombre nacionalidad debe contener como minimo 3 caracteres',
                'nuevoNombre.max' => 'El campo nombre nacionalidad debe contener como maximo 100 caracteres',
                'nuevoNombre.regex' => 'El campo nombre nacionalidad no puede estar vacío o contener caracteres especiales',
                'nuevoNombre.unique' => 'El nombre de nacionalidad ya esta asignada a otra nacionalidad'
            ]);

        if ($validadorNacionalidad->fails()) {
            return redirect('usuario/usuario')
                ->withErrors($validadorNacionalidad)
                ->withInput();
        }else{
            $nacionalidad = new Nacionalidades();
            $nacionalidad->nacionalidad = $request->nuevoNombre;
            $nacionalidad->save();
            return redirect('usuario/usuario')->with('mensaje','Nacionalidad registrada correctamente');
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function mostrar($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function eliminar($id)
    {
    }
}
