<?php

namespace App\Http\Controllers;

use App\Departamentos;
use App\Nacionalidades;
use App\Perfiles;
use http\Client\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Psy\Util\Json;

class independienteControlador extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($tabla)
    {
        $independientes = db::select("SELECT * FROM ".$tabla);

        switch ($tabla){
            case 'perfiles':
                $columnaid = 'idperfil';
                $columnaDescripcion = 'perfil';
                $datos =array(
                    'columnaDescripcion'=>'perfil',
                    'columnaid'=>'idperfil',
                    'titulo'=>"Perfiles",
                    'nomNatural'=>"perfil",
                    'tituloCabecera'=>'Administrar Perfiles',
                    'BotonPrincipal'=>'Agregar Perfil',
                    'cabeceraModal'=>'Agregar Perfil',
                    'iconoImputModal'=>'fa fa-user',
                    'botonGuardarModal'=>'Guardar Perfil',
                    'cabeceraModalEditar'=>'Editar Perfil',
                    'iconoImputModalEditar'=>'fa fa-user');
                return view('abmIndependiente', compact('independientes','datos','columnaid','columnaDescripcion','tabla'));
                break;
            case 'departamentos':
                $columnaid = 'iddepartamentos';
                $columnaDescripcion = 'departamento';
                $datos =array(
                    'columnaDescripcion'=>'departamento',
                    'columnaid'=>'iddepartamentos',
                    'titulo'=>"Departamentos",
                    'nomNatural'=>"departamento",
                    'tituloCabecera'=>'Administrar Departamentos',
                    'BotonPrincipal'=>'Agregar Departamento',
                    'cabeceraModal'=>'Agregar Departamento',
                    'iconoImputModal'=>'fa fa-map-marker',
                    'botonGuardarModal'=>'Guardar Departamento',
                    'cabeceraModalEditar'=>'Editar Departamento',
                    'iconoImputModalEditar'=>'fa fa-map-marker');
                return view('abmIndependiente', compact('independientes','datos','columnaid','columnaDescripcion','tabla'));
                break;
            case 'nacionalidades':
                $columnaid = 'idnacionalidad';
                $columnaDescripcion = 'nacionalidad';
                $datos =array(
                    'columnaDescripcion'=>'nacionalidad',
                    'columnaid'=>'idnacionalidad',
                    'titulo'=>"Nacionalidades",
                    'nomNatural'=>"nacionalidad",
                    'tituloCabecera'=>'Administrar Nacionalidades',
                    'BotonPrincipal'=>'Agregar Nacionalidad',
                    'cabeceraModal'=>'Agregar Nacionalidad',
                    'iconoImputModal'=>'fa fa-globe',
                    'botonGuardarModal'=>'Guardar Nacionalidad',
                    'cabeceraModalEditar'=>'Editar Nacionalidad',
                    'iconoImputModalEditar'=>'fa fa-globe');


                return view('abmIndependiente', compact('independientes','datos','columnaid','columnaDescripcion','tabla'));

                break;
            case 'profesiones':
                $columnaid = 'idprofesion';
                $columnaDescripcion = 'profesion';
                $datos =array(
                    'columnaDescripcion'=>'profesion',
                    'columnaid'=>'idprofesion',
                    'titulo'=>"Profesiones",
                    'nomNatural'=>"profesion",
                    'tituloCabecera'=>'Administrar Profesiones',
                    'BotonPrincipal'=>'Agregar Profesion',
                    'cabeceraModal'=>'Agregar Profesion',
                    'iconoImputModal'=>'fa fa-briefcase',
                    'botonGuardarModal'=>'Guardar Profesion',
                    'cabeceraModalEditar'=>'Editar Profesion',
                    'iconoImputModalEditar'=>'fa fa-briefcase');


                return view('abmIndependiente', compact('independientes','datos','columnaid','columnaDescripcion','tabla'));

                break;
            case 'estados':
                $columnaid = 'idestados';
                $columnaDescripcion = 'estado';
                $datos =array(
                    'columnaDescripcion'=>'estado',
                    'columnaid'=>'idestados',
                    'titulo'=>"Estados",
                    'nomNatural'=>"estado",
                    'tituloCabecera'=>'Administrar Estados',
                    'BotonPrincipal'=>'Agregar Estado',
                    'cabeceraModal'=>'Agregar Estado',
                    'iconoImputModal'=>'fa fa-sign-in',
                    'botonGuardarModal'=>'Guardar Estado',
                    'cabeceraModalEditar'=>'Editar Estado',
                    'iconoImputModalEditar'=>'fa fa-sign-in');


                return view('abmIndependiente', compact('independientes','datos','columnaid','columnaDescripcion','tabla'));

                break;
            case 'tipoprestamo':
                $columnaid = 'idtipoprestamo';
                $columnaDescripcion = 'nombretipoprestamo';
                $datos =array(
                    'columnaDescripcion'=>'nombretipoprestamo',
                    'columnaid'=>'idtipoprestamo',
                    'titulo'=>"Tipo Prestamo",
                    'nomNatural'=>"Tipo Prestamo",
                    'tituloCabecera'=>'Administrar Tipos de Prestamos',
                    'BotonPrincipal'=>'Agregar Tipo Prestamo',
                    'cabeceraModal'=>'Agregar Tripo Prestamo',
                    'iconoImputModal'=>'fa fa-th',
                    'botonGuardarModal'=>'Guardar Tipo Prestamo',
                    'cabeceraModalEditar'=>'Editar Tipo Prestamo',
                    'iconoImputModalEditar'=>'fa fa-th');


                return view('abmIndependiente', compact('independientes','datos','columnaid','columnaDescripcion','tabla'));

                break;

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function guardar(Request $request)
    {
        $nombre = $request->input('nuevoNombre');
        $tabla = $request->input('tablaind');
        $nomNatural = $request->input('nomNatural');
        $colDescrip = $request->input('columnDescrip');
        $validador = Validator::make($request->all(),[
            'nuevoNombre' => 'required|min:3|max:45|regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚÜü ]+$/|unique:'.$tabla.','.$colDescrip.''],
               [
                   'nuevoNombre.required' => 'El campo nombre '.$nomNatural.' es requerido',
                   'nuevoNombre.min' => 'El campo nombre '.$nomNatural.' debe contener como minimo 3 caracteres',
                   'nuevoNombre.max' => 'El campo nombre '.$nomNatural.' debe contener como maximo 100 caracteres',
                   'nuevoNombre.regex' => 'El campo nombre '.$nomNatural.' no puede estar vacío/a o contener caracteres especiales',
                   'nuevoNombre.unique' => 'El nombre de '.$nomNatural.' '.$nombre.' ya esta asignado/a a otro/a '.$nomNatural.''
                ]);
                if ($validador->fails()) {
                    return redirect()->back()->withErrors($validador)->withInput();
                }else{
                    $insertar = db::insert("INSERT INTO $tabla ($colDescrip) VALUES ('$nombre')");
                    return redirect()->back()->with('mensaje',$nomNatural.' registrado/a correctamente');
                }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(Request $request)
    {
        $tabla = $request->input('tablainde');
        $id = $request->input('idActual');
        $nomNatural = $request->input('nomNaturalAc');
        $colDescrip = $request->input('columnDescripAc');
        $colId = $request->input('columnId');
        $nombre = $request->input('nuevoNombre');
        $validadorAc = Validator::make($request->all(), [
                    'nuevoNombre' => 'required|min:3|max:45|regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚÜü ]+$/
                    |unique:'.$tabla.','.$colDescrip.','.$id.','.$colId.''],
                    [
                        'nuevoNombre.required' => 'El campo nombre '.$nomNatural.' es requerido',
                        'nuevoNombre.min' => 'El campo nombre '.$nomNatural.' debe contener como minimo 3 caracteres',
                        'nuevoNombre.max' => 'El campo nombre '.$nomNatural.' debe contener como maximo 100 caracteres',
                        'nuevoNombre.regex' => 'El campo nombre '.$nomNatural.' no puede estar vacío/a o contener caracteres especiales',
                        'nuevoNombre.unique' => 'El nombre de '.$nomNatural.' '.$nombre.' ya esta asignado/a a otro/a '.$nomNatural.''
                    ]);
                if ($validadorAc->fails()) {
                    return redirect()->back()->withErrors($validadorAc)->withInput();
                } else {
                    $actualizar = db::update("UPDATE $tabla SET $colDescrip = '".$nombre."' WHERE $colId = $id");
                    return redirect()->back()->with('mensaje', 'Nombre de '.$nomNatural.' actualizado/a correctamente');
                }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function eliminar(Request $request)
    {
       $tabla = $request->input('tabla');
       // return redirect()->back()->with('mensaje', 'Nombre de Nacionalidad actualizada correctamente');
        if($tabla == 'nacionalidades'){
            return redirect()->back()->with('mensaje', 'Nombre de Nacionalidad actualizada correctamente');
        }else{
            return Response::json([error],404);
        }

    }
}
