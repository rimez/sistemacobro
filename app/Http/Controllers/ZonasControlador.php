<?php

namespace App\Http\Controllers;

use App\Departamentos;
use App\Sucursal;
use App\Usuario;
use App\Zona;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class ZonasControlador extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $zonas = Zona::join("usuarios","usuarios.idusuarios", "=", "zonacobranza.usuarios_idusuarios")
            ->join("sucursales","usuarios.sucursalidsucursal", "=", "sucursales.idsucursales")
            ->select("zonacobranza.idzonacobranza","sucursales.nombresucursal", "usuarios.idusuarios", "usuarios.usuario", "usuarios.fotousuario","zonacobranza.nombrezona")
            ->get();
        $usuarios = Usuario::all();
        return view('abmZonas', compact('zonas','usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function guardar(Request $request)
    {
        //dd($request->all());
        $validadorZona = Validator::make($request->all(), [
            'nuevoNombre' => 'required|min:3|max:100|regex:/^[#\.\/\-a-zA-Z0-9 ,]+$/|unique:zonacobranza,nombrezona',
            'idUsuario' => 'required|unique:zonacobranza,usuarios_idusuarios'],
            [
                'nuevoNombre.required' => 'El campo nombre de la zona es requerido',
                'nuevoNombre.min' => 'El campo nombre de la zona debe contener como minimo 3 caracteres',
                'nuevoNombre.max' => 'El campo nombre de la zona debe contener como maximo 100 caracteres',
                'nuevoNombre.regex' => 'El campo nombre de la zona no puede estar vacío o contener caracteres especiales',
                'nuevoNombre.unique' => 'El nombre de la zona ya esta asignado a otra zona',
                'idUsuario.unique' => 'El usuario seleccionado ya tiene una zona de cobranza asignada'
            ]);

        if ($validadorZona->fails()) {
            return redirect()->back()
                ->withErrors($validadorZona)
                ->withInput();
        }else{
            $zona = new Zona();
            $zona->nombrezona = $request->nuevoNombre;
            $zona->usuarios_idusuarios = $request->idUsuario;
            $guardado = $zona->save();
            if ($guardado){
                return redirect()->back()->with('mensaje','Zona de cobranza registrada correctamente');
            }else{
                return redirect()->back()->withErrors('mensaje','Error al registrar zona de cobranza');
            }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function mostrar($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(Request $request)
    {
       // dd($request->all());
        $validadorZona = Validator::make($request->all(), [
            'editarNombre' => 'required|min:3|max:100|regex:/^[#\.\/\-a-zA-Z0-9 ,]+$/|unique:zonacobranza,nombrezona,'.$request->input('idActual').',idzonacobranza',
            'idUsuario' => 'required|unique:zonacobranza,usuarios_idusuarios,'.$request->input('idActual').',idzonacobranza'],
            [
                'nuevoNombre.required' => 'El campo nombre de la zona es requerido',
                'nuevoNombre.min' => 'El campo nombre de la zona debe contener como minimo 3 caracteres',
                'nuevoNombre.max' => 'El campo nombre de la zona debe contener como maximo 100 caracteres',
                'nuevoNombre.regex' => 'El campo nombre de la zona no puede estar vacío o contener caracteres especiales',
                'nuevoNombre.unique' => 'El nombre de la zona ya esta asignado a otra zona',
                'idUsuario.unique' => 'El usuario seleccionado ya tiene una zona de cobranza asignada'
            ]);

        if ($validadorZona->fails()) {
            return redirect()->back()
                ->withErrors($validadorZona)
                ->withInput();
        }else{
            $id = $request->idActual;
            $zona = Zona::find($id);
            $zona->nombrezona = $request->editarNombre;
            $zona->usuarios_idusuarios = $request->idUsuario;
            $guardado = $zona->save();
            if ($guardado){
                return redirect()->back()->with('mensaje','Zona de cobranza actualizada correctamente');
            }else{
                return redirect()->back()->withErrors('mensaje','Error al actualizar zona de cobranza');
            }

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function eliminar($id)
    {
        try {

            // Conseguimos el objeto
            $zona=Zona::where('idzonacobranza', '=', $id)->first();

            // Lo eliminamos de la base de datos
            $eliminar = $zona->delete();
            if($eliminar){
                return Redirect::back()->with('mensaje','Sucursal eliminada con exito');
            }else{
                return Redirect::back()->withErrors('mensaje','Error al eliminar esta sucursal');
            }


        }catch (\Illuminate\Database\QueryException $e){
            return Redirect::back()->withErrors(['No se puede eliminar esta sucursal. Ya esta siendo utilizado/a']);
        }
    }

}
