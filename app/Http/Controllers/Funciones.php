<?php

namespace App\Http\Controllers;

use DB;
use Session;

trait Funciones
{
    public function  sesion_iniciada(){
        if (session()->has('nombre_usuario')){
            return true;
        }else{
            return false;
        }
    }
    /**
     * Fecha de base de datos
     *
     * @param f fecha en formato yyyy-MM-dd
     * @return Fecha en formato dd-MM-YYYY
     */
    public function fechaDeBD($f){

        if($f==null || $f==""){

            return "";
        }

        return $fecha = substr($f, 8, 10).'-'.substr($f, 5, 2).'-'.substr($f, 0, 4);

    }
    /**
     * Fecha para base de datos
     *
     * @param f fecha en formato dd-MM-yyyy
     * @return Fecha en formato para base de datos
     */
    public static function fechaParaBD($f){

        if($f==null || $f==""){

            return "";
        }

        return $fecha = substr($f, 6, 10).'-'.substr($f, 3, 2).'-'.substr($f, 0, 2);

    }
    /**
     * Calcular la edad entre una fecha y la fecha actual
     *
     * @param fechaNacimiento fecha en formato dd-MM-yyyy
     * @return edad
     */
    public static function obtenerEdad($fechaNacimiento){

        $fechaHoy = date("d-m-Y");

        return Funciones::cantidadAnhosEntreDosFechas($fechaNacimiento, $fechaHoy);
    }
    /**
     * Obtener fecha actual
     * @return
     */
    public static function obtenerHoy(){
        return date("d-m-Y");
    }
    public static function obtenerHoyBD(){
        return date("Y-m-d");
    }
    /**
     * Obtener hoy con hora
     * @return
     */
    public static function obtenerHoyConHora(){
        return $hoy = date("Y-m-d H:i:s");
    }
    /**
     * Calcular la cantidad de años entre dos fechas
     *
     * @param f fecha en formato dd-MM-yyyy
     * @param f2 fecha en formato dd-MM-yyyy
     * @return cantidad de anios
     */
    public static function cantidadAnhosEntreDosFechas($f, $f2){

        $diaDesde =  substr($f, 0, 2);
        $mesDesde =  substr($f, 3, 2);
        $anioDesde = substr($f, 6, 10);

        $diaHasta =  substr($f2, 0, 2);
        $mesHasta =  substr($f2, 3, 2);
        $anioHasta = substr($f2, 6, 10);

       // dd($anioHasta.' '.$anioDesde);

        $anio = ($anioHasta - $anioDesde);

        if ($mesHasta < $mesDesde || ($mesHasta == $mesDesde && $diaHasta < $diaDesde)) {

            $anio -= 1;
        }

        return $anio;
    }
}
