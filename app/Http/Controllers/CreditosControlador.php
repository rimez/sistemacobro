<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\Creditos;
use App\PlanesCredito;
use App\TipoPrestamo;
use App\Usuario;
use App\Zona;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Funciones;
use Psy\Util\Json;

class CreditosControlador extends Controller
{
    use Funciones;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $creditos = Creditos::join("clientes", "clientes.idcliente","=", "creditos.clientes_idcliente")
            ->join("planescredito","planescredito.idplanescredito","=","creditos.planescredito_idplanescredito")
            ->join("usuarios","usuarios.idusuarios", "=","creditos.usuarios_idusuarios")
            ->join("zonacobranza", "usuarios.idusuarios","=","zonacobranza.usuarios_idusuarios")
            ->select("creditos.idcreditos","clientes.idcliente","clientes.nombrecliente","clientes.apellidocliente",
                    "creditos.creditofechasolicitud","planescredito.montoplan","creditos.creditogarantia",
                "creditos.creditoestadosolicitud","zonacobranza.nombrezona")->get();

        return view('creditos', compact('creditos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function guardar(Request $request)
    {
       //dd($request->all());
        $validadorCredito = Validator::make($request->all(), [
            'idZona' => 'required',
            'idCliente' => 'required',
            'idGarantia' => 'required',
            'idPlan' => 'required',

        ],
            [
                'idZona.required' => 'El campo zona es requerido',
                'idCliente.required' => 'El campo Cliente es requerido',
                'idGarantia.required' => 'El campo garantia es requerido',
                'idPlan.required' => 'El campo plan requerido',
            ]);
        if ($validadorCredito->fails()) {
            return redirect()->back()
                ->withErrors($validadorCredito)
                ->withInput();
        }else{
            $errores = array();
            $idCliente = $request->input('idCliente');
            $idGarante = $request->input('idGarante');
            if ($idCliente == $idGarante){
                array_push($errores,"El Cliente y el garante no pueden ser la misma persona");
            }
            $idGarantia = $request->input('idGarantia');
            if($idGarantia == "0" and $idGarante==null){
                array_push($errores,"Para garantia solidaria debe de seleccionar un garante");
            }
            if($errores){
                return redirect()->back()
                    ->withErrors($errores)
                    ->withInput();
            }
            $credito = new Creditos();
            $credito->clientes_idcliente = $request->idCliente;
            $credito->creditogastosadm= $request->gastosadm;
            $credito->creditofechasolicitud= $this->obtenerHoyBD();
            $credito->creditogarantia= $request->idGarantia;
            $credito->creditonomgarante= $request->nuevoApeyNomGar;
            $credito->creditocigarante= $request->nuevaciGar;
            $credito->creditoidgarante= $request->idGarante;
            $credito->creditoestadosolicitud= "SOLICITADO";
            $credito->tipoprestamo_idtipoprestamo= $request->tipoPrest;
            $credito->usuarios_idusuarios= $request->idZona;
            $credito->planescredito_idplanescredito= $request->idPlan;
            $guardado = $credito->save();
            if($guardado){
                return redirect()->back()->with('mensaje','Credito registrado correctamente');

            }else{
                return redirect()->back()->withErrors('mensaje','Error al guardar credito');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function mostrar()
    {
        $clientes = Cliente::all();
        $tiposPrestamos = TipoPrestamo::all();
        $usuarios = Usuario::all();
        $planes = PlanesCredito::all();
        $zonas = Zona::all();
        return view('abmCreditos', compact('clientes','tiposPrestamos','usuarios','planes','zonas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(Request $request)
    {
        $id = $request->id;
        $estado = "APROBADO";
        $credito = Creditos::find($id);
        $credito->creditoestadosolicitud = $estado;
        $guardado = $credito->save();
        if ($guardado){
            return Json::encode($id);
        }else{

        }
    }
    public function rechazar(Request $request)
    {
        $id = $request->id;
        $estado = "RECHAZADO";
        $credito = Creditos::find($id);
        $credito->creditoestadosolicitud = $estado;
        $guardado = $credito->save();
        if ($guardado){
            return Json::encode($id);
        }else{

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function eliminar($id)
    {
        //
    }
}
