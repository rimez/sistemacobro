<?php

namespace App\Http\Controllers;

use App\Ciudades;
use App\Cliente;
use App\Departamentos;
use App\Http\Controllers\Funciones;
use App\Http\Requests\ValidacionCliente;
use App\Http\Requests\ValidacionClienteActualizar;
use App\Nacionalidades;
use App\Ocupaciones;
use App\Profesiones;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Psy\Util\Json;

class ClientesControlador extends Controller
{
    use Funciones;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $clientes = Cliente::all();
        return view('clientes', compact('clientes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardar(ValidacionCliente $request)
    {
        $errores = array();
        $fechaNacimiento = $request->input('nuevaFechaNacimiento');
        $edad = $this->obtenerEdad($fechaNacimiento);
        if ($edad < 18){
            array_push($errores,"El usuario no puede ser menor de edad");

        }
        if($errores){
            return redirect('clientes/abm-cliente')->withErrors($errores);
        }

        $cliente = new Cliente();
        $cliente->nombrecliente= $request->nombre;
        $cliente->apellidocliente= $request->apellido;
        $cliente->cedulacliente= $request->cedula;
        $cliente->fechanaccliente= $request->nuevaFechaNacimiento;
        $cliente->telefonocliente= $request->nuevoTelefono;
        $cliente->celularcliente= $request->nuevoCelular;
        $cliente->sexocliente= $request->nuevoSexo;
        $cliente->estadocivil= $request->nuevoEstadoCivil;
        $cliente->domiciliocliente= $request->direccion;
        $cliente->coordenadasdomicilio= $request->coordenadas;
        $cliente->cedulavenccliente= $this->fechaParaBD($request->nuevoVenCedula);
        $cliente->fechaingresocliente= $this->obtenerHoyConHora();
        $cliente->profesiones_idprofesion= $request->idprofesion;
        $cliente->estados_idestados= 1;
        $cliente->nacionalidades_idnacionalidad= $request->idnacionalidad;
        $cliente->ciudades_iddepartamentos= $request->idDepartamento;
        $cliente->ocupaciones_idocupaciones= $request->idOcupacion;
        $cliente->ingresopromcliente= $request->ingprom;
        $cliente->ciudades_idciudades = $request->idciudad;
        $cliente->save();
        return redirect('clientes/cliente')->with('mensaje','Cliente registrado correctamente');




    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function mostrar()
    {
        $nacionalidades = Nacionalidades::all();
        $profesiones = Profesiones::all();
        $departamentos = Departamentos::all();
        $ocupaciones = Ocupaciones::all();
        return view('abmClientes', compact('nacionalidades','profesiones','departamentos','ocupaciones'));
    }
    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function obtnerGarante($idCliente)
    {
        $cliente = Cliente::find($idCliente);

        return Json::encode($cliente);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editar($id)
    {
        $cliente = Cliente::find($id);
        $nacionalidades = Nacionalidades::all();
        $profesiones = Profesiones::all();
        $departamentos = Departamentos::all();
        $ocupaciones = Ocupaciones::all();
        $ciudades = Ciudades::all();
        return view('actualizarCliente', compact('cliente','nacionalidades','profesiones','departamentos',
            'ocupaciones','ciudades'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function actualizar(ValidacionClienteActualizar $request)
    {

        $errores = array();
        $fechaNacimiento = $request->input('nuevaFechaNacimiento');
        $edad = $this->obtenerEdad($fechaNacimiento);
        if ($edad < 18){
            array_push($errores,"El usuario no puede ser menor de edad");

        }
        if($errores){
            return redirect()->back()->withErrors($errores);
        }
        $idCliente = $request->idCliente;
        $nombrecliente= $request->nombre;
        $apellidocliente= $request->apellido;
        $cedulacliente= $request->cedula;
        $fechanaccliente= $this->fechaParaBD($request->nuevaFechaNacimiento);
        $telefonocliente= $request->nuevoTelefono;
        $celularcliente= $request->nuevoCelular;
        $sexocliente= $request->nuevoSexo;
        $estadocivil= $request->nuevoEstadoCivil;
        $domiciliocliente= $request->direccion;
        $coordenadasdomicilio= $request->coordenadas;
        $cedulavenccliente= $this->fechaParaBD($request->nuevoVenCedula);
        $profesiones_idprofesion= $request->idprofesion;
        $nacionalidades_idnacionalidad= $request->idnacionalidad;
        $ciudades_iddepartamentos= $request->idDepartamento;
        $ocupaciones_idocupaciones= $request->idOcupacion;
        $ingresopromcliente= $request->ingprom;
        $ciudades_idciudades = $request->idciudad;
        Cliente::where('idcliente',$idCliente)->update(array('nombrecliente'=>$nombrecliente,'apellidocliente'=>$apellidocliente,
            'cedulacliente'=>$cedulacliente,'fechanaccliente'=>$fechanaccliente,'telefonocliente'=>$telefonocliente,'celularcliente'=>$celularcliente,
            'sexocliente'=>$sexocliente,'estadocivil'=>$estadocivil,'domiciliocliente'=>$domiciliocliente,'coordenadasdomicilio'=>$coordenadasdomicilio,
            'cedulavenccliente'=>$cedulavenccliente,'profesiones_idprofesion'=>$profesiones_idprofesion,'nacionalidades_idnacionalidad'=>$nacionalidades_idnacionalidad,
            'ciudades_iddepartamentos'=>$ciudades_iddepartamentos,'ocupaciones_idocupaciones'=>$ocupaciones_idocupaciones,'ingresopromcliente'=>$ingresopromcliente,
            'ciudades_idciudades'=>$ciudades_idciudades));
        return redirect('clientes/cliente')->with('mensaje','Cliente actualizado correctamente');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function eliminarCliente($id)
    {
        try {

            // Conseguimos el objeto
            $cliente=Cliente::where('idcliente', '=', $id)->first();

            // Lo eliminamos de la base de datos
           $eliminar = $cliente->delete();
            if($eliminar){
                return Redirect::back()->with('mensaje','Cliente eliminado con exito');
            }else{
                return Redirect::back()->withErrors('mensaje','Error al eliminar este cliente');
            }


        }catch (\Illuminate\Database\QueryException $e){
            return Redirect::back()->withErrors(['No se puede eliminar este cliente. Ya esta siendo utilizado/a']);
        }
    }
}
