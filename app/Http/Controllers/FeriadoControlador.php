<?php

namespace App\Http\Controllers;

use App\Feriados;
use App\Sucursal;
use Illuminate\Http\Request;
use App\Http\Controllers\Funciones;
use Illuminate\Support\Facades\Validator;

class FeriadoControlador extends Controller
{
    use Funciones;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $feriados = Feriados::join("sucursales","sucursales.idsucursales","=","feriados.sucursales_idsucursales")
        ->select("feriados.idferiados",
            "feriados.fechaferiados",
            "feriados.descricionferiados",
            "feriados.sucursales_idsucursales",
            "sucursales.nombresucursal")->get();
        $sucursales = Sucursal::all();

        return view('abmFeriados',compact('feriados','sucursales'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Registro multiple para feriados nacionales
     *
     *
     * @return bool
     */
    public function guardarFeriadoSucursal($fecha,$descripcion,$idSucursal)
    {
        $feriado = new Feriados();
        $feriado->fechaferiados=$fecha;
        $feriado->descricionferiados=$descripcion;
        $feriado->sucursales_idsucursales=$idSucursal;
        $guardado =$feriado->save();
        return $guardado;
    }
    /**
     * Registro multiple para feriados nacionales
     *
     *
     * @return bool
     */
    public function guardarFeriadoNacional($fecha,$descripcion)
    {
        $sucursales = Sucursal::all();
        foreach ($sucursales as $sucursal){
            $feriado = new Feriados();
            $feriado->fechaferiados=$fecha;
            $feriado->descricionferiados=$descripcion;
            $feriado->sucursales_idsucursales=$sucursal->idsucursales;
            $guardado =$feriado->save();
        }
        return $guardado;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function guardar(Request $request)
    {
        //dd($request->all());
        $validadorFeriado = Validator::make($request->all(), [
            'idSucursal' => 'required',
            'nuevoNombre' => 'required',
            'nuevaFechaFeriado' => 'required'],

            [
                'idSucursal.required' => 'El campo sucursal es requerido',
                'nuevoNombre.required' => 'El campo descripcion de feriado es requerido',
                'nuevaFechaFeriado.required' => 'El campo fecha de feriado  es requerido',
            ]);
        if ($validadorFeriado->fails()) {
            return redirect()->back()
                ->withErrors($validadorFeriado)
                ->withInput();
        }else{
            $idSucursal=$request->idSucursal;
            $descripcion = $request->nuevoNombre;
            $fecha = $this->fechaParaBD($request->nuevaFechaFeriado);
            if ($idSucursal=="0"){
                if($this->guardarFeriadoNacional($fecha,$descripcion)){
                    return redirect()->back()->with('mensaje','Feriado Nacional registrado correctamente');
                }else{
                    return redirect()->back()->withErrors('mensaje','Ocurrio un error al registrar Feriado Nacional');
                }
            }else{
                if($this->guardarFeriadoSucursal($fecha,$descripcion,$idSucursal)){
                    return redirect()->back()->with('mensaje','Feriado registrado correctamente');
                }else{
                    return redirect()->back()->withErrors('mensaje','Error al guardar Feriado');
                }
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function mostrar($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function eliminar($id)
    {
        //
    }
}
