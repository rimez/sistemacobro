<?php

namespace App\Http\Controllers;

use App\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class Login extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    return view('login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function ingreso(Request $request)
    {
        $resultado = DB::select("SELECT
                                          count(*) `cant`,
                                          `usuarios`.`idusuarios`,
                                          `usuarios`.`nombreusuario`,
                                          `usuarios`.`apellidousuario`,
                                          `usuarios`.`usuario`,
                                          `usuarios`.`fotousuario`,
                                          `usuarios`.`passusuario`
                                        FROM
                                          `usuarios`
                                        WHERE
                                          `usuarios`.`usuario` = '".$request->usuario."'
                                        GROUP BY
                                            `usuarios`.`idusuarios`,
                                          `usuarios`.`nombreusuario`,
                                          `usuarios`.`apellidousuario`,
                                          `usuarios`.`usuario`,
                                          `usuarios`.`fotousuario`,
                                          `usuarios`.`passusuario`");



        if($resultado[0]->cant>0){
            $contra = $request->contra;
            $encriptada = $resultado[0]->passusuario;
           if(Hash::check($contra, $encriptada)){
               session(['idusuarios'=>$resultado[0]->idusuarios,
                        'usuario'=>$resultado[0]->usuario,
                        'nombreusuario'=>$resultado[0]->nombreusuario,
                        'apellidousuario'=>$resultado[0]->apellidousuario,
                        'fotousuario'=>$resultado[0]->fotousuario]);
               $hoy = date("Y-m-d H:i:s");
               Usuario::where('idusuarios',$resultado[0]->idusuarios)->update(array('ultimologinusuario'=>$hoy));
               return redirect('inicio');
           }else{
               dd("no entro");
           }

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function cerrar_sesion()
    {
        session()->flush();
        return redirect('/');
    }
}
