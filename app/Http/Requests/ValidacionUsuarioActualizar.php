<?php

namespace App\Http\Requests;

use http\Env\Request;
use Illuminate\Foundation\Http\FormRequest;

class ValidacionUsuarioActualizar extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       //dd($this->all());
        return [
            'nombre' => 'required|min:3|max:45|regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚÜü ]+$/',
            'apellido' => 'required|min:3|max:45|regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚÜü ]+$/',
            'idPErfil' => 'required',
            'cedula' => 'required|min:4|max:45|unique:usuarios,cedulausuario,'.$this->input('idUsuario').',idusuarios|regex:/^[a-zA-Z0-9]+$/',
            'nuevoTelefono' => 'required|max:45|regex:/^[()\-0-9 ]+$/',
            'direccion' => 'required|max:100|regex:/^[#\.\/\-a-zA-Z0-9 ,]+$/',
            'nuevaFechaNacimiento' => 'required|regex:/^[0-9-]+$/',
            'nuevaFoto' => 'mimes:jpeg,png',
            'idciudad' => 'required',
            'idnacionalidad' => 'required',
            'idDepartamento' => 'required',

        ];
    }
    public function messages()
    {
        return [
            //nombre
            'nombre.required' => 'El campo nombre es requerido',
            'nombre.max'  => 'El campo nombre no puede sobrepasar 45 caracteres',
            'nombre.min' => 'El campo nombre debe tener como minimo 3 caracteres',
            'nombre.regex' => 'El campo nombre no puede estar vacío o contener caracteres especiales',
            //apellido
            'apellido.required' => 'El campo apellido es requerido',
            'apellido.max'  => 'El campo apellido no puede sobrepasar 45 caracteres',
            'apellido.min' => 'El campo apellido debe tener como minimo 3 caracteres',
            'apellido.regex' => 'El campo apellido no puede estar vacío o contener caracteres especiales',
            //cedula
            'cedula.required' => 'El campo cedula es requerido',
            'cedula.max'  => 'El campo cedula no puede sobrepasar 45 caracteres',
            'cedula.min' => 'El campo cedula debe tener como minimo 4 caracteres',
            'cedula.unique' => 'La cedula de usuario ya esta asignado a otro usuario',
            'cedula.regex' => 'El campo cedula no puede estar vacío o contener caracteres especiales',
            //telefono
            'nuevoTelefono.required' => 'El campo celular es requerido',
            'nuevoTelefono.max'  => 'El campo celular no puede sobrepasar 45 caracteres',
            'nuevoTelefono.regex' => 'El campo celular no puede estar vacío o contener caracteres especiales',
            //direccion
            'direccion.required' => 'El campo dirección  es requerido',
            'direccion.max'  => 'El campo dirección  no puede sobrepasar 100 caracteres',
            'direccion.regex' => 'El campo dirección  no puede estar vacío o contener caracteres especiales',
            //fecha de nacimiento
            'nuevaFechaNacimiento.required'  => 'El campo fecha de nacimiento es requerido',
            'nuevaFechaNacimiento.regex' => 'El campo fecha de nacimiento no puede estar vacío o contener caracteres especiales',
            //nacionalidad y ciudad
            'idciudad.required'  => 'El campo ciudad es requerido',
            'idnacionalidad.required'  => 'El campo nacionalidad es requerido',
            //foto usuario
            'nuevaFoto.mimes' => 'La nueva foto debe ser un archivo de tipo: jpeg, png',
        ];
    }

}
