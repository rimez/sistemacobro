<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidacionUsuario extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|min:3|max:45|regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚÜü ]+$/',
            'apellido' => 'required|min:3|max:45|regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚÜü ]+$/',
            'usuario' => 'required|min:4|max:50|unique:usuarios,usuario|regex:/^[a-zA-Z0-9]+$/',
            'nuevoPerfil' => 'required',
            'contrasena' => 'required|max:100',
            'repcontrasena' => 'required|max:100|same:contrasena',
            'cedula' => 'required|min:4|max:45|unique:usuarios,cedulausuario|regex:/^[a-zA-Z0-9]+$/',
            'nuevoTelefono' => 'required|max:45|regex:/^[()\-0-9 ]+$/',
            'direccion' => 'required|max:100|regex:/^[#\.\/\-a-zA-Z0-9 ,]+$/',
            'nuevaFechaNacimiento' => 'required|regex:/^[0-9-]+$/',
            'nuevaFoto' => 'image:jpeg,png',
            'idciudad' => 'required',
            'idnacionalidad' => 'required',

        ];
    }
        public function messages()
    {
        return [
            //nombre
            'nombre.required' => 'El campo nombre es requerido',
            'nombre.max'  => 'El campo nombre no puede sobrepasar 45 caracteres',
            'nombre.min' => 'El campo nombre debe tener como minimo 3 caracteres',
            'nombre.regex' => 'El campo nombre no puede estar vacío o contener caracteres especiales',
            //apellido
            'apellido.required' => 'El campo apellido es requerido',
            'apellido.max'  => 'El campo apellido no puede sobrepasar 45 caracteres',
            'apellido.min' => 'El campo apellido debe tener como minimo 3 caracteres',
            'apellido.regex' => 'El campo apellido no puede estar vacío o contener caracteres especiales',
            //usuario
            'usuario.required' => 'El campo usuario es requerido',
            'usuario.max'  => 'El campo usuario no puede sobrepasar 45 caracteres',
            'usuario.min' => 'El campo usuario debe tener como minimo 4 caracteres',
            'usuario.unique' => 'El nombre de usuario ya esta asignado a otro usuario',
            'usuario.regex' => 'El campo usuario no puede estar vacío o contener caracteres especiales',
            //contrasena
            'contrasena.required' => 'El campo contraseña es requerido',
            'contrasena.max'  => 'El campo contraseña no puede sobrepasar 100 caracteres',
            //repetir contraseña
            'repcontrasena.required' => 'El campo contraseña es requerido',
            'repcontrasena.same'  => 'Los campos contraseña  y repetir contraseña no coinciden',
            //cedula
            'cedula.required' => 'El campo cedula es requerido',
            'cedula.max'  => 'El campo cedula no puede sobrepasar 45 caracteres',
            'cedula.min' => 'El campo cedula debe tener como minimo 4 caracteres',
            'cedula.unique' => 'La cedula de usuario ya esta asignado a otro usuario',
            'cedula.regex' => 'El campo cedula no puede estar vacío o contener caracteres especiales',
            //telefono
            'nuevoTelefono.required' => 'El campo celular es requerido',
            'nuevoTelefono.max'  => 'El campo celular no puede sobrepasar 45 caracteres',
            'nuevoTelefono.regex' => 'El campo celular no puede estar vacío o contener caracteres especiales',
            //direccion
            'direccion.required' => 'El campo dirección  es requerido',
            'direccion.max'  => 'El campo dirección  no puede sobrepasar 100 caracteres',
            'direccion.regex' => 'El campo dirección  no puede estar vacío o contener caracteres especiales',
            //fecha de nacimiento
            'nuevaFechaNacimiento.required'  => 'El campo fecha de nacimiento es requerido',
            'nuevaFechaNacimiento.regex' => 'El campo fecha de nacimiento no puede estar vacío o contener caracteres especiales',
            //nacionalidad y ciudad
            'idciudad.required'  => 'El campo ciudad es requerido',
            'idnacionalidad.required'  => 'El campo nacionalidad es requerido',
        ];
    }

}
