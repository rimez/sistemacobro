<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use http\Env\Request;
class ValidacionClienteActualizar extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|min:3|max:45|regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚÜü ]+$/',
            'apellido' => 'required|min:3|max:45|regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚÜü ]+$/',
            'nuevoSexo' => 'required',
            'cedula' => 'required|min:4|max:45|unique:clientes,cedulacliente,'.$this->input('idCliente').',idcliente|regex:/^[a-zA-Z0-9]+$/',
            'nuevoVenCedula' => 'required|regex:/^[0-9-]+$/',
            'nuevoEstadoCivil' => 'required',
            'nuevaFechaNacimiento' => 'required|regex:/^[0-9-]+$/',
            'nuevoTelefono' => 'required|max:45|regex:/^[()\-0-9 ]+$/',
            'nuevoCelular' => 'required|max:45|regex:/^[()\-0-9 ]+$/',
            'idprofesion' => 'required',
            'idOcupacion' => 'required',
            'ingprom' => 'required|max:9|min:6|regex:/^[0-9]+$/',
            'idnacionalidad' => 'required',
            'idDepartamento' => 'required',
            'idciudad' => 'required',
            'direccion' => 'required|max:100|regex:/^[#\.\/\-a-zA-Z0-9 ,]+$/',
            'coordenadas' => 'max:45',
        ];
    }
    public function messages()
    {
        return [
            //nombre
            'nombre.required' => 'El campo nombre es requerido',
            'nombre.max'  => 'El campo nombre no puede sobrepasar 45 caracteres',
            'nombre.min' => 'El campo nombre debe tener como minimo 3 caracteres',
            'nombre.regex' => 'El campo nombre no puede estar vacío o contener caracteres especiales',
            //apellido
            'apellido.required' => 'El campo apellido es requerido',
            'apellido.max'  => 'El campo apellido no puede sobrepasar 45 caracteres',
            'apellido.min' => 'El campo apellido debe tener como minimo 3 caracteres',
            'apellido.regex' => 'El campo apellido no puede estar vacío o contener caracteres especiales',
            //sexo
            'nuevoSexo.required' => 'El campo sexo es requerido',
            'nuevoSexo.max'  => 'El campo sexo no puede sobrepasar 100 caracteres',
            //cedula
            'cedula.required' => 'El campo cedula es requerido',
            'cedula.max'  => 'El campo cedula no puede sobrepasar 45 caracteres',
            'cedula.min' => 'El campo cedula debe tener como minimo 4 caracteres',
            'cedula.unique' => 'La cedula de cliente ya esta asignado a otro cliente',
            'cedula.regex' => 'El campo cedula no puede estar vacío o contener caracteres especiales',
            //fecha de vencimiento cedula
            'nuevoVenCedula.required'  => 'El campo Vencimiento de Cédula es requerido',
            'nuevoVenCedula.regex' => 'El campo Vencimiento de Cédula no puede estar vacío o contener caracteres especiales',
            //estado civil
            'nuevoEstadoCivil.required' => 'El campo Estado civil es requerido',
            'nuevoEstadoCivil.max'  => 'El campo Estado civil no puede sobrepasar 100 caracteres',
            //fecha de nacimiento
            'nuevaFechaNacimiento.required'  => 'El campo fecha de nacimiento es requerido',
            'nuevaFechaNacimiento.regex' => 'El campo fecha de nacimiento no puede estar vacío o contener caracteres especiales',
            //telefono
            'nuevoTelefono.required' => 'El campo Teléfono es requerido',
            'nuevoTelefono.max'  => 'El campo Teléfono no puede sobrepasar 45 caracteres',
            'nuevoTelefono.regex' => 'El campo Teléfono no puede estar vacío o contener caracteres especiales',
            //celular
            'nuevoCelular.required' => 'El campo celular es requerido',
            'nuevoCelular.max'  => 'El campo celular no puede sobrepasar 45 caracteres',
            'nuevoCelular.regex' => 'El campo celular no puede estar vacío o contener caracteres especiales',
            //profesion
            'idprofesion.required' => 'El campo Profesión es requerido',
            'idprofesion.max'  => 'El campo Profesión no puede sobrepasar 100 caracteres',
            //ocupacion
            'idOcupacion.required' => 'El campo Ocupación es requerido',
            'idOcupacion.max'  => 'El campo Ocupación no puede sobrepasar 100 caracteres',
            //ingreso promedio
            'ingprom.max'  => 'El campo Ingreso promedio no puede sobrepasar 9 caracteres',
            'ingprom.min' => 'El campo Ingreso promedio debe tener como minimo 6 caracteres',
            //nacionalidad
            'idnacionalidad.required'  => 'El campo Nacionalidad es requerido',
            //departamento
            'idDepartamento.required' => 'El campo Departamento es requerido',
            //ciudad
            'idciudad.required' => 'El campo Ciudad es requerido',
            //direccion
            'direccion.required' => 'El campo dirección  es requerido',
            'direccion.max'  => 'El campo dirección  no puede sobrepasar 100 caracteres',
            'direccion.regex' => 'El campo dirección  no puede estar vacío o contener caracteres especiales',
            //coordenadas
            'coordenadas.max' => 'El campo Coordenadas del domicilio debe tener como maximo 45 caracteres',
            'coordenadas.regex' => 'Coordenadas del domicilio invalida',

        ];
    }
}

