<?php
namespace App\Utilidades;

class UtilFechas{
    /**
     * Validar fecha.
     *
     * @param f fecha en formato dd-MM-yyyy
     * @return
     */
    static public function fechaEsValida($f) {

        $dia =  substr($f, 0, 2);
        $mes =  substr($f, 3, 2);
        $anio = substr($f, 6, 10);

        if ($anio < 1 || $mes > 12 || $mes < 1 || $dia > 31 || $dia < 1) {

        } else if (($mes == 4 || $mes == 6 || $mes == 9 || $mes == 11) && ($dia > 30)) {
//                System.out.println("Error, el mes solo tiene 30 dias en este mes");
        } else if ($mes == 2 && UtilFechas::bisiesto($anio) && $dia > 29) {
//                System.out.println("Error, el mes solo tiene 29 dias en este mes");
        } else if ($mes == 2 && ! UtilFechas::bisiesto($anio) && $dia > 28) {
//                System.out.println("Error, el mes solo tiene 28 dias en este mes");
        } else {


            return true;
        }

        return false;
    }


    /**
     * Anio biciesto
     *
     * @param anio
     * @return bool
     */
    public static function bisiesto( $anio) {
        if ($anio % 400 == 0) {

            return true;

        } else {

            return $anio % 4 == 0 && $anio % 100 != 0;
        }
    }

    /**
     * Fecha para base de datos
     *
     * @param f fecha en formato dd-MM-yyyy
     * @return string
     */
    public static function fechaParaBD($f){

        if($f==null || $f==""){

            return "";
        }

        return $fecha = substr($f, 6, 10).'-'.substr($f, 3, 2).'-'.substr($f, 0, 2);

    }

    /**
     * Fecha de base de datos
     *
     * @param f fecha en formato yyyy-MM-dd
     * @return Fecha en formato dd-MM-YYYY
     */
    public static function fechaDeBD($f){

        if($f==null || $f==""){

            return "";
        }

        return $fecha = substr($f, 8, 10).'-'.substr($f, 5, 2).'-'.substr($f, 0, 4);

    }

    /**
     * Calcular la cantidad de años entre dos fechas
     *
     * @param f fecha en formato dd-MM-yyyy
     * @param f2 fecha en formato dd-MM-yyyy
     * @return cantidad de anios
     */
    public static function cantidadAnhosEntreDosFechas($f, $f2){

        $diaDesde =  substr($f, 0, 2);
        $mesDesde =  substr($f, 3, 2);
        $anioDesde = substr($f, 6, 10);

        $diaHasta =  substr($f2, 0, 2);
        $mesHasta =  substr($f2, 3, 2);
        $anioHasta = substr($f2, 6, 10);

        $anio = ($anioHasta - $anioDesde);

        if ($mesHasta < $mesDesde || ($mesHasta == $mesDesde && $diaHasta < $diaDesde)) {

            $anio -= 1;
        }

        return $anio;
    }

    /**
     * Calcular la edad entre una fecha y la fecha actual
     *
     * @param fechaNacimiento fecha en formato dd-MM-yyyy
     * @return edad
     */
    public static function obtenerEdad($fechaNacimiento){

        $fechaHoy = date("d-m-Y");

        return UtilFechas::cantidadAnhosEntreDosFechas($fechaNacimiento, $fechaHoy);
    }

    /**
     * Fecha de base de datos con hora
     *
     * @param f fecha en formato yyyy-MM-dd hh:mm:ss
     * @return Fecha en formato dd-MM-YYYY hh:mm:ss
     */
    public static function fechaDeBDConHora($f){

        if($f==null || $f==""){

            return "";
        }

        return $fecha = substr($f, 8, 2).'-'.substr($f, 5, 2).'-'.substr($f, 0, 4).' '.substr($f, 10, 18);

    }
}
