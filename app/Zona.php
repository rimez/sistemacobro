<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zona extends Model
{
    protected $table = 'zonacobranza';
    public $timestamps = false;
    protected $primaryKey = 'idzonacobranza';
}
