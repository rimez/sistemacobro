<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoPrestamo extends Model
{
    protected $table = 'tipoprestamo';
    public $timestamps = false;
}
