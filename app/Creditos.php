<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Creditos extends Model
{
    protected $table = 'creditos';
    protected $primaryKey = 'idcreditos';
    protected $dates = ['creditofechasolicitud','creditofechadesembolso'];
    public $timestamps = false;
}
