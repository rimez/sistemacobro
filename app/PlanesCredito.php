<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanesCredito extends Model
{
    protected $table = 'planescredito';
    protected $primaryKey = 'idplanescredito';
    public $timestamps = false;
}
