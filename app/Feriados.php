<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feriados extends Model
{
    protected $table = 'feriados';
    protected $primaryKey = 'idferiados';
    protected $dates = ['fechaferiados'];
    public $timestamps = false;
}
