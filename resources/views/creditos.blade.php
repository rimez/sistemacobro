@extends("theme.lte.layout")
@section('titulo')
    Creditos
@endsection
@section('titulo_cabecera')
    Creditos
@endsection
@section('contenido')
    @include('includes.form-error')
    @include('includes.mensaje')
    <div class="box">
        <div class="box-header with-border">
            <button class="btn btn-primary" onclick="location.href='{{route('abm_creditos')}}'">
                Agregar Credito
            </button>
            <div class="box-body">
                @unless(empty($creditos))
                    <table class="table table-bordered table-striped dt-responsive tablas">
                        <thead>
                        <tr>
                            <th style="width:80px">Credito Nro.</th>
                            <th style="width:73px">Nro. Cliente</th>
                            <th>Nombre y Apellido</th>
                            <th>Monto Solicitado</th>
                            <th>Zona</th>
                            <th>Fecha Solicitud</th>
                            <th>Estado</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($creditos as $credito)
                            <tr>
                                <td>{{$credito->idcreditos}}</td>
                                <td>{{$credito->idcliente}}</td>
                                <td>{{$credito->apellidocliente}}, {{$credito->nombrecliente}}</td>
                                <td>{{$credito->montoplan}}</td>
{{--                                @if($credito->creditogarantia ==true)--}}
{{--                                    <td>PERSONAL</td>--}}
{{--                                    @else--}}
{{--                                    <td>SOLIDARIA</td>--}}
{{--                                    @endif--}}
                                <td>{{$credito->nombrezona}}</td>

                                <td>{{$credito->creditofechasolicitud->format('d-m-Y')}}</td>
                                @if($credito->creditoestadosolicitud=="APROBADO")
                                    <td><button class="btn btn-success btn-xs" style="width:95px">APROBADO</button></td>
                                @elseif($credito->creditoestadosolicitud=="RECHAZADO")
                                    <td><button class="btn btn-danger btn-xs" style="width:95px">RECHAZADO</button></td>
                                @elseif($credito->creditoestadosolicitud=="DESEMBOLSADO")
                                    <td><button class="btn btn-primary btn-xs" style="width:95px">DESEMBOLSADO</button></td>
                                @else
                                    <td><button class="btn btn-warning btn-xs" style="width:95px">{{$credito->creditoestadosolicitud}}</button></td>
                                @endif

                                <td>
                                    <div class="btn-group">
                                        <button class="btn btn-success btnAprobarCredito"
                                                idCredito="{{$credito->idcreditos}}"
                                                nombreCliente = "{{$credito->apellidocliente}}, {{$credito->nombrecliente}}"
                                                montoPlan = "{{$credito->montoplan}}"
                                                estadoCredito="{{$credito->creditoestadosolicitud}}">
                                            <i class="fa fa-check">Aprobar</i></button>
                                        <button class="btn btn-danger btnRechazarCredito"
                                                idCredito="{{$credito->idcreditos}}"
                                                nombreCliente = "{{$credito->apellidocliente}}, {{$credito->nombrecliente}}"
                                                montoPlan = "{{$credito->montoplan}}"
                                                estadoCredito="{{$credito->creditoestadosolicitud}}"><i class="fa fa-times">Rechazar</i></button>
                                        @if($credito->creditoestadosolicitud=="DESEMBOLSADO")
                                            <button class="btn btn-primary"style="width: 110px"><i class="fa fa-print" > Imprimir</i></button>
                                            @else
                                            <button class="btn btn-primary "style="width: 110px"><i class="fa fa-calendar-check-o"> Desembolsar</i></button>
                                            @endif
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endunless
            </div>
        </div>
    </div>
@endsection
