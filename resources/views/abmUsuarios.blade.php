@extends("theme.lte.layout")
@section('titulo')
    Usuarios
@endsection
@section('titulo_cabecera')
    Usuarios
@endsection
@section('contenido')
    @include('includes.form-error')
    @include('includes.mensaje')
    <div class="box">
        <div class="box-header with-border">
            <button class="btn btn-primary" onclick="location.href='{{route('usuarios')}}'">
                Agregar usuario
            </button>
            <div class="box-body">
                @unless(empty($usuarios))
                <table class="table table-bordered table-striped dt-responsive tablas">
                    <thead>
                    <tr>
                        <th style="width:10px">#</th>
                        <th>Sucursal</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Usuario</th>
                        <th>Foto</th>
                        <th>Perfil</th>
                        <th>Estado</th>
                        <th>Fecha de ingreso</th>
                        <th>Ultimo Login</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($usuarios as $usuario)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$usuario->nombresucursal}}</td>
                        <td>{{$usuario->nombreusuario}}</td>
                        <td>{{$usuario->apellidousuario}}</td>
                        <td>{{$usuario->usuario}}</td>
                        @if($usuario->fotousuario ==="")
                            <td><img src="{{asset("assets/lte/img/default/default.png")}}" class="img-thumbnail" width="40px"></td>
                            @else
                            <td><img src="{!!$usuario->fotousuario !!}" class="img-thumbnail" width="40px"></td>
                            @endif
                        <td>{{$usuario->perfil}}</td>
                        @if($usuario->estado ==true)
                        <td><button class="btn btn-success btn-xs">Activado</button></td>
                        @else
                            <td><button class="btn btn-danger btn-xs">Desactivado</button></td>
                        @endif
                        <td>{{$usuario->fecha}}</td>
                        <td>{{$usuario->fechaLogin}}</td>
                        <td>
                            <div class="btn-group">
                                <button class="btn btn-warning" onclick="location.href='{{route('editar_usuario',['id' => $usuario->idusuarios])}}'">
                                    <i class="fa fa-pencil"></i></button>
                                <button class="btn btn-danger btnEliminarUsuario" data-href="{{route('eliminar_usuario',
                                        ['id' => $usuario->idusuarios])}}">
                                    <i class="fa fa-times"></i></button>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                @endunless
            </div>
        </div>
    </div>
    @endsection
