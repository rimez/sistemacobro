@extends("theme.lte.layout")
@section('titulo')
    Sucursales
@endsection
@section('titulo_cabecera')
    Administrar Sucursales
@endsection
@section('contenido')
    @include('includes.form-error')
    @include('includes.mensaje')
    <div class="box">

        <div class="box-header with-border">

            <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregar">

                Agregar Sucursal

            </button>

        </div>


        <div class="box-body">

            <table class="table table-bordered table-striped dt-responsive tablas">

                <thead>

                <tr>

                    <th style="width:10px">#</th>
                    <th>Sucursal</th>
                    <th>Teléfono</th>
                    <th>Celular</th>
                    <th>Fecha de Creación</th>
                    <th>Acciones</th>

                </tr>

                </thead>

                <tbody>

                @foreach($sucursales as $sucursal)

                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$sucursal->nombresucursal}}</td>
                        <td>{{$sucursal->telefonosucursal}}</td>
                        <td>{{$sucursal->celularsucursal}}</td>
                        <td>{{$sucursal->fechacreacionsucursal->format('d-m-Y H:i:s')}}</td>
                        <td>

                            <div class="btn-group">

                                <button class="btn btn-warning btnEditarSucursal" idSucursal="{{$sucursal->idsucursales}}"
                                        nombreSucursal="{{$sucursal->nombresucursal}}" telefono="{{$sucursal->telefonosucursal}}"
                                        celular="{{$sucursal->celularsucursal}}"
                                        data-toggle= "modal" data-target="#modalEditar"><i class="fa fa-pencil"></i></button>

                                <button class="btn btn-danger btnEliminarSucursal" data-href="{{route('eliminar_sucursal',
                                        ['id' => $sucursal->idsucursales])}}">
                                    <i class="fa fa-times"></i></button>
                            </div>

                        </td>
                    </tr>
                @endforeach

                </tbody>

            </table>

        </div>

    </div>


    <!--=====================================
    MODAL AGREGAR SUCURSAL
    ======================================-->

    <div id="modalAgregar" class="modal fade" role="dialog">

        <div class="modal-dialog">

            <div class="modal-content">

                <form action="{{route('guardar_sucursal')}}" role="form" method="post" enctype="multipart/form-data">
                @csrf

                <!--=====================================
                CABEZA DEL MODAL
                ======================================-->

                    <div class="modal-header" style="background:#3c8dbc; color:white">

                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                        <h4 class="modal-title">Agregar Sucursal</h4>

                    </div>

                    <!--=====================================
                    CUERPO DEL MODAL
                    ======================================-->

                    <div class="modal-body">

                        <div class="box-body">

                            <!-- ENTRADA PARA EL NOMBRE -->

                            <div class="form-group">

                                <div class="input-group">

                                    <span class="input-group-addon"><i class="fa fa-building"></i></span>

                                    <input type="text" class="form-control input-lg" name="nuevoNombre" placeholder="Ingresar nombre" value="">

                                </div>

                            </div>
                            <!-- ENTRADA PARA EL TELEFONO -->

                            <div class="form-group">

                                <div class="input-group">

                                    <span class="input-group-addon"><i class="fa fa-phone"></i></span>

                                    <input type="text" class="form-control input-lg" name="nuevoTelefono"
                                           placeholder="Ingresar número de teléfono" data-inputmask="'mask':'(9999) 999-999'"
                                           data-mask value="{{old('nuevoTelefono')}}" required>

                                </div>

                            </div>

                            <!-- ENTRADA PARA EL Celular -->

                            <div class="form-group">

                                <div class="input-group">

                                    <span class="input-group-addon"><i class="fa fa-mobile"></i></span>

                                    <input type="text" class="form-control input-lg" name="nuevoCelular"
                                           placeholder="Ingresar número de celular" data-inputmask="'mask':'(9999) 999-999'"
                                           data-mask value="{{old('nuevoCelular')}}" required>

                                </div>

                            </div>

                        </div>

                    </div>

                    <!--=====================================
                    PIE DEL MODAL
                    ======================================-->

                    <div class="modal-footer">

                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

                        <button type="submit" class="btn btn-primary">Guardar</button>

                    </div>

                </form>

            </div>

        </div>

    </div>

    <!--=====================================
               ACTUALIZAR DATOS DE SUCURSAL
     ======================================-->

    <div id="modalEditar" class="modal fade" role="dialog">

        <div class="modal-dialog">

            <div class="modal-content">

                <form action="{{route('actualizar_sucursal')}}" role="form" method="post" enctype="multipart/form-data">
                @csrf

                <!--=====================================
                CABEZA DEL MODAL
                ======================================-->

                    <div class="modal-header" style="background:#3c8dbc; color:white">

                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                        <h4 class="modal-title">Actualizar Sucursal</h4>

                    </div>

                    <!--=====================================
                    CUERPO DEL MODAL
                    ======================================-->

                    <div class="modal-body">

                        <div class="box-body">

                            <!-- ENTRADA PARA EL NOMBRE -->

                            <div class="form-group">

                                <div class="input-group">

                                    <span class="input-group-addon"><i class="fa fa-building"></i></span>

                                    <input type="text" class="form-control input-lg" id="editarNombre" name="editarNombre" value="" required>
                                    <input type="hidden" id="idActual" name="idActual" value="">

                                </div>

                            </div>
                            <!-- ENTRADA PARA EL TELEFONO -->

                            <div class="form-group">

                                <div class="input-group">

                                    <span class="input-group-addon"><i class="fa fa-phone"></i></span>

                                    <input type="text" class="form-control input-lg" name="editarTelefono" id="editarTelefono"
                                           placeholder="Ingresar número de teléfono" data-inputmask="'mask':'(9999) 999-999'"
                                           data-mask value="{{old('nuevoTelefono')}}" required>

                                </div>

                            </div>

                            <!-- ENTRADA PARA EL Celular -->

                            <div class="form-group">

                                <div class="input-group">

                                    <span class="input-group-addon"><i class="fa fa-mobile"></i></span>

                                    <input type="text" class="form-control input-lg" name="editarCelular" id="editarCelular"
                                           placeholder="Ingresar número de celular" data-inputmask="'mask':'(9999) 999-999'"
                                           data-mask value="{{old('nuevoCelular')}}" required>

                                </div>

                            </div>

                        </div>

                    </div>

                    <!--=====================================
                    PIE DEL MODAL
                    ======================================-->

                    <div class="modal-footer">

                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

                        <button type="submit" class="btn btn-primary">Guardar Cambios</button>

                    </div>
                </form>

            </div>

        </div>

    </div>
@endsection
