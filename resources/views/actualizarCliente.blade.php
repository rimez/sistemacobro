@extends("theme.lte.layout")
@section('titulo')
    Clientes
@endsection
@section('titulo_cabecera')
    Clientes
@endsection
@section('contenido')
    @include('includes.form-error')
    @include('includes.mensaje')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Actualizar datos de Cliente</h3>
                </div>
                <form action="{{route('actualizar_cliente')}}" role="form" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="box-body">
                        <div class="row">
                            <!-- ENTRADA PARA EL NOMBRE -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Nombre:</label>
                                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Ingrese Nombre" value="{{$cliente->nombrecliente}}" required>
                                    <input type="hidden" id="idCliente" name="idCliente" value="{{$cliente->idcliente}}">
                                </div>
                            </div>
                            <!-- ENTRADA PARA EL APELLIDO -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Apellido:</label>
                                    <input type="text" class="form-control" id="apellido" name="apellido"
                                           placeholder="Ingrese apellido" required value="{{$cliente->apellidocliente}}">
                                </div>
                            </div>
                            <!-- ENTRADA PARA EL SEXO -->
                            <div class="col-md-4">
                                <label for="exampleInputSexo">Sexo</label>
                                <select class="form-control" name="nuevoSexo"  id="sexo" required>
                                    <option value="">Selecionar sexo</option>
                                    <option value="1">Masculino</option>
                                    <option value="0">Femenino</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <!-- ENTRADA PARA LA CEDULA -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Cédula:</label>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-id-card-o"></i></span>
                                            <input type="text" class="form-control" id="cedula" name="cedula"
                                                   placeholder="Ingrese Numero de cédula" value="{{$cliente->cedulacliente}}" required>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- ENTRADA PARA VENCIMIENTO CEDULA -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Vencimiento de Cédula:</label>

                                    <div class="form-group">

                                        <div class="input-group">

                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>

                                            <input type="text" class="form-control" name="nuevoVenCedula"
                                                   placeholder="Ingresar fecha de vencimiento de cédula"
                                                   data-inputmask="'alias': 'dd-mm-yyyy'" data-mask
                                                   value={{\Carbon\Carbon::parse($cliente->cedulavenccliente)->format('d/m/Y')}}"" required>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- ENTRADA PARA EL ESTADO CIVIL -->
                            <div class="col-md-4">
                                <label for="exampleInputSexo">Estado civil:</label>
                                <select class="form-control" name="nuevoEstadoCivil" id="estadoCivil" required>
                                    <option value="">Selecionar Estado civil</option>
                                    <option value="1">Soltero</option>
                                    <option value="0">Casado</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <!-- ENTRADA PARA LA FECHA DE NACIMIENTO -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Fecha de nacimiento:</label>

                                    <div class="form-group">

                                        <div class="input-group">

                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>

                                            <input type="text" class="form-control" name="nuevaFechaNacimiento"
                                                   placeholder="Ingresar fecha de nacimiento"
                                                   data-inputmask="'alias': 'dd-mm-yyyy'" data-mask
                                                   value="{{\Carbon\Carbon::parse($cliente->fechanaccliente)->format('d/m/Y')}}" required>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- ENTRADA PARA EL NUM DE Telefono-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Teléfono:</label>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-phone"></i></span>

                                            <input type="text" class="form-control" name="nuevoTelefono"
                                                   placeholder="Ingresar número de teléfono" data-inputmask="'mask':'(9999) 999-999'"
                                                   data-mask value="{{$cliente->telefonocliente}}" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- ENTRADA PARA EL NUM DE CELULAR-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Celular:</label>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-mobile"></i></span>

                                            <input type="text" class="form-control" name="nuevoCelular"
                                                   placeholder="Ingresar número de celular" data-inputmask="'mask':'(9999) 999-999'"
                                                   data-mask value="{{$cliente->celularcliente}}" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <!-- ENTRADA PARA LA PROFESION -->
                            <div class="col-md-4">
                                <label for="exampleInputEmail1">Profesión:</label>
                                <div class="input-group input-group-sm">
                                    <select class="form-control select2" name="idprofesion" id="idprofesion"
                                            sexo = "{{$cliente->sexocliente}}"
                                            estadoCivil = "{{$cliente->estadocivil}}"
                                            idProfesion="{{$cliente->profesiones_idprofesion}}"
                                            idOcupacion="{{$cliente->ocupaciones_idocupaciones}}"
                                            idNaci="{{$cliente->nacionalidades_idnacionalidad}}"
                                            idDepar="{{$cliente->ciudades_iddepartamentos}}"
                                            idCiu="{{$cliente->ciudades_idciudades}}" required>
                                        <option value="">Seleccionar Profesión</option>
                                        @foreach($profesiones as $profesion)
                                            <option value="{{$profesion->idprofesion}}">{{$profesion->profesion}}</option>
                                        @endforeach
                                    </select>
                                    <span class="input-group-btn">
                                     <button type="button" class="btn btn-success btn-flat" data-toggle= "modal"
                                             data-target="#modalAgregarProfesion">+</button>
                                </span>
                                </div>
                            </div>
                            <!-- ENTRADA PARA EL OCUPACION -->
                            <div class="col-md-4">
                                <label for="exampleInputEmail1">Ocupacion</label>
                                <div class="input-group input-group-sm">
                                    <select class="form-control select2" required id="idOcupacion" name="idOcupacion">
                                        <option value="">Seleccionar Ocupacion</option>
                                        @foreach($ocupaciones as $ocupacion)
                                            <option value="{{$ocupacion->idocupaciones}}">{{$ocupacion->ocupacion}}</option>
                                        @endforeach
                                    </select>
                                    <span class="input-group-btn">
                                     <button type="button" class="btn btn-success btn-flat" data-toggle= "modal"
                                             data-target="#modalAgregarOcupacion">+</button>
                                </span>
                                </div>
                            </div>
                            <!-- ENTRADA PARA EL INGRESO PROMEDIO -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Ingreso promedio:</label>
                                    <input type="number" min="100000" class="form-control" id="ingprom" name="ingprom"
                                           placeholder="Ingrese ingreso promedio del cliente" required value="{{$cliente->ingresopromcliente}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <!-- ENTRADA PARA LA NACIONALIDAD -->
                            <div class="col-md-4">
                                <label for="exampleInputEmail1">Nacionalidad</label>
                                <div class="input-group input-group-sm">
                                    <select class="form-control select2" id="idnacionalidad" name="idnacionalidad" required>
                                        <option value="">Seleccionar Nacionalidad</option>
                                        @foreach($nacionalidades as $nacionalidad)
                                            <option value="{{$nacionalidad->idnacionalidad}}">{{$nacionalidad->nacionalidad}}</option>
                                        @endforeach
                                    </select>
                                    <span class="input-group-btn">
                                     <button type="button" class="btn btn-success btn-flat" data-toggle= "modal"
                                             data-target="#modalAgregarNacionalidad">+</button>
                                </span>
                                </div>
                            </div>
                            <!-- ENTRADA PARA EL DEPARTAMENTO -->
                            <div class="col-lg-4">
                                <label for="exampleInputEmail1">Departamento</label>
                                <div class="input-group input-group-sm">
                                    <select class="form-control select2 departamento2" required id="idDepartamento" name="idDepartamento">
                                        <option value="">Seleccionar Departamento</option>
                                        @foreach($departamentos as $departamento)
                                            <option value="{{$departamento->iddepartamentos}}">{{$departamento->departamento}}</option>
                                        @endforeach
                                    </select>
                                    <span class="input-group-btn">
                                     <button type="button" class="btn btn-success btn-flat" data-toggle= "modal"
                                             data-target="#modalAgregarDepartamento">+</button>
                                </span>
                                </div>
                            </div>
                            <!-- ENTRADA PARA LA CIUDAD -->
                            <div class="col-md-4">
                                <label for="exampleInputEmail1">ciudad</label>
                                <div class="input-group input-group-sm">
                                    <select class="form-control select2 ciudad2" id="idCiudad" name="idciudad" required>
                                        <option value="">Seleccionar Ciudad</option>
                                        @foreach($ciudades as $ciudad)
                                            <option value="{{$ciudad->idciudades}}">{{$ciudad->ciudad}}</option>
                                        @endforeach

                                    </select>
                                    <span class="input-group-btn">
                                     <button type="button" class="btn btn-success btn-flat btnCiudad" disabled>+</button>
                                </span>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px">
                            <!-- ENTRADA PARA LA DIRECCION -->
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Direccion</label>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-home"></i></span>
                                            <input type="text" class="form-control" id="direccion" name="direccion"
                                                   placeholder="Ingrese Nombre" value="{{$cliente->domiciliocliente}}" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- ENTRADA PARA LAS COORDENADAS -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Coordenadas del domicilio</label>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-location-arrow"></i></span>
                                            <input type="text" class="form-control" id="coordenadas" name="coordenadas"
                                                   placeholder="Ingrese Coordenadas" value="{{$cliente->coordenadasdomicilio}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Guardar cambios</button>
                    </div>
                </form>
            </div>

        </div>
    </div>


    <!--=====================================
    MODAL AGREGAR NACIONALIDAD
    ======================================-->

    <div id="modalAgregarNacionalidad" class="modal fade" role="dialog">

        <div class="modal-dialog">

            <div class="modal-content">

                <form action="{{route('guardar_nacionalidad')}}" role="form" method="post" enctype="multipart/form-data">
                @csrf

                <!--=====================================
                CABEZA DEL MODAL
                ======================================-->

                    <div class="modal-header" style="background:#3c8dbc; color:white">

                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                        <h4 class="modal-title">Agregar Nacionalidad</h4>

                    </div>

                    <!--=====================================
                    CUERPO DEL MODAL
                    ======================================-->

                    <div class="modal-body">

                        <div class="box-body">

                            <!-- ENTRADA PARA EL NOMBRE -->

                            <div class="form-group">

                                <div class="input-group">

                                    <span class="input-group-addon"><i class="fa fa-th"></i></span>

                                    <input type="text" class="form-control input-lg" name="nuevoNombre" placeholder="Ingresar nacionalidad" value="">

                                </div>

                            </div>

                        </div>

                    </div>

                    <!--=====================================
                    PIE DEL MODAL
                    ======================================-->

                    <div class="modal-footer">

                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

                        <button type="submit" class="btn btn-primary">Guardar Nacionalidad</button>

                    </div>

                </form>

            </div>

        </div>

    </div>
    <!--=====================================
    MODAL AGREGAR DEPARTAMENTO
    ======================================-->

    <div id="modalAgregarDepartamento" class="modal fade" role="dialog">

        <div class="modal-dialog">

            <div class="modal-content">

                <form action="{{route('guardar_departamento')}}" role="form" method="post" enctype="multipart/form-data">
                @csrf

                <!--=====================================
                CABEZA DEL MODAL
                ======================================-->

                    <div class="modal-header" style="background:#3c8dbc; color:white">

                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                        <h4 class="modal-title">Agregar Departamento</h4>

                    </div>

                    <!--=====================================
                    CUERPO DEL MODAL
                    ======================================-->

                    <div class="modal-body">

                        <div class="box-body">

                            <!-- ENTRADA PARA EL NOMBRE -->

                            <div class="form-group">

                                <div class="input-group">

                                    <span class="input-group-addon"><i class="fa fa-th"></i></span>

                                    <input type="text" class="form-control input-lg" name="nuevoNombre" placeholder="Ingresar departamento" value="">

                                </div>

                            </div>

                        </div>

                    </div>

                    <!--=====================================
                    PIE DEL MODAL
                    ======================================-->

                    <div class="modal-footer">

                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

                        <button type="submit" class="btn btn-primary">Guardar Departamento</button>

                    </div>

                </form>

            </div>

        </div>

    </div>
    <!--=====================================
    MODAL AGREGAR OCUPACION
    ======================================-->

    <div id="modalAgregarOcupacion" class="modal fade" role="dialog">

        <div class="modal-dialog">

            <div class="modal-content">

                <form action="{{route('guardar_independiente')}}" role="form" method="post" enctype="multipart/form-data">
                @csrf

                <!--=====================================
                CABEZA DEL MODAL
                ======================================-->

                    <div class="modal-header" style="background:#3c8dbc; color:white">

                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                        <h4 class="modal-title">Agregar Ocupacion</h4>

                    </div>

                    <!--=====================================
                    CUERPO DEL MODAL
                    ======================================-->

                    <div class="modal-body">

                        <div class="box-body">

                            <!-- ENTRADA PARA EL NOMBRE -->

                            <div class="form-group">

                                <div class="input-group">

                                    <span class="input-group-addon"><i class="fa fa-th"></i></span>

                                    <input type="text" class="form-control input-lg" name="nuevoNombre" placeholder="Ingresar Ocupacion" value="">
                                    <input type="hidden" id="tablaind" name="tablaind" value="ocupaciones">
                                    <input type="hidden" id="nomNatural" name="nomNatural" value="ocupacion">
                                    <input type="hidden" id="columnDescrip" name="columnDescrip" value="ocupacion">

                                </div>

                            </div>

                        </div>

                    </div>

                    <!--=====================================
                    PIE DEL MODAL
                    ======================================-->

                    <div class="modal-footer">

                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

                        <button type="submit" class="btn btn-primary">Guardar Ocupacion</button>

                    </div>

                </form>

            </div>

        </div>

    </div>
    <!--=====================================
    MODAL AGREGAR PROFESION
    ======================================-->

    <div id="modalAgregarProfesion" class="modal fade" role="dialog">

        <div class="modal-dialog">

            <div class="modal-content">

                <form action="{{route('guardar_independiente')}}" role="form" method="post" enctype="multipart/form-data">
                @csrf

                <!--=====================================
                CABEZA DEL MODAL
                ======================================-->

                    <div class="modal-header" style="background:#3c8dbc; color:white">

                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                        <h4 class="modal-title">Agregar Profesion</h4>

                    </div>

                    <!--=====================================
                    CUERPO DEL MODAL
                    ======================================-->

                    <div class="modal-body">

                        <div class="box-body">

                            <!-- ENTRADA PARA EL NOMBRE -->

                            <div class="form-group">

                                <div class="input-group">

                                    <span class="input-group-addon"><i class="fa fa-th"></i></span>

                                    <input type="text" class="form-control input-lg" name="nuevoNombre" placeholder="Ingresar Profesion" value="">
                                    <input type="hidden" id="tablaind" name="tablaind" value="profesiones">
                                    <input type="hidden" id="nomNatural" name="nomNatural" value="profesion">
                                    <input type="hidden" id="columnDescrip" name="columnDescrip" value="profesion">

                                </div>

                            </div>

                        </div>

                    </div>

                    <!--=====================================
                    PIE DEL MODAL
                    ======================================-->

                    <div class="modal-footer">

                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

                        <button type="submit" class="btn btn-primary">Guardar Ocupacion</button>

                    </div>

                </form>

            </div>

        </div>

    </div>
@endsection


