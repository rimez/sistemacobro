@extends("theme.lte.layout")
@section('titulo')
    Feriados
@endsection
@section('titulo_cabecera')
    Administrar Feriados de Sucursales
@endsection
@section('contenido')
    @include('includes.form-error')
    @include('includes.mensaje')
    <div class="box">

        <div class="box-header with-border">

            <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregar">

                Agregar Feriado

            </button>

        </div>


        <div class="box-body">

            <table class="table table-bordered table-striped dt-responsive tablas">

                <thead>

                <tr>

                    <th style="width:57px">#</th>
                    <th>Sucursal</th>
                    <th>Descripción</th>
                    <th>Fecha</th>
                    <th>Acciones</th>
                </tr>

                </thead>

                <tbody>

                @foreach($feriados as $feriado)

                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$feriado->nombresucursal}}</td>
                        <td>{{$feriado->descricionferiados}}</td>
                        <td>{{$feriado->fechaferiados->format('d-m-Y')}}</td>

                        <td>

                            <div class="btn-group">

                                <button class="btn btn-warning btnEditarFeriado" idFeriado="{{$feriado->idferidados}}"
                                        idSucursal="{{$feriado->sucursales_idsucursales}}" descripcionFeriado="{{$feriado->descricionferidados}}"
                                        fechaFeriado="{{$feriado->fechaferiados}}"
                                        data-toggle= "modal" data-target="#modalEditar"><i class="fa fa-pencil"></i></button>

                                <button class="btn btn-danger btnEliminarFeriado" data-href="{{route('eliminar_feriado',
                                        ['id' => $feriado->idferiados])}}">
                                    <i class="fa fa-times"></i></button>
                            </div>

                        </td>
                    </tr>
                @endforeach

                </tbody>

            </table>

        </div>

    </div>


    <!--=====================================
    MODAL AGREGAR FERIADO
    ======================================-->

    <div id="modalAgregar" class="modal fade" role="dialog">

        <div class="modal-dialog">

            <div class="modal-content">

                <form action="{{route('guardar_feriado')}}" role="form" method="post" enctype="multipart/form-data">
                @csrf

                <!--=====================================
                CABEZA DEL MODAL
                ======================================-->

                    <div class="modal-header" style="background:#3c8dbc; color:white">

                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                        <h4 class="modal-title">Agregar Feriado de Sucursal</h4>

                    </div>

                    <!--=====================================
                    CUERPO DEL MODAL
                    ======================================-->

                    <div class="modal-body">

                        <div class="box-body">
                            <!-- ENTRADA PARA EL SELECCIONAR SUCURSAL -->

                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-building"></i></span>


                                    <select class="form-control input-lg " name="idSucursal" required>
                                        <option value="">Selecionar Sucursal</option>
                                        <option value="0">Todas las sucursales(Feriado Nacional)</option>
                                        @foreach($sucursales as $sucursal)
                                            <option value="{{$sucursal->idsucursales}}">{{$sucursal->nombresucursal}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <!-- ENTRADA PARA LA DESCRIPCION -->

                            <div class="form-group">

                                <div class="input-group">

                                    <span class="input-group-addon"><i class="fa fa-th"></i></span>

                                    <input type="text" class="form-control input-lg" name="nuevoNombre" placeholder="Ingresar descripcion del feriado" value="" required>

                                </div>

                            </div>
                            <!-- ENTRADA PARA LA FECHA -->

                            <div class="form-group">

                                <div class="input-group">

                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>

                                    <input type="text" class="form-control input-lg" name="nuevaFechaFeriado"
                                           placeholder="Ingresar fecha de feriado"
                                           data-inputmask="'alias': 'dd-mm-yyyy'" data-mask
                                           required>
                                </div>

                            </div>

                        </div>

                    </div>

                    <!--=====================================
                    PIE DEL MODAL
                    ======================================-->

                    <div class="modal-footer">

                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

                        <button type="submit" class="btn btn-primary">Guardar</button>

                    </div>

                </form>

            </div>

        </div>

    </div>

    <!--=====================================
               ACTUALIZAR DATOS DE FERIADO
     ======================================-->

    <div id="modalEditar" class="modal fade" role="dialog">

        <div class="modal-dialog">

            <div class="modal-content">

                <form action="{{route('actualizar_feriado')}}" role="form" method="post" enctype="multipart/form-data">
                @csrf

                <!--=====================================
                CABEZA DEL MODAL
                ======================================-->

                    <div class="modal-header" style="background:#3c8dbc; color:white">

                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                        <h4 class="modal-title">Actualizar Feriado</h4>

                    </div>

                    <!--=====================================
                    CUERPO DEL MODAL
                    ======================================-->

                    <div class="modal-body">

                        <div class="box-body">
                            <!-- ENTRADA PARA LA DESCRIPCION -->

                            <div class="form-group">

                                <div class="input-group">

                                    <span class="input-group-addon"><i class="fa fa-th"></i></span>

                                    <input type="text" class="form-control input-lg" name="actualizarNombre" placeholder="Ingresar descripcion del feriado" value="" required>

                                </div>

                            </div>

                            <!-- ENTRADA PARA LA FECHA -->

                            <div class="form-group">

                                <div class="input-group">

                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>

                                    <input type="text" class="form-control input-lg" name="actualizarFechaFeriado"
                                           placeholder="Ingresar fecha de feriado"
                                           data-inputmask="'alias': 'dd-mm-yyyy'" data-mask
                                           required>
                                </div>

                            </div>

                        </div>

                    </div>

                    <!--=====================================
                    PIE DEL MODAL
                    ======================================-->

                    <div class="modal-footer">

                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

                        <button type="submit" class="btn btn-primary">Guardar Cambios</button>

                    </div>
                </form>

            </div>

        </div>

    </div>
@endsection
