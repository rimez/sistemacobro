@extends("theme.lte.layout")
@section('titulo')
    Clientes
@endsection
@section('titulo_cabecera')
    Clientes
@endsection
@section('contenido')
    @include('includes.form-error')
    @include('includes.mensaje')
    <div class="box">
        <div class="box-header with-border">
            <button class="btn btn-primary" onclick="location.href='{{route('abm_clientes')}}'">
                Agregar Cliente
            </button>
            <div class="box-body">
                @unless(empty($clientes))
                    <table class="table table-bordered table-striped dt-responsive tablas">
                        <thead>
                        <tr>
                            <th style="width:10px">#</th>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Cedula</th>
                            <th>Telefono</th>
                            <th>Celular</th>
                            <th>Fecha de Nacimiento</th>
                            <th>Fecha de ingreso</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($clientes as $cliente)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$cliente->nombrecliente}}</td>
                                <td>{{$cliente->apellidocliente}}</td>
                                <td>{{$cliente->cedulacliente}}</td>
                                <td>{{$cliente->telefonocliente}}</td>
                                <td>{{$cliente->celularcliente}}</td>
                                <td>{{$cliente->fechanaccliente->format('d-m-Y')}}</td>
                                <td>{{$cliente->fechaingresocliente->format('d-m-Y H:i:s')}}</td>
                                <td>
                                    <div class="btn-group">
                                        <button class="btn btn-warning" onclick="location.href='{{route('editar_cliente',['id' => $cliente->idcliente])}}'">
                                            <i class="fa fa-pencil"></i></button>
                                        <button class="btn btn-danger btnEliminarCliente" data-href="{{route('eliminar_cliente',
                                        ['id' => $cliente->idcliente])}}">
                                            <i class="fa fa-times"></i></button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endunless
            </div>
        </div>
    </div>
@endsection
