<!--=====================================
MODAL EDITAR
======================================-->

<div id="@yield('nombreModalEditar')" class="modal fade" role="dialog">

    <div class="modal-dialog">

        <div class="modal-content">

            <form action="@yield('rutaEditarModal')" role="form" method="post" enctype="multipart/form-data">
            @csrf

            <!--=====================================
                CABEZA DEL MODAL
                ======================================-->

                <div class="modal-header" style="background:#3c8dbc; color:white">

                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                    <h4 class="modal-title">@yield('cabeceraModalEditar')</h4>

                </div>

                <!--=====================================
                CUERPO DEL MODAL
                ======================================-->

                <div class="modal-body">

                    <div class="box-body">

                        <!-- ENTRADA PARA EL NOMBRE -->

                        <div class="form-group">

                            <div class="input-group">

                                <span class="input-group-addon"><i class="@yield('iconoImputModalEditar')"></i></span>

                                <input type="text" class="form-control input-lg" id="nuevoNombre" name="nuevoNombre" value="" required>
                                <input type="hidden" id="idActual" name="idActual">


                            </div>

                        </div>

                    </div>

                </div>

                <!--=====================================
                PIE DEL MODAL
                ======================================-->

                <div class="modal-footer">

                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

                    <button type="submit" class="btn btn-primary">@yield('botonEditarModal')</button>


                </div>
            </form>

        </div>

    </div>

</div>
