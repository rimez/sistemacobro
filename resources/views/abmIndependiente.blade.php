@extends("theme.lte.layout")
@section('titulo')
   {{$datos['titulo']}}
@endsection
@section('titulo_cabecera')
    {{$datos['tituloCabecera']}}
@endsection
@section('contenido')
    @include('includes.form-error')
    @include('includes.mensaje')
    <div class="box">

        <div class="box-header with-border">

            <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregar">

                {{$datos['BotonPrincipal']}}

            </button>

        </div>


        <div class="box-body">

            <table class="table table-bordered table-striped dt-responsive tablas">

                <thead>

                <tr>

                    <th style="width:10px">#</th>
                    <th>{{$datos['titulo']}}</th>
                    <th>Acciones</th>

                </tr>

                </thead>

                <tbody>

                @foreach($independientes as $independiente)

                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$independiente->$columnaDescripcion}}</td>

                        <td>

                            <div class="btn-group">

                                <button class="btn btn-warning btnEditarIndependiente" idIndependiente="{{$independiente->$columnaid}}" nombre="{{$independiente->$columnaDescripcion}}"
                                        data-toggle= "modal" data-target="#modalEditar"><i class="fa fa-pencil"></i></button>

                                <button class="btn btn-danger btnEliminarIndependiente" data-href="/independientenacionalidades" tablaeli="{{$tabla}}"
                                    idEli="{{$independiente->$columnaid}} " token = "{{csrf_token()}}">
                                    <i class="fa fa-times"></i></button>
                            </div>

                        </td>
                    </tr>
                @endforeach

                </tbody>

            </table>

        </div>

    </div>


<!--=====================================
MODAL AGREGAR INDEPENDIENTE
======================================-->

    <div id="modalAgregar" class="modal fade" role="dialog">

        <div class="modal-dialog">

            <div class="modal-content">

                <form action="{{route('guardar_independiente')}}" role="form" method="post" enctype="multipart/form-data">
                @csrf

                <!--=====================================
                CABEZA DEL MODAL
                ======================================-->

                    <div class="modal-header" style="background:#3c8dbc; color:white">

                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                        <h4 class="modal-title">{{$datos['cabeceraModal']}}</h4>

                    </div>

                    <!--=====================================
                    CUERPO DEL MODAL
                    ======================================-->

                    <div class="modal-body">

                        <div class="box-body">

                            <!-- ENTRADA PARA EL NOMBRE -->

                            <div class="form-group">

                                <div class="input-group">

                                    <span class="input-group-addon"><i class="{{$datos['iconoImputModal']}}"></i></span>

                                    <input type="text" class="form-control input-lg" name="nuevoNombre" placeholder="Ingresar {{$datos['nomNatural']}}" value="">
                                    <input type="hidden" id="tablaind" name="tablaind" value="{{$tabla}}">
                                    <input type="hidden" id="nomNatural" name="nomNatural" value="{{$datos['nomNatural']}}">
                                    <input type="hidden" id="columnDescrip" name="columnDescrip" value="{{$datos['columnaDescripcion']}}">

                                </div>

                            </div>

                        </div>

                    </div>

                    <!--=====================================
                    PIE DEL MODAL
                    ======================================-->

                    <div class="modal-footer">

                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

                        <button type="submit" class="btn btn-primary">{{$datos['botonGuardarModal']}}</button>

                    </div>

                </form>

            </div>

        </div>

    </div>

    <div id="modalEditar" class="modal fade" role="dialog">

        <div class="modal-dialog">

            <div class="modal-content">

                <form action="{{route('actualizar_independiente')}}" role="form" method="post" enctype="multipart/form-data">
                @csrf

                <!--=====================================
                CABEZA DEL MODAL
                ======================================-->

                    <div class="modal-header" style="background:#3c8dbc; color:white">

                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                        <h4 class="modal-title">{{$datos['cabeceraModalEditar']}}</h4>

                    </div>

                    <!--=====================================
                    CUERPO DEL MODAL
                    ======================================-->

                    <div class="modal-body">

                        <div class="box-body">

                            <!-- ENTRADA PARA EL NOMBRE -->

                            <div class="form-group">

                                <div class="input-group">

                                    <span class="input-group-addon"><i class="{{$datos['iconoImputModalEditar']}}"></i></span>

                                    <input type="text" class="form-control input-lg" id="nuevoNombre" name="nuevoNombre" value="" required>
                                    <input type="hidden" id="idActual" name="idActual">
                                    <input type="hidden" id="tablainde" name="tablainde" value="{{$tabla}}">
                                    <input type="hidden" id="nomNaturalAc" name="nomNaturalAc" value="{{$datos['nomNatural']}}">
                                    <input type="hidden" id="columnDescripAc" name="columnDescripAc" value="{{$datos['columnaDescripcion']}}">
                                    <input type="hidden" id="columnId" name="columnId" value="{{$datos['columnaid']}}">

                                </div>

                            </div>

                        </div>

                    </div>

                    <!--=====================================
                    PIE DEL MODAL
                    ======================================-->

                    <div class="modal-footer">

                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

                        <button type="submit" class="btn btn-primary">Guardar Cambios</button>

                    </div>
                </form>

            </div>

        </div>

    </div>
@endsection







