@extends("theme.lte.layout")
@section('titulo')
    Zonas
@endsection
@section('titulo_cabecera')
    Administrar Zonas de Cobranza
@endsection
@section('contenido')
    @include('includes.form-error')
    @include('includes.mensaje')
    <div class="box">

        <div class="box-header with-border">

            <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregar">

                Agregar Zona

            </button>

        </div>


        <div class="box-body">

            <table class="table table-bordered table-striped dt-responsive tablas">

                <thead>

                <tr>

                    <th style="width:57px">Zona Nro</th>
                    <th>Sucursal</th>
                    <th>Descripcion</th>
                    <th>Usuario</th>
                    <th>Foto</th>
                    <th>Acciones</th>
                </tr>

                </thead>

                <tbody>

                @foreach($zonas as $zona)

                    <tr>
                        <td>{{$zona->idzonacobranza}}</td>
                        <td>{{$zona->nombresucursal}}</td>
                        <td>{{$zona->nombrezona}}</td>
                        <td>{{$zona->usuario}}</td>
                        <td><img src="{!!$zona->fotousuario !!}" class="img-thumbnail" width="40px"></td>

                        <td>

                            <div class="btn-group">

                                <button class="btn btn-warning btnEditarZona" idZona="{{$zona->idzonacobranza}}"
                                        nombreUsuario="{{$zona->usuario}}" idUsuario="{{$zona->idusuarios}}"
                                        nombreZona="{{$zona->nombrezona}}"
                                        data-toggle= "modal" data-target="#modalEditar"><i class="fa fa-pencil"></i></button>

                                <button class="btn btn-danger btnEliminarZona" data-href="{{route('eliminar_zona',
                                        ['id' => $zona->idzonacobranza])}}">
                                    <i class="fa fa-times"></i></button>
                            </div>

                        </td>
                    </tr>
                @endforeach

                </tbody>

            </table>

        </div>

    </div>


    <!--=====================================
    MODAL AGREGAR ZONA
    ======================================-->

    <div id="modalAgregar" class="modal fade" role="dialog">

        <div class="modal-dialog">

            <div class="modal-content">

                <form action="{{route('guardar_zona')}}" role="form" method="post" enctype="multipart/form-data">
                @csrf

                <!--=====================================
                CABEZA DEL MODAL
                ======================================-->

                    <div class="modal-header" style="background:#3c8dbc; color:white">

                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                        <h4 class="modal-title">Agregar Zona de Cobranza</h4>

                    </div>

                    <!--=====================================
                    CUERPO DEL MODAL
                    ======================================-->

                    <div class="modal-body">

                        <div class="box-body">
                            <!-- ENTRADA PARA EL usuario -->

                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>


                                    <select class="form-control input-lg " name="idUsuario" required>
                                        <option value="">Selecionar Usuario</option>
                                        @foreach($usuarios as $usuario)
                                            <option value="{{$usuario->idusuarios}}">{{$usuario->idusuarios}} - {{$usuario->usuario}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <!-- ENTRADA PARA EL NOMBRE -->

                            <div class="form-group">

                                <div class="input-group">

                                    <span class="input-group-addon"><i class="fa fa-street-view"></i></span>

                                    <input type="text" class="form-control input-lg" name="nuevoNombre" placeholder="Ingresar nombre de la zona" value="" required>

                                </div>

                            </div>

                        </div>

                    </div>

                    <!--=====================================
                    PIE DEL MODAL
                    ======================================-->

                    <div class="modal-footer">

                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

                        <button type="submit" class="btn btn-primary">Guardar</button>

                    </div>

                </form>

            </div>

        </div>

    </div>

    <!--=====================================
               ACTUALIZAR DATOS DE ZONA
     ======================================-->

    <div id="modalEditar" class="modal fade" role="dialog">

        <div class="modal-dialog">

            <div class="modal-content">

                <form action="{{route('actualizar_zona')}}" role="form" method="post" enctype="multipart/form-data">
                @csrf

                <!--=====================================
                CABEZA DEL MODAL
                ======================================-->

                    <div class="modal-header" style="background:#3c8dbc; color:white">

                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                        <h4 class="modal-title">Actualizar Zona</h4>

                    </div>

                    <!--=====================================
                    CUERPO DEL MODAL
                    ======================================-->

                    <div class="modal-body">

                        <div class="box-body">
                            <!-- ENTRADA PARA EL usuario -->

                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>


                                    <select class="form-control input-lg " name="idUsuario" id="comboUsuario" required>
                                        <option value="">Selecionar Usuario</option>
                                        @foreach($usuarios as $usuario)
                                            <option value="{{$usuario->idusuarios}}">{{$usuario->idusuarios}} - {{$usuario->usuario}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <!-- ENTRADA PARA EL NOMBRE -->

                            <div class="form-group">

                                <div class="input-group">

                                    <span class="input-group-addon"><i class="fa fa-building"></i></span>

                                    <input type="text" class="form-control input-lg" id="editarNombre" name="editarNombre" value="" required>
                                    <input type="hidden" id="idActual" name="idActual" value="">

                                </div>

                            </div>

                        </div>

                    </div>

                    <!--=====================================
                    PIE DEL MODAL
                    ======================================-->

                    <div class="modal-footer">

                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

                        <button type="submit" class="btn btn-primary">Guardar Cambios</button>

                    </div>
                </form>

            </div>

        </div>

    </div>
@endsection
