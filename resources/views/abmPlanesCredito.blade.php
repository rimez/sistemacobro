@extends("theme.lte.layout")
@section('titulo')
    Planes de Credito
@endsection
@section('titulo_cabecera')
    Administrar Planes de Credito
@endsection
@section('contenido')
    @include('includes.form-error')
    @include('includes.mensaje')
    <div class="box">

        <div class="box-header with-border">

            <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregar">

                Agregar Plan

            </button>

        </div>


        <div class="box-body">

            <table class="table table-bordered table-striped dt-responsive tablas">

                <thead>

                <tr>

                    <th style="width:57px">Plan Nro</th>
                    <th>Monto</th>
                    <th>Plazo</th>
                    <th>Tasa de interes</th>
                    <th>Tipo de Pago</th>
                    <th>Estado</th>
                    <th>Acciones</th>
                </tr>

                </thead>

                <tbody>

                @foreach($planes as $plan)

                    <tr>
                        <td>{{$plan->idplanescredito}}</td>
                        <td>{{$plan->montoplan}}</td>
                        <td>{{$plan->plazoplan}}</td>
                        <td>{{$plan->interesplan}}%</td>
                        @if($plan->tipopagoplan == true)

                        <td>Diario</td>
                        @else
                            <td>Mensual</td>
                        @endif
                        @if($plan->estadoplan ==true)
                            <td><button class="btn btn-success btn-xs" style="width:60px">ACTIVO</button></td>
                        @else
                            <td><button class="btn btn-danger btn-xs" style="width:60px">INACTIVO</button></td>
                        @endif

                        <td>

                            <div class="btn-group">

                                <button class="btn btn-warning btnEditarPlan" idPlan="{{$plan->idplanescredito}}"
                                        montoPlan="{{$plan->montoplan}}" plazoPlan="{{$plan->plazoplan}}"
                                        interesPlan="{{$plan->interesplan}}" tipoPagoPlan ="{{$plan->tipopagoplan}}"
                                        estadoPlan = "{{$plan->estadoplan}}" gastosadmPlan = "{{$plan->gatosadmplan}}"
                                        data-toggle= "modal" data-target="#modalEditar"><i class="fa fa-pencil"></i></button>

                                <button class="btn btn-danger btnEliminarPlan" data-href="{{route('eliminar_plan',
                                        ['id' => $plan->idplanescredito])}}">
                                    <i class="fa fa-times"></i></button>
                            </div>

                        </td>
                    </tr>
                @endforeach

                </tbody>

            </table>

        </div>

    </div>


    <!--=====================================
    MODAL AGREGAR PLAN
    ======================================-->

    <div id="modalAgregar" class="modal fade" role="dialog">

        <div class="modal-dialog">

            <div class="modal-content">

                <form action="{{route('guardar_plan')}}" role="form" method="post" enctype="multipart/form-data">
                @csrf

                <!--=====================================
                CABEZA DEL MODAL
                ======================================-->

                    <div class="modal-header" style="background:#3c8dbc; color:white">

                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                        <h4 class="modal-title">Agregar Plan de Credito</h4>

                    </div>

                    <!--=====================================
                    CUERPO DEL MODAL
                    ======================================-->

                    <div class="modal-body">

                        <div class="box-body">
                            <!-- ENTRADA PARA EL MONTO -->

                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-money"></i></span>

                                    <input type="number" min="300000" class="form-control input-lg" name="nuevoMonto"
                                           placeholder="Ingresar Monto" value="" required>
                                </div>
                            </div>
                            <!-- ENTRADA PARA LOS GASTOS ADMINISTRATIVOS -->

                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-th"></i></span>

                                    <input type="number" min="10000" max="299000" class="form-control input-lg" name="nuevoGastAdm"
                                           placeholder="Ingresar gasto administrativo" value="" required>
                                </div>
                            </div>

                            <!-- ENTRADA PARA EL PLAZO -->

                            <div class="form-group">

                                <div class="input-group">

                                    <span class="input-group-addon"><i class="fa fa-calendar-check-o"></i></span>

                                    <input type="number" min="3" class="form-control input-lg" name="nuevoPlazo"
                                           placeholder="Ingresar Plazo" value="" required>

                                </div>

                            </div>

                            <!-- ENTRADA PARA LA TASA DE INTERES -->

                            <div class="form-group">

                                <div class="input-group">

                                    <span class="input-group-addon"><i class="fa fa-percent"></i></span>

                                    <input type="number" min="1" max="99" class="form-control input-lg" name="nuevaTasaInteres"
                                           placeholder="Ingresar tasa de interes" value="" required>

                                </div>

                            </div>
                            <!-- ENTRADA PARA SELECCIONAR TIPO DE PAGO -->

                            <div class="form-group">

                                <div class="input-group">

                                    <span class="input-group-addon"><i class="fa fa-th"></i></span>

                                    <select class="form-control input-lg cambioTipoPago" name="nuevoTipoPago" id="nuevoTipoPago" required>

                                        <option value="">Selecionar Tipo de Pago</option>
                                        <option value="1">Diario</option>
                                        <option value="0">Mensual</option>


                                    </select>

                                </div>

                            </div>

                        </div>

                    </div>

                    <!--=====================================
                    PIE DEL MODAL
                    ======================================-->

                    <div class="modal-footer">

                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

                        <button type="submit" class="btn btn-primary">Guardar</button>

                    </div>

                </form>

            </div>

        </div>

    </div>

    <!--=====================================
               ACTUALIZAR DATOS DE PLAN
     ======================================-->

    <div id="modalEditar" class="modal fade" role="dialog">

        <div class="modal-dialog">

            <div class="modal-content">

                <form action="{{route('actualizar_plan')}}" role="form" method="post" enctype="multipart/form-data">
                @csrf

                <!--=====================================
                CABEZA DEL MODAL
                ======================================-->

                    <div class="modal-header" style="background:#3c8dbc; color:white">

                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                        <h4 class="modal-title">Actualizar Plan</h4>

                    </div>

                    <!--=====================================
                    CUERPO DEL MODAL
                    ======================================-->

                    <div class="modal-body">

                        <div class="box-body">
                            <!-- ENTRADA PARA SELECCIONAR ACTIVAR DESACTIVAR -->

                            <div class="form-group">

                                <div class="input-group">

                                    <span class="input-group-addon"><i class="fa fa-cog"></i></span>

                                    <select class="form-control input-lg cambioEstadoPago" name="editarEstado" id="editarEstado" required>

                                        <option value="">Selecionar Estado</option>
                                        <option value="1">Activo</option>
                                        <option value="0">Inactivo</option>


                                    </select>

                                </div>

                            </div>
                            <!-- ENTRADA PARA EL MONTO -->

                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-money"></i></span>

                                    <input type="number" min="300000" class="form-control input-lg" id="EditarMonto"
                                           placeholder="Ingresar Monto" value="" required readonly>
                                    <input type="hidden" name="idActual" id="idActual" value="">
                                </div>
                            </div>
                            <!-- ENTRADA PARA LOS GASTOS ADMINISTRATIVOS -->

                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-th"></i></span>

                                    <input type="number" min="10000" max="299000" class="form-control input-lg" id="nuevoGastAdm"
                                           placeholder="Ingresar gasto administrativo" value="" required readonly>
                                </div>
                            </div>

                            <!-- ENTRADA PARA EL PLAZO -->

                            <div class="form-group">

                                <div class="input-group">

                                    <span class="input-group-addon"><i class="fa fa-calendar-check-o"></i></span>

                                    <input type="number" min="3" class="form-control input-lg" id="editarPlazo"
                                           placeholder="Ingresar Plazo" value="" required readonly>

                                </div>

                            </div>

                            <!-- ENTRADA PARA LA TASA DE INTERES -->

                            <div class="form-group">

                                <div class="input-group">

                                    <span class="input-group-addon"><i class="fa fa-percent"></i></span>

                                    <input type="number" min="1" max="99" class="form-control input-lg"
                                           id="editarTasaInteres"
                                           placeholder="Ingresar Plazo" value="" required readonly>

                                </div>

                            </div>

                            <!-- ENTRADA PARA SELECCIONAR TIPO DE PAGO -->

                            <div class="form-group">

                                <div class="input-group">

                                    <span class="input-group-addon"><i class="fa fa-th"></i></span>

                                    <select class="form-control input-lg cambioTipoPago" id="editarTipoPago" required disabled>

                                        <option value="">Selecionar Tipo de Pago</option>
                                        <option value="1">Diario</option>
                                        <option value="0">Mensual</option>


                                    </select>

                                </div>

                            </div>

                        </div>

                    </div>

                    <!--=====================================
                    PIE DEL MODAL
                    ======================================-->

                    <div class="modal-footer">

                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

                        <button type="submit" class="btn btn-primary">Guardar Cambios</button>

                    </div>
                </form>

            </div>

        </div>

    </div>
@endsection
