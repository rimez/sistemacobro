<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="active">
                <a href="/inicio">
                    <i class="fa fa-home"></i>
                    <span>Inicio</span>
                </a>
            </li>
            <li class="active">
                <a href="{{route('abm_usuarios')}}">
                    <i class="fa fa-user"></i>
                    <span>Usuarios</span>
                </a>
            </li>
            <li class="active">
                <a href="{{route('clientes')}}">
                    <i class="fa fa-users"></i>
                    <span>Clientes</span>
                </a>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-pencil-square-o"></i> <span>ABMS</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">

                    <li class="treeview">
                        <a href="#"><i class="fa fa-circle-o"></i> Creditos
                            <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{route('creditos')}}"><i class="fa fa-file-text"></i>Solicitudes</a></li>
                            <li><a href="{{route('independiente',['tabla' => 'tipoprestamo'])}}"><i class="fa fa-th"></i>Tipos de Prestamo</a></li>
                            <li><a href="{{route('planes')}}"><i class="fa fa-list-ol"></i>Planes</a></li>
                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#"><i class="fa fa-building"></i>Sucursales
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{route('sucursales')}}"><i class="fa fa-building"></i>Sucursales</a></li>
                            <li><a href="{{route('feriado')}}"><i class="fa fa-calendar-check-o"></i>Feriado/s</a></li>
                        </ul>
                    </li>
                    <li><a href="{{route('zonas')}}"><i class="fa fa-street-view"></i>Zonas</a></li>
                    <li><a href="{{route('independiente',['tabla' => 'departamentos'])}}"><i class="fa fa-map-marker"></i>Departamentos</a></li>
                    <li><a href="{{route('independiente',['tabla' => 'nacionalidades'])}}"><i class="fa fa-globe"></i>Nacionalidades</a></li>
                    <li><a href="{{route('independiente',['tabla' => 'perfiles'])}}"><i class="fa fa-user"></i>Perfiles</a></li>
                    <li><a href="{{route('independiente',['tabla' => 'profesiones'])}}"><i class="fa fa-briefcase"></i>Profesiones</a></li>
                    <li><a href="{{route('independiente',['tabla' => 'estados'])}}"><i class="fa fa-sign-in"></i>Estados</a></li>
                </ul>
            </li>
        </ul>
    </section>
</aside>
