<header class="main-header">
    <!--==============================
	=            LOGOTIPO            =
	===============================-->
    <a href="inicio" class="logo">
        <!-- logo mini -->
        <span class="logo-mini">

				<img src="{{asset("assets/lte/img/iconoSis.ico")}}"
                     class="img-responsive" style="padding:5px">
			</span>

        <!-- logo normal -->
        <span class="logo-lg">

				<img src="{{asset("assets/lte/img/login.png")}}"
                     class="img-responsive" style="padding:5px 0px">
			</span>
    </a>
    <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{!! session('fotousuario') !!}" class="user-image" alt="User Image">
                        <span class="hidden-xs">{!! session('nombreusuario') !!}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="pull-right">
                                <a href="{{route('cerrar_sesion')}}" class="btn btn-danger btn-flat">Cerrar sesión</a>
                            </div>
                        </li>

                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
