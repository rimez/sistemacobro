<!DOCTYPE html>
<html lang="es">
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" id="csrf-token" content="{{ csrf_token() }}">
        <link rel="icon" href="{{asset("assets/lte/img/iconoSis.ico")}}">
        <title>@yield('titulo', 'Sistema')</title>



        <!--=====================================
        PLUGINS DE CSS
        ======================================-->
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
            <link rel="stylesheet" href="{{asset("assets/lte/bower_components/bootstrap/dist/css/bootstrap.min.css")}}">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{asset("assets/lte/bower_components/font-awesome/css/font-awesome.min.css")}}">
        <!-- Ionicons -->
        <link rel="stylesheet" href="{{asset("assets/lte/bower_components/Ionicons/css/ionicons.min.css")}}">
        <!-- daterange picker -->
        <link rel="stylesheet" href="{{asset("assets/lte/bower_components/bootstrap-daterangepicker/daterangepicker.css")}}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{asset("assets/lte/dist/css/AdminLTE.css")}}">
        <!-- bootstrap datepicker -->
        <link rel="stylesheet" href="{{asset("assets/lte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css")}}">
        <!-- Select2 -->
        <link rel="stylesheet" href="{{asset("assets/lte/bower_components/select2/dist/css/select2.min.css")}}">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="{{asset("assets/lte/dist/css/skins/_all-skins.min.css")}}">
        <!-- Data Tables -->
        <link rel="stylesheet" href="{{asset("assets/lte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css")}}">
        <link rel="stylesheet" href="{{asset("assets/lte/bower_components/datatables.net-bs/css/responsive.bootstrap.min.css")}}">





    @yield('styles')

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>-->
        <!-- <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>-->
        <!--<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>-->
        <!--[endif]-->

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <body class="hold-transition skin-blue sidebar-collapse sidebar-mini">
        <!-- Site wrapper -->
        <div class="wrapper">
            <!-- inicio header -->
            @include("theme/lte/header")
            <!-- fin header -->
            <!-- inicio aside -->
            @include("theme/lte/aside")
            <!-- fin aside -->
            <div class="content-wrapper">
                <section class="content-header">
                    <h1>@yield('titulo_cabecera')</h1>
                </section>
{{--                <ol class="breadcrumb">--}}
{{--                    <li class="active">Inicio</li>--}}

{{--                </ol>--}}
                <section class="content">

                    @yield('contenido')

                </section>
            </div>
            <!-- inicio footer -->
            @include("theme/lte/footer")
            <!-- fin foote -->
        </div>

        <!--=====================================
        PLUGINS DE JAVASCRIPT
        ======================================-->
        <!-- jQuery 3 -->
        <script src="{{asset("assets/lte/bower_components/jquery/dist/jquery.min.js")}}"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="{{asset("assets/lte/bower_components/bootstrap/dist/js/bootstrap.min.js")}}"></script>
        <!-- SlimScroll -->
        <script src="{{asset("assets/lte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js")}}"></script>
        <!-- FastClick -->
        <script src="{{asset("assets/lte/bower_components/fastclick/lib/fastclick.js")}}"></script>
        <!-- AdminLTE App -->
        <script src="{{asset("assets/lte/dist/js/adminlte.min.js")}}"></script>




        <!-- SweetAlert2 -->
        <script src="{{asset("assets/lte/plugins/sweetalert2/sweetalert2.all.js")}}"></script>

        <script src="{{asset("assets/lte/bower_components/datatables.net/js/jquery.dataTables.min.js")}}"></script>
        <!-- Bootstrap WYSIHTML5 -->
{{--        <script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>--}}
        <script src="{{asset("assets/lte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js")}}"></script>
        <script src="{{asset("assets/lte/bower_components/datatables.net-bs/js/dataTables.responsive.min.js")}}"></script>
        <!-- Select2 -->
        <script src="{{asset("assets/lte/bower_components/select2/dist/js/select2.full.min.js")}}"></script>
        <!-- date-range-picker -->
        <script src="{{asset("assets/lte/bower_components/moment/min/moment.min.js")}}"></script>
        <script src="{{asset("assets/lte/bower_components/bootstrap-daterangepicker/daterangepicker.js")}}"></script>
        <!-- bootstrap datepicker -->
        <script src="{{asset("assets/lte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js")}}"></script>
        <!-- InputMask -->
        <script src="{{asset("assets/lte/plugins/input-mask/jquery.inputmask.js")}}"></script>
        <script src="{{asset("assets/lte/plugins/input-mask/jquery.inputmask.date.extensions.js")}}"></script>
        <script src="{{asset("assets/lte/plugins/input-mask/jquery.inputmask.extensions.js")}}"></script>

        <script src="{{asset("assets/lte/js/plantilla.js")}}"></script>
        <script src="{{asset("assets/lte/js/usuarios.js")}}"></script>
        <script src="{{asset("assets/lte/js/independiente.js")}}"></script>
        <script src="{{asset("assets/lte/js/clientes.js")}}"></script>
        <script src="{{asset("assets/lte/js/sucursales.js")}}"></script>
        <script src="{{asset("assets/lte/js/zonas.js")}}"></script>
        <script src="{{asset("assets/lte/js/planes.js")}}"></script>
        <script src="{{asset("assets/lte/js/creditos.js")}}"></script>

{{--        @yield('scripts')--}}
    </body>
</html>
