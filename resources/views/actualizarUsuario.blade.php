@extends("theme.lte.layout")
@section('titulo')
    Usuario
@endsection
@section('titulo_cabecera')
    Usuario - {{$usuario[0]->usuario}}
@endsection
@section('contenido')
    @include('includes.form-error')
    @include('includes.mensaje')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Editar Usuario</h3>
                </div>
                <form action="{{route('actualizar_usuario')}}" role="form" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="box-body">
                        <div class="row">
                            <!-- ENTRADA PARA EL NOMBRE -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Nombre</label>
                                        <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Ingrese Nombre"

                                               value="{{$usuario[0]->nombreusuario}}" required>
                                    <input type="hidden" id="idUsuario" name="idUsuario" value="{{$usuario[0]->idusuarios}}">
                                    <input type="hidden" id="usuario" name="usuario" value="{{$usuario[0]->usuario}}">
                                    <input type="hidden" id="rutaFoto" name="rutaFoto" value="{{$usuario[0]->fotousuario}}">
                                </div>
                            </div>
                            <!-- ENTRADA PARA EL APELLIDO -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Apellido</label>
                                    <input type="text" class="form-control" id="apellido" name="apellido"
                                           placeholder="Ingrese apellido" required value="{{$usuario[0]->apellidousuario}}">
                                </div>
                            </div>
                            <!-- ENTRADA PARA SELECCIONAR EL PERFIL -->
                            <div class="col-md-4">
                                <label for="exampleInputEmail1">Perfil</label>
                                <div class="input-group input-group-sm">
                                    <select class="form-control select2" id="idPErfil" idbd="{{$usuario[0]->idperfil}}"
                                            idNac="{{$usuario[0]->idnacionalidad}}"
                                            idDepar="{{$usuario[0]->iddepartamentos}}"
                                            idCiu="{{$usuario[0]->idciudades}}"
                                            name="idPErfil" required>
                                        <option value="">Selecionar perfil</option>
                                        @foreach($perfiles as $perfil)
                                            <option value="{{$perfil->idperfil}}">{{$perfil->perfil}}</option>
                                        @endforeach
                                    </select>
                                    <span class="input-group-btn">
                                     <button type="button" class="btn btn-success btn-flat" data-toggle= "modal"
                                             data-target="#modalAgregarPerfil">+</button>
                                </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <!-- ENTRADA PARA LA CEDULA -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Cedula</label>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-id-card-o"></i></span>
                                            <input type="text" class="form-control" id="cedula" name="cedula"
                                                   placeholder="Ingrese Numero de cedula" value="{{$usuario[0]->cedulausuario}}" required>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- ENTRADA PARA EL NUM DE CELULAR-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Celular</label>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-phone"></i></span>

                                            <input type="text" class="form-control" name="nuevoTelefono"
                                                   placeholder="Ingresar telefono" data-inputmask="'mask':'(9999) 999-999'"
                                                   data-mask
                                                   value="{{$usuario[0]->celularusuario}}"

                                                   required>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- ENTRADA PARA LA FECHA DE NACIMIENTO -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Fecha de nacimiento:</label>

                                    <div class="form-group">

                                        <div class="input-group">

                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>

                                            <input type="text" class="form-control" name="nuevaFechaNacimiento"
                                                   placeholder="Ingresar fecha de nacimiento"
                                                   data-inputmask="'alias': 'dd-mm-yyyy'" data-mask
                                                   value="{{$usuario[0]->fecha}}"  required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <!-- ENTRADA PARA LA NACIONALIDAD -->
                            <div class="col-md-4">
                                <label for="exampleInputEmail1">Nacionalidad</label>
                                <div class="input-group input-group-sm">
                                    <select class="form-control select2" id="idnacionalidad" name="idnacionalidad" required>
                                        <option value="">Seleccionar Nacionalidad</option>
                                        @foreach($nacionalidades as $nacionalidad)
                                            <option value="{{$nacionalidad->idnacionalidad}}">{{$nacionalidad->nacionalidad}}</option>
                                        @endforeach
                                    </select>
                                    <span class="input-group-btn">
                                     <button type="button" class="btn btn-success btn-flat" data-toggle= "modal"
                                             data-target="#modalAgregarNacionalidad">+</button>
                                </span>
                                </div>
                            </div>
                            <!-- ENTRADA PARA EL DEPARTAMENTO -->
                            <div class="col-md-4">
                                <label for="exampleInputEmail1">Departamento</label>
                                <div class="input-group input-group-sm">
                                    <select class="form-control select2 departamento2" required id="idDepartamento" name="idDepartamento">
                                        <option value="">Seleccionar Departamento</option>
                                        @foreach($departamentos as $departamento)
                                            <option value="{{$departamento->iddepartamentos}}">{{$departamento->departamento}}</option>
                                        @endforeach
                                    </select>
                                    <span class="input-group-btn">
                                     <button type="button" class="btn btn-success btn-flat" data-toggle= "modal"
                                             data-target="#modalAgregarDepartamento">+</button>
                                </span>
                                </div>
                            </div>
                            <!-- ENTRADA PARA LA CIUDAD -->
                            <div class="col-md-4">
                                <label for="exampleInputEmail1">Ciudad</label>
                                <div class="input-group input-group-sm">
                                    <select class="form-control select2 ciudad2" id="idCiudad" name="idciudad" required>
                                        <option value="">Seleccionar Ciudad</option>
                                        @foreach($ciudades as $ciudad)
                                            <option value="{{$ciudad->idciudades}}">{{$ciudad->ciudad}}</option>
                                        @endforeach

                                    </select>
                                    <span class="input-group-btn">
                                     <button type="button" class="btn btn-success btn-flat btnCiudad2">+</button>
                                </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <!-- ENTRADA PARA LA DIRECCION -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Direccion</label>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-home"></i></span>
                                            <input type="text" class="form-control" id="direccion" name="direccion"
                                                   placeholder="Ingrese Nombre" value="{{$usuario[0]->direccionusuario}}" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- ENTRADA PARA LA FOTO -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Foto</label>
                                    <input type="file" class="nuevaFoto" name="nuevaFoto">
                                    <p class="help-block">Peso máximo de la foto 4 MB</p>
                                    <img
                                        src="{{$usuario[0]->fotousuario}}" class="img-thumbnail previsualizar" width="100px">
                                    <input type="hidden" name="fotoActual" id="fotoActual">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Guardar Cambios</button>
                    </div>
                </form>
            </div>

        </div>
    </div>


    <!--=====================================
    MODAL AGREGAR NACIONALIDAD
    ======================================-->

    <div id="modalAgregarNacionalidad" class="modal fade" role="dialog">

        <div class="modal-dialog">

            <div class="modal-content">

                <form action="{{route('guardar_nacionalidad')}}" role="form" method="post" enctype="multipart/form-data">
                @csrf

                <!--=====================================
                CABEZA DEL MODAL
                ======================================-->

                    <div class="modal-header" style="background:#3c8dbc; color:white">

                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                        <h4 class="modal-title">Agregar Nacionalidad</h4>

                    </div>

                    <!--=====================================
                    CUERPO DEL MODAL
                    ======================================-->

                    <div class="modal-body">

                        <div class="box-body">

                            <!-- ENTRADA PARA EL NOMBRE -->

                            <div class="form-group">

                                <div class="input-group">

                                    <span class="input-group-addon"><i class="fa fa-th"></i></span>

                                    <input type="text" class="form-control input-lg" name="nuevoNombre" placeholder="Ingresar nacionalidad" value="">

                                </div>

                            </div>

                        </div>

                    </div>

                    <!--=====================================
                    PIE DEL MODAL
                    ======================================-->

                    <div class="modal-footer">

                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

                        <button type="submit" class="btn btn-primary">Guardar Nacionalidad</button>

                    </div>

                </form>

            </div>

        </div>

    </div>
    <!--=====================================
    MODAL AGREGAR DEPARTAMENTO
    ======================================-->

    <div id="modalAgregarDepartamento" class="modal fade" role="dialog">

        <div class="modal-dialog">

            <div class="modal-content">

                <form action="{{route('guardar_departamento')}}" role="form" method="post" enctype="multipart/form-data">
                @csrf

                <!--=====================================
                CABEZA DEL MODAL
                ======================================-->

                    <div class="modal-header" style="background:#3c8dbc; color:white">

                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                        <h4 class="modal-title">Agregar Departamento</h4>

                    </div>

                    <!--=====================================
                    CUERPO DEL MODAL
                    ======================================-->

                    <div class="modal-body">

                        <div class="box-body">

                            <!-- ENTRADA PARA EL NOMBRE -->

                            <div class="form-group">

                                <div class="input-group">

                                    <span class="input-group-addon"><i class="fa fa-th"></i></span>

                                    <input type="text" class="form-control input-lg" name="nuevoNombre" placeholder="Ingresar departamento" value="">

                                </div>

                            </div>

                        </div>

                    </div>

                    <!--=====================================
                    PIE DEL MODAL
                    ======================================-->

                    <div class="modal-footer">

                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

                        <button type="submit" class="btn btn-primary">Guardar Departamento</button>

                    </div>

                </form>

            </div>

        </div>

    </div>



    {{--Modal Agregar Perfil--}}
    @include("includes.modal-guardar")
@section('nombreModalGuardar','modalAgregarPerfil')
@section('rutaGuardarModal')
    {{route('guardar_perfil')}}
@endsection
@section('cabeceraModal','Agregar Perfil')
@section('iconoImputModal','fa fa-th')
@section('botonGuardarModal','Guardar Perfil')

{{--Modal Agregar Departamento--}}
@include("includes.modal-guardar")
@section('nombreModalGuardar','modalAgregarDepartamento')
@section('rutaGuardarModal')
    {{--    {{route('guardar_estado_dispositivo')}}--}}
@endsection
@section('cabeceraModal','Agregar Departamento')
@section('iconoImputModal','fa fa-th')
@section('botonGuardarModal','Guardar Departamento')
@endsection

