@extends("theme.lte.layout")
@section('titulo')
    Creditos - Solicitud
@endsection
@section('titulo_cabecera')
    Creditos - Solicitud
@endsection
@section('contenido')
    @include('includes.form-error')
    @include('includes.mensaje')
{{--    <div class="box box-primary">--}}
{{--        <div class="box-header with-border">--}}
{{--            <h3 class="box-title">Agregar Solicitud de Credito</h3>--}}
{{--            <form action="{{route('guardar_credito')}}" role="form" method="post" enctype="multipart/form-data">--}}
{{--                {{ csrf_field() }}--}}
{{--                <div class="box-body">--}}
{{--                    <div class="row">--}}
{{--                        <div class="col-md-6">--}}
{{--                            <div class="form-group">--}}
{{--                                <label>Cliente</label>--}}
{{--                                <select class="form-control select2" style="width: 100%;">--}}
{{--                                    <option value="">Seleccionar Cliente</option>--}}
{{--                                    @foreach($clientes as $cliente)--}}
{{--                                        <option value="{{$cliente->idcliente}}">{{$cliente->idcliente}}--}}
{{--                                            - C.I.N {{$cliente->cedulacliente}} - {{$cliente->apellidocliente}},--}}
{{--                                            {{$cliente->nombrecliente}}</option>--}}
{{--                                    @endforeach--}}
{{--                                </select>--}}
{{--                            </div>--}}
{{--                            <div class="form-group">--}}
{{--                                <label>Garantia:</label>--}}
{{--                                <select class="form-control select2" style="width: 100%;">--}}
{{--                                    <option value="">Seleccionar Garantia</option>--}}
{{--                                    <option value="1">Personal</option>--}}
{{--                                    <option value="0">Solidaria</option>--}}
{{--                                </select>--}}
{{--                            </div>--}}
{{--                            <div class="form-group">--}}
{{--                                <label>Apellido y Nombre del Garante</label>--}}
{{--                                <div class="form-group">--}}
{{--                                    <div class="input-group">--}}
{{--                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>--}}
{{--                                        <input type="text" class="form-control" name="nuevoApeyNom"--}}
{{--                                               placeholder="" required readonly>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                    </div>--}}
{{--                        <div class="col-md-6">--}}
{{--                            <div class="form-group">--}}
{{--                                <label>Plan</label>--}}
{{--                                <select class="form-control select2" style="width: 100%;">--}}
{{--                                    <option value="">Seleccionar Plan de Creditos</option>--}}
{{--                                    @foreach($planes as $plan)--}}
{{--                                        <option value="{{$plan->idplanescredito}}">{{$plan->montoplan}}</option>--}}
{{--                                    @endforeach--}}
{{--                                </select>--}}
{{--                            </div>--}}
{{--                            <div class="form-group">--}}
{{--                                <label>Garante</label>--}}
{{--                                <select class="form-control select2" disabled style="width: 100%;">--}}
{{--                                    <option value="">Seleccionar Garante</option>--}}
{{--                                    @foreach($clientes as $cliente)--}}
{{--                                        <option value="{{$cliente->idcliente}}">{{$cliente->idcliente}}</option>--}}
{{--                                    @endforeach--}}
{{--                                </select>--}}
{{--                            </div>--}}
{{--                            <div class="form-group">--}}
{{--                                <label>Nro. de cedula del Garante</label>--}}
{{--                                <div class="form-group">--}}
{{--                                    <div class="input-group">--}}
{{--                                        <span class="input-group-addon"><i class="fa fa-id-card-o"></i></span>--}}
{{--                                        <input type="text" class="form-control" name="nuevoApeyNom"--}}
{{--                                               placeholder="" required readonly>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                        </div>--}}

{{--                    </div>--}}

{{--                </div>--}}
{{--                <!-- /.box-body -->--}}
{{--                <div class="box-footer">--}}
{{--                    <button type="submit" class="btn btn-primary ">Guardar Solicitud</button>--}}
{{--                    <button type="button" class="btn btn-danger" style="width: 120px">Cancelar</button>--}}
{{--                </div>--}}
{{--            </form>--}}

{{--        </div>--}}
{{--    </div>--}}
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Agregar credito</h3>
                </div>
                <form action="{{route('guardar_credito')}}" role="form" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Zona</label>
                                    <select class="form-control select2" id="zona" name="idZona" style="width: 100%;" required>
                                        <option value="">Seleccionar Zona</option>
                                        @foreach($zonas as $zona)
                                            <option value="{{$zona->usuarios_idusuarios}}">{{$zona->nombrezona}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Tipo de Prestamo</label>
                                    <select class="form-control select2" id="tipoPrest" name="tipoPrest" style="width: 100%;" required>
                                        <option value="">Seleccionar Tipo de prestamo</option>
                                        @foreach($tiposPrestamos as $tipoPrestamo)
                                            <option value="{{$tipoPrestamo->idtipoprestamo}}">{{$tipoPrestamo->nombretipoprestamo}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-6">
{{--                                Entrada cliente--}}
                                <div class="form-group">
                                    <label>Cliente</label>
                                    <select class="form-control select2 clientes" id="cliente" name="idCliente" style="width: 100%;" required>
                                        <option value="">Seleccionar Cliente</option>
                                        @foreach($clientes as $cliente)
                                            <option value="{{$cliente->idcliente}}" id="C{{$cliente->idcliente}}">{{$cliente->idcliente}}
                                                - C.I.N {{$cliente->cedulacliente}} - {{$cliente->apellidocliente}},
                                                {{$cliente->nombrecliente}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
{{--                                    Entrada Garanantia--}}
                                    <label>Garantia:</label>
                                    <select class="form-control select2 garantia" name="idGarantia" style="width: 100%;" required>
                                        <option value="">Seleccionar Garantia</option>
                                        <option value="1">Personal</option>
                                        <option value="0">Solidaria</option>
                                    </select>
                                </div>
{{--                                Entrada nombre Garante--}}
                                <div class="form-group">
                                    <label>Apellido y Nombre del Garante</label>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                            <input type="text" class="form-control" name="nuevoApeyNomGar" id="nuevoApeyNomGar"
                                                   placeholder="" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
{{--                            Entrada para Seleccionar Plan--}}
                                <div class="form-group">
                                    <label>Plan</label>
                                    <select class="form-control select2 plan" name="idPlan" id="idPlan" style="width: 100%;" required>
                                        <option value="">Seleccionar Plan de Creditos</option>
                                        @foreach($planes as $plan)
                                            <option value="{{$plan->idplanescredito}}">{{$plan->idplanescredito}}-{{$plan->montoplan}}</option>
                                        @endforeach
                                    </select>
                                    <input type="hidden" id="gastosadm" name="gastosadm" value="">
                                </div>
{{--                                Seleccionar Garante--}}
                                <div class="form-group">
                                    <label>Garante</label>
                                    <select class="form-control select2 garante" id="garante" disabled style="width: 100%;">
                                        <option value="">Seleccionar Garante</option>
                                        @foreach($clientes as $cliente)
                                            <option value="{{$cliente->idcliente}}" id="G{{$cliente->idcliente}}">{{$cliente->idcliente}}
                                                - C.I.N {{$cliente->cedulacliente}} - {{$cliente->apellidocliente}},
                                                {{$cliente->nombrecliente}}</option>
                                        @endforeach
                                    </select>
                                    <input type="hidden" id="idGarante" name="idGarante" value="">
                                </div>
{{--                                Entrada cedula--}}
                                <div class="form-group">
                                    <label>Nro. de cedula del Garante</label>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-id-card-o"></i></span>
                                            <input type="text" class="form-control" name="nuevaciGar" id="nuevaciGar"
                                                   placeholder="" required readonly>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Guardar credito</button>
                        <button type="button" class="btn btn-danger" style="width: 120px">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
